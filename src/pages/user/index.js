import React, {useEffect, useState} from 'react';
import {StyleSheet, View, ScrollView, StatusBar} from 'react-native';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import {colors} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import SectionHeader from './parts/SectionHeader';
import SectionContent from './parts/SectionContent';

const User = ({navigation}) => {
  const [dataUser, setDataUser] = useState(null);

  useEffect(() => {
    const _validasiSession = async () => {
      const session = await AsyncStorageLib.getItem('dataUser');
      setDataUser(session !== null && JSON.parse(session));
    };
    _validasiSession();
  }, []);
  return (
    <>
      <StatusBar
        translucent
        barStyle={'dark-content'}
        backgroundColor="transparent"
      />
      <View style={styles.page}>
        <View style={styles.content}>
          <ScrollView>
            {/* section header */}
            <SectionHeader data={dataUser} />
            {/* section content */}
            <SectionContent navigation={navigation} />
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default User;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingTop: getStatusBarHeight() + 30,
  },
});
