import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../../../utils';
import {TouchableOpacity} from 'react-native-gesture-handler';

const ItemMenu = ({icon, name, desc, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        borderWidth: 1,
        borderColor: colors.greySecondary,
        borderRadius: 10,
        marginBottom: 15,
      }}>
      <View style={{paddingHorizontal: 10}}>{icon}</View>
      <View style={{flex: 1, paddingLeft: 10}}>
        <Text
          style={{
            fontSize: fontSize.small,
            fontFamily: fonts.primary['normal'],
            color: colors.dark,
          }}>
          {name}
        </Text>
        <Text
          numberOfLines={1}
          style={{
            fontSize: fontSize.mini,
            fontFamily: fonts.primary[300],
            color: colors.grey,
            maxWidth: '95%',
          }}>
          {desc}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default ItemMenu;

const styles = StyleSheet.create({});
