import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import ItemMenu from './components/ItemMenu';
import {Gap} from '../../../components';
import Icon from 'react-native-vector-icons/Feather';
import {colors, FUNCToast, VERSIAPP} from '../../../utils';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux';
import {
  ICUserChangePassword,
  ICUserEditProfile,
  ICUserLogout,
  ICUserVersiApp,
} from '../../../assets/icons';

const SectionContent = ({navigation}) => {
  const dispatch = useDispatch();

  const btnLogout = async () => {
    await AsyncStorageLib.clear();
    dispatch({type: 'SESSION_USER', payload: null});
    FUNCToast('SUCCESS', {msg: 'Anda telah keluar dari aplikasi'});
    setTimeout(() => {
      navigation.replace('Splash');
    }, 2000);
  };

  const jsonMenus = [
    // {
    //   name: 'Edit Profil',
    //   desc: 'Perbarui Informasi Akun Anda',
    //   icon: <ICUserEditProfile />,
    //   onPress: () => navigation.navigate('EditProfil'),
    // },
    {
      name: 'Ganti Password',
      desc: 'Ubah Menjadi Password Baru',
      icon: <ICUserChangePassword />,
      onPress: () => navigation.navigate('UbahPassword'),
    },
    {
      name: 'Versi App',
      desc: 'Lihat versi aplikasi saat ini',
      icon: <ICUserVersiApp />,
      onPress: () => FUNCToast('INFO', {msg: `BNPP APP.  \nVersi ${VERSIAPP}`}),
    },
    {
      name: 'Logout',
      desc: 'Hapus session login akun dari aplikasi',
      icon: <ICUserLogout />,
      onPress: () => btnLogout(),
    },
  ];
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={20} />
      {jsonMenus.map((v, i) => {
        return (
          <ItemMenu
            key={i}
            name={v.name}
            desc={v.desc}
            icon={v.icon}
            onPress={v.onPress}
          />
        );
      })}
    </View>
  );
};

export default SectionContent;

const styles = StyleSheet.create({});
