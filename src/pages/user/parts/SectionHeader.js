import {StyleSheet, Text, View, Image} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors, fonts, fontSize} from '../../../utils';
import {Gap} from '../../../components';
import {Logo} from '../../../assets';
import axios from 'axios';

const SectionHeader = ({data}) => {
  const [isProfileImg, setIsProfileImg] = useState(false);
  useEffect(() => {
    axios
      .get(data?.profile_photo_url)
      .then(res => {
        setIsProfileImg(true);
      })
      .catch(err => setIsProfileImg(false));
  }, []);
  return (
    <View style={{alignItems: 'center', width: '100%'}}>
      <View style={{width: 100, height: 100}}>
        <Image
          // source={isProfileImg ? {uri: data?.profile_photo_url} : Logo}
          source={Logo}
          style={{
            width: undefined,
            height: undefined,
            resizeMode: 'cover',
            flex: 1,
            backgroundColor: colors.greySecondary,
            borderRadius: 100,
          }}
        />
      </View>
      <Gap height={15} />
      <Text
        style={{
          fontSize: fontSize.large,
          fontFamily: fonts.primary[600],
          color: colors.dark,
          paddingHorizontal: 20,
          textAlign: 'center',
        }}>
        {data?.userProfile.nama || 'No Name'}
      </Text>
      <Gap height={5} />
      <Text
        style={{
          fontSize: fontSize.small,
          fontFamily: fonts.primary[400],
          color: colors.grey,
        }}>
        {data?.userProfile.email || 'email@gmail.com'}
      </Text>
      <Gap height={5} />
      <Text
        style={{
          fontSize: fontSize.mini,
          fontFamily: fonts.primary[400],
          color: colors.blue,
        }}>
        {data?.userProfile.telepon || '08988888888'}
      </Text>
    </View>
  );
};

export default SectionHeader;

const styles = StyleSheet.create({});
