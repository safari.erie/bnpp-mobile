import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {apiChangePassword, setFormLogin} from '../../../redux/actions';
import {Button, Gap, LabelTextInput} from '../../../components';
import {useDispatch, useSelector} from 'react-redux';

const SectionContent = ({navigation}) => {
  const {formLogin} = useSelector(state => state.loginReducer);
  const dispatch = useDispatch();
  return (
    <>
      <View style={{paddingHorizontal: 15}}>
        <Gap height={20} />
        <LabelTextInput
          title={'Password Lama'}
          placeholder={'Ketikkan Password Lama...'}
          onChangeText={e => dispatch(setFormLogin('PasswordLama', e))}
        />
        <LabelTextInput
          title={'Password Baru'}
          placeholder={'Ketikkan Password Baru...'}
          onChangeText={e => dispatch(setFormLogin('PasswordBaru', e))}
        />
        <LabelTextInput
          title={'Konfirmasi Password Baru'}
          placeholder={'Ketikkan Konfirmasi Password Baru...'}
          onChangeText={e => dispatch(setFormLogin('PasswordBaru2', e))}
        />
      </View>
      <View
        style={{
          position: 'absolute',
          bottom: 0,
          width: '100%',
          paddingHorizontal: 15,
          paddingBottom: 30,
        }}>
        <Button
          type={'primary'}
          title={'Simpan Password Baru'}
          onPress={() => dispatch(apiChangePassword(formLogin, navigation))}
          width={'100%'}
        />
      </View>
    </>
  );
};

export default SectionContent;

const styles = StyleSheet.create({});
