import {StyleSheet, View, StatusBar} from 'react-native';
import React, {useEffect} from 'react';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {colors} from '../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Header} from '../../components';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux';
import {setFormLogin} from '../../redux/actions';
import SectionContent from './parts/SectionContent';

const UbahPassword = ({navigation}) => {
  const dispatch = useDispatch();

  useEffect(() => {
    const _validasiSession = async () => {
      try {
        const session = await AsyncStorageLib.getItem('dataUser');
        const json = session !== null && JSON.parse(session);
        dispatch(setFormLogin('idUser', json.iduser));
      } catch (error) {
        console.log(error);
      }
    };
    _validasiSession();
  }, []);

  return (
    <>
      <StatusBar
        translucent
        barStyle={'dark-content'}
        backgroundColor="transparent"
      />
      <View style={styles.page}>
        <View style={styles.content}>
          {/* section header */}
          <Header
            title={'Ganti Password'}
            iconLeft={
              <Icon name="keyboard-backspace" color={colors.dark} size={25} />
            }
            onPressLeft={() => navigation.goBack()}
          />
          {/* section content */}
          <SectionContent navigation={navigation} />
        </View>
      </View>
    </>
  );
};

export default UbahPassword;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    paddingTop: getStatusBarHeight() + 20,
  },
});
