import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  StatusBar,
  Animated,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {LoadingSplash, LottieLoadingSplash} from '../../assets';
import {colors, fonts} from '../../utils';
import AnimatedLottieView from 'lottie-react-native';
import {getHomes} from '../../redux/actions';
import {useDispatch} from 'react-redux';

const Spalsh = ({navigation}) => {
  const [fadeAnim] = useState(new Animated.Value(0));
  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 5000);
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 2000,
    }).start();
    dispatch(getHomes('GET_HOMES'));
  }, [navigation]);

  return (
    <>
      <StatusBar
        translucent
        barStyle={'dark-content'}
        backgroundColor="transparent"
      />
      <Animated.View style={styles.container(fadeAnim)}>
        <View style={{width: 350, height: 350}}>
          <AnimatedLottieView source={LottieLoadingSplash} autoPlay loop />
        </View>
        <Text
          numberOfLines={2}
          style={{
            fontSize: 14,
            fontFamily: fonts.primary['normal'],
            color: colors.dark,
          }}>
          sedang mempersiapkan fitur...
        </Text>
      </Animated.View>
      {/* <View style={styles.container}>
        <Image source={LoadingSplash} style={{width: 100, height: 100}} />
      </View> */}
    </>
  );
};

export default Spalsh;

const styles = StyleSheet.create({
  container: fadeAnim => ({
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    opacity: fadeAnim,
    marginTop: -50,
  }),
});
