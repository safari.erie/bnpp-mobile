import {StyleSheet, View, StatusBar, ScrollView, Text} from 'react-native';
import React, {useEffect} from 'react';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {colors, fonts, fontSize, FUNCToast} from '../../utils';
import SectionHeader from './parts/SectionHeader';
import SectionContent from './parts/SectionContent';
import {getDownloads} from '../../redux/actions';
import {useDispatch, useSelector} from 'react-redux';
import {Button, Gap} from '../../components';
import {Modal} from 'react-native-paper';
import {IconFileDownload} from '../../assets';

const Download = () => {
  const {dataDownloads, modalDownload, formDownload} = useSelector(
    state => state.downloadReducers,
  );
  const dispatch = useDispatch();

  const containerStyle = {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
  };

  useEffect(() => {
    dispatch(getDownloads());
  }, []);
  return (
    <>
      <StatusBar
        translucent
        barStyle={'dark-content'}
        backgroundColor="transparent"
      />
      <View style={styles.page}>
        <View style={styles.content}>
          {/* section header */}
          <SectionHeader />
          <ScrollView>
            {/* section inbox */}
            <SectionContent data={dataDownloads} />
          </ScrollView>
        </View>
      </View>
      <Modal
        style={{marginHorizontal: 20, borderRadius: 10}}
        visible={modalDownload}
        onDismiss={() =>
          dispatch({
            type: 'MODAL_DOWNLOAD',
            payload: !modalDownload,
          })
        }
        contentContainerStyle={containerStyle}>
        <ScrollView style={{paddingHorizontal: 5}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{width: '40%'}}>
              <View
                style={{
                  flex: 1,
                  backgroundColor: '#F5F5F5',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 30,
                  borderRadius: 10,
                }}>
                <IconFileDownload />
              </View>
              <Gap height={10} />
              <Text
                style={{
                  fontSize: fontSize.mini,
                  fontFamily: fonts.primary[400],
                  color: colors.grey,
                }}>
                Keterangan : {formDownload.Keterangan}
              </Text>
              <Text
                style={{
                  fontSize: fontSize.mini,
                  fontFamily: fonts.primary[400],
                  color: colors.grey,
                }}>
                Tahun : {formDownload.Tahun}
              </Text>
            </View>
            <View style={{width: '56%'}}>
              <Text
                style={{
                  fontSize: fontSize.small,
                  fontFamily: fonts.primary[600],
                  color: colors.dark,
                }}>
                {formDownload.NamaDokumen}
              </Text>
              <Gap height={5} />
              <Text
                style={{
                  fontSize: fontSize.small,
                  fontFamily: fonts.primary[400],
                  color: colors.dark,
                }}>
                {formDownload.Deskripsi}
              </Text>
            </View>
          </View>
          <Gap height={30} />
          <Button
            type={'primary'}
            title={'Mulai Unduh Data'}
            // onPress={() => FUNCDownloadFile(formDownload.Url)}
            onPress={() => FUNCToast('WARN', {msg: 'File tidak ada'})}
            width={'100%'}
          />
        </ScrollView>
      </Modal>
    </>
  );
};

export default Download;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingTop: getStatusBarHeight() + 20,
  },
});
