import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch} from 'react-redux';
import {getDownloads} from '../../../redux/actions';

const SectionHeader = () => {
  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.textHeader}>Download</Text>
      </View>
      <View>
        <TouchableOpacity
          onPress={() => dispatch(getDownloads('GET_DOWNLOADS'))}>
          <Icon name="refresh" color={colors.blue} size={20} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SectionHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingBottom: 15,
  },
  textHeader: {
    fontSize: fontSize.large,
    fontFamily: fonts.primary[600],
    color: colors.dark,
  },
});
