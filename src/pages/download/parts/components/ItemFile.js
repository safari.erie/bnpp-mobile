import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {IconFileDownload} from '../../../../assets';
import {Gap} from '../../../../components';
import {colors, fonts, fontSize} from '../../../../utils';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const ItemFile = ({name, cat, onPress}) => {
  return (
    <View style={{width: '47%', marginBottom: 15}}>
      <View
        style={{
          flex: 1,
          backgroundColor: '#F5F5F5',
          justifyContent: 'center',
          alignItems: 'center',
          paddingVertical: 30,
          borderRadius: 10,
        }}>
        <IconFileDownload />
      </View>
      <Gap height={10} />
      <TouchableOpacity
        onPress={onPress}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View style={{flex: 1}}>
          <Text
            numberOfLines={1}
            style={{
              fontSize: fontSize.small,
              fontFamily: fonts.primary['normal'],
              color: colors.dark,
              maxWidth: '90%',
            }}>
            {name}
          </Text>
          <Text
            style={{
              fontSize: fontSize.mini,
              fontFamily: fonts.primary[400],
              color: colors.grey,
            }}>
            {cat}
          </Text>
        </View>
        <View>
          <Icon name="download" color={colors.blue} size={20} />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default ItemFile;

const styles = StyleSheet.create({});
