import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const ItemLoading = () => {
  var loading = [];

  for (let i = 0; i < 6; i++) {
    loading.push(
      <SkeletonPlaceholder.Item width={wp('45%')} marginBottom={20} key={i}>
        <SkeletonPlaceholder.Item
          width={wp('45%')}
          height={100}
          borderRadius={10}
        />
        <SkeletonPlaceholder.Item
          width={wp('30%')}
          height={15}
          borderRadius={10}
          marginTop={10}
        />
        <SkeletonPlaceholder.Item
          width={wp('30%')}
          height={10}
          borderRadius={10}
          marginTop={10}
        />
      </SkeletonPlaceholder.Item>,
    );
  }
  return (
    <SkeletonPlaceholder speed={1000}>
      <SkeletonPlaceholder.Item
        flexDirection="row"
        justifyContent="space-between"
        flexWrap="wrap">
        {loading}
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

export default ItemLoading;
