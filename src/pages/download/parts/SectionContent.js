import {StyleSheet, View} from 'react-native';
import React from 'react';
import ItemFile from './components/ItemFile';
import {useDispatch, useSelector} from 'react-redux';
import {setFormDownload} from '../../../redux/actions';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ItemLoading from './components/ItemLoading';

const SectionContent = ({data}) => {
  const {modalDownload} = useSelector(state => state.downloadReducers);
  const dispatch = useDispatch();

  const showModal = v => {
    dispatch(setFormDownload('NamaDokumen', v.NamaDokumen));
    dispatch(setFormDownload('Keterangan', v.Keterangan));
    dispatch(setFormDownload('Deskripsi', v.Deskripsi));
    dispatch(setFormDownload('Tahun', v.Tahun));
    dispatch(setFormDownload('Url', 'https://i.imgur.com/UYiroysl.jpg'));
    dispatch({type: 'MODAL_DOWNLOAD', payload: !modalDownload});
  };

  return (
    <>
      <View style={{paddingHorizontal: 15}}>
        {data.length !== 0 ? (
          <>
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                width: '100%',
                justifyContent: 'space-between',
              }}>
              {data.map((v, i) => {
                return (
                  <ItemFile
                    key={i}
                    name={v.NamaDokumen}
                    cat={v.Keterangan}
                    onPress={() => showModal(v)}
                  />
                );
              })}
            </View>
          </>
        ) : (
          <>
            <View style={{height: hp('80%')}}>
              <ItemLoading />
            </View>
          </>
        )}
      </View>
    </>
  );
};

export default SectionContent;

const styles = StyleSheet.create({});
