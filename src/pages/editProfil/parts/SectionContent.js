import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {colors, fonts, FUNCToast} from '../../../utils';
import {setFormLogin} from '../../../redux/actions';
import {RadioButton} from 'react-native-paper';
import {Gap, LabelTextInput} from '../../../components';

const SectionContent = ({dataUser}) => {
  const {formLogin} = useSelector(state => state.loginReducer);
  const dispatch = useDispatch();
  const onNumberTelepon = (key, text) => {
    let newText = '';
    let numbers = '0123456789';

    for (var i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText = newText + text[i];
      } else {
        FUNCToast('WARN', {msg: 'Wajib Numerik'});
      }
    }
    if (text.length > 13) {
      FUNCToast('WARN', {msg: 'Tidak boleh lebih dari 13 digit'});
      return;
    }
    dispatch(setFormLogin(key, newText));
  };
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={20} />
      <LabelTextInput
        title={'Username'}
        placeholder={'Ketikkan Username...'}
        defaultValue={dataUser?.userProfile.username}
        onChangeText={e => dispatch(setFormLogin('username', e))}
      />
      <LabelTextInput
        title={'Email'}
        placeholder={'Ketikkan Email...'}
        defaultValue={dataUser?.userProfile.email}
        onChangeText={e => dispatch(setFormLogin('email', e))}
      />
      <LabelTextInput
        title={'Nama Lengkap'}
        placeholder={'Ketikkan Nama Lengkap...'}
        defaultValue={dataUser?.userProfile.nama}
        onChangeText={e => dispatch(setFormLogin('nama', e))}
      />
      <LabelTextInput
        title={'Alamat'}
        placeholder={'Ketikkan Alamat...'}
        defaultValue={dataUser?.userProfile.alamat}
        onChangeText={e => dispatch(setFormLogin('alamat', e))}
      />
      <LabelTextInput
        title={'No. Telepon'}
        placeholder={'Ketikkan No. Telepon ...'}
        defaultValue={dataUser?.userProfile.telepon}
        onChangeText={e => onNumberTelepon('telepon', e)}
        value={formLogin.telepon}
      />

      <View style={{paddingHorizontal: 10, paddingTop: 5}}>
        <Text
          style={{
            fontSize: 13,
            fontFamily: fonts.primary[400],
            color: colors.dark,
          }}>
          Jenis Kelamin :
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 10,
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View
              style={{
                borderRadius: 100,
                backgroundColor: colors.greySecondary,
              }}>
              <RadioButton
                value="l"
                color={colors.dark}
                status={formLogin.gender === 'l' ? 'checked' : 'unchecked'}
                onPress={() => dispatch(setFormLogin('gender', 'l'))}
              />
            </View>
            <Text
              style={{
                fontSize: 13,
                fontFamily: fonts.primary[400],
                color: colors.dark,
                paddingHorizontal: 10,
              }}>
              Laki - Laki
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View
              style={{
                borderRadius: 100,
                backgroundColor: colors.greySecondary,
              }}>
              <RadioButton
                value="p"
                color={colors.dark}
                status={formLogin.gender === 'p' ? 'checked' : 'unchecked'}
                onPress={() => dispatch(setFormLogin('gender', 'p'))}
              />
            </View>
            <Text
              style={{
                fontSize: 13,
                fontFamily: fonts.primary[400],
                color: colors.dark,
                paddingHorizontal: 10,
              }}>
              Perempuan
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default SectionContent;

const styles = StyleSheet.create({});
