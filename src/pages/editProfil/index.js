import {StyleSheet, Text, View, StatusBar} from 'react-native';
import React, {useEffect, useState} from 'react';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {colors, fonts, FUNCToast} from '../../utils';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Button, Gap, Header, LabelTextInput} from '../../components';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import {useDispatch, useSelector} from 'react-redux';
import {apiEditProfil, setFormLogin} from '../../redux/actions';
import {RadioButton} from 'react-native-paper';
import SectionContent from './parts/SectionContent';

const EditProfil = ({navigation}) => {
  const [dataUser, setDataUser] = useState(null);

  const {formLogin} = useSelector(state => state.loginReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    const _validasiSession = async () => {
      try {
        const session = await AsyncStorageLib.getItem('dataUser');
        setDataUser(session !== null && JSON.parse(session));
        dispatch(setFormLogin('gender', dataUser.userProfile.gender));
      } catch (error) {}
    };
    _validasiSession();
  }, []);

  const onNumberTelepon = (key, text) => {
    let newText = '';
    let numbers = '0123456789';

    for (var i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText = newText + text[i];
      } else {
        FUNCToast('WARN', {msg: 'Wajib Numerik'});
      }
    }
    if (text.length > 13) {
      FUNCToast('WARN', {msg: 'Tidak boleh lebih dari 13 digit'});
      return;
    }
    dispatch(setFormLogin(key, newText));
  };

  return (
    <>
      <StatusBar
        translucent
        barStyle={'dark-content'}
        backgroundColor="transparent"
      />
      <View style={styles.page}>
        <View style={styles.content}>
          {/* section header */}
          <Header
            title={'Edit Profil'}
            iconLeft={
              <Icon name="keyboard-backspace" color={colors.dark} size={25} />
            }
            onPressLeft={() => navigation.goBack()}
            iconRight={<Icon name="check" color={colors.bluelight} size={25} />}
            onPressRight={() =>
              dispatch(apiEditProfil(dataUser.iduser, formLogin, navigation))
            }
          />
          <ScrollView>
            {/* section content */}
            <SectionContent dataUser={dataUser} />
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default EditProfil;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    paddingTop: getStatusBarHeight() + 20,
  },
});
