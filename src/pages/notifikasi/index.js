import {StyleSheet, View, StatusBar} from 'react-native';
import React from 'react';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {colors} from '../../utils';
import {ScrollView} from 'react-native-gesture-handler';
import SectionHeader from './parts/SectionHeader';
import SectionContent from './parts/SectionContent';

const Notifikasi = ({navigation}) => {
  return (
    <>
      <StatusBar
        translucent
        barStyle={'dark-content'}
        backgroundColor="transparent"
      />
      <View style={styles.page}>
        <View style={styles.content}>
          {/* section header */}
          <SectionHeader />
          <ScrollView>
            {/* section inbox */}
            <SectionContent navigation={navigation} />
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default Notifikasi;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingTop: getStatusBarHeight() + 20,
  },
});
