import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../../../utils';
import {Gap} from '../../../../components';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const TitikMerah = () => {
  return (
    <View
      style={{
        position: 'absolute',
        top: -1,
        right: -1,
        width: 10,
        height: 10,
        borderRadius: 10,
        backgroundColor: colors.red,
      }}
    />
  );
};

const ItemNotif = ({title, desc, time, onPress, isRead = true}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        flexDirection: 'row',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#F5F5F5',
        padding: 20,
        marginBottom: 10,
      }}>
      {!isRead && <TitikMerah />}

      <View style={{paddingRight: 20}}>
        <View
          style={{
            width: 40,
            height: 40,
            borderRadius: 40,
            backgroundColor: colors.blue,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon name="inbox-full-outline" color={colors.white} size={20} />
        </View>
      </View>
      <View style={{flex: 1}}>
        <Text
          style={{
            fontSize: fontSize.small,
            fontFamily: fonts.primary[600],
            color: colors.dark,
          }}>
          {title}
        </Text>
        <Gap height={2} />
        <Text
          numberOfLines={2}
          style={{
            fontSize: fontSize.small,
            fontFamily: fonts.primary[400],
            color: colors.dark,
          }}>
          {desc}
        </Text>
        <Gap height={6} />
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Icon name="clock-outline" color={colors.grey} size={15} />
          <Text
            style={{
              fontSize: fontSize.mini,
              fontFamily: fonts.primary[400],
              color: colors.grey,
              paddingLeft: 5,
            }}>
            {time}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ItemNotif;

const styles = StyleSheet.create({});
