import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {TouchableOpacity} from 'react-native-gesture-handler';

const SectionHeader = () => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.textHeader}>Notifikasi</Text>
      </View>
      <View>
        <TouchableOpacity>
          <Icon name="refresh" color={colors.blue} size={20} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SectionHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingBottom: 15,
  },
  textHeader: {
    fontSize: fontSize.large,
    fontFamily: fonts.primary[600],
    color: colors.dark,
  },
});
