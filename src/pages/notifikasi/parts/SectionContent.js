import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import ItemNotif from './components/ItemNotif';

const SectionContent = ({navigation}) => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <ItemNotif
        title={'Login Terdeteksi'}
        desc={
          'Baru-baru ini ada yang menggunakan akun anda, apabila bukan anda silahkan ganti password sekarang juga !'
        }
        time={'22-03-2022 17:00'}
        onPress={() => navigation.navigate('DetailNotifikasi')}
        isRead={false}
      />
      <ItemNotif
        title={'Login Terdeteksi'}
        desc={
          'Baru-baru ini ada yang menggunakan akun anda, apabila bukan anda silahkan ganti password sekarang juga !'
        }
        time={'22-03-2022 17:00'}
        onPress={() => alert('fire')}
        isRead={false}
      />
      <ItemNotif
        title={'Login Terdeteksi'}
        desc={
          'Baru-baru ini ada yang menggunakan akun anda, apabila bukan anda silahkan ganti password sekarang juga !'
        }
        time={'22-03-2022 17:00'}
        onPress={() => alert('fire')}
      />
      <ItemNotif
        title={'Login Terdeteksi'}
        desc={
          'Baru-baru ini ada yang menggunakan akun anda, apabila bukan anda silahkan ganti password sekarang juga !'
        }
        time={'22-03-2022 17:00'}
        onPress={() => alert('fire')}
      />
      <ItemNotif
        title={'Login Terdeteksi'}
        desc={
          'Baru-baru ini ada yang menggunakan akun anda, apabila bukan anda silahkan ganti password sekarang juga !'
        }
        time={'22-03-2022 17:00'}
        onPress={() => alert('fire')}
      />
      <ItemNotif
        title={'Login Terdeteksi'}
        desc={
          'Baru-baru ini ada yang menggunakan akun anda, apabila bukan anda silahkan ganti password sekarang juga !'
        }
        time={'22-03-2022 17:00'}
        onPress={() => alert('fire')}
      />
      <ItemNotif
        title={'Login Terdeteksi'}
        desc={
          'Baru-baru ini ada yang menggunakan akun anda, apabila bukan anda silahkan ganti password sekarang juga !'
        }
        time={'22-03-2022 17:00'}
        onPress={() => alert('fire')}
      />
      <ItemNotif
        title={'Login Terdeteksi'}
        desc={
          'Baru-baru ini ada yang menggunakan akun anda, apabila bukan anda silahkan ganti password sekarang juga !'
        }
        time={'22-03-2022 17:00'}
        onPress={() => alert('fire')}
      />
    </View>
  );
};

export default SectionContent;

const styles = StyleSheet.create({});
