import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {colors, fonts} from '../../utils';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Gap, Header} from '../../components';
const DetailNotifikasi = ({navigation}) => {
  return (
    <>
      <StatusBar
        translucent
        barStyle={'dark-content'}
        backgroundColor="transparent"
      />
      <View style={styles.page}>
        <View style={styles.content}>
          {/* SECTION HEADER */}
          <Header
            title={'Baca Notifikasi'}
            iconLeft={
              <Icon name="keyboard-backspace" color={colors.dark} size={25} />
            }
            onPressLeft={() => navigation.goBack()}
          />
          <ScrollView>
            {/* SECTION CONTENT */}
            <View style={{padding: 15}}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: fonts.primary[600],
                  color: colors.dark,
                }}>
                Login Terdeteksi Pada aplikasi bnpp app
              </Text>

              <Gap height={5} />
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon name="clock-outline" color={colors.grey} size={15} />
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: fonts.primary[400],
                    color: colors.grey,
                    paddingLeft: 5,
                  }}>
                  22-06-2022 17:40
                </Text>
              </View>
              <Gap height={15} />
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: fonts.primary[400],
                  color: colors.dark,
                }}>
                Baru-baru ini ada yang menggunakan akun anda, apabila bukan anda
                silahkan ganti password sekarang juga !
              </Text>
            </View>
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default DetailNotifikasi;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    paddingTop: getStatusBarHeight() + 20,
  },
});
