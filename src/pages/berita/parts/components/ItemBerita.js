import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Gap} from '../../../../components';
import {colors, fonts, fontSize} from '../../../../utils';

const ItemBerita = ({desc, image, date, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 10,
        padding: 15,
        borderWidth: 1,
        borderColor: '#F5F5F5',
        marginBottom: 10,
      }}>
      <View
        style={{
          flex: 1,
          paddingRight: 10,
          justifyContent: 'space-between',
        }}>
        <View style={{paddingBottom: 10}}>
          <Text
            numberOfLines={2}
            style={{
              fontSize: fontSize.small,
              fontFamily: fonts.primary['normal'],
              color: colors.dark,
            }}>
            {desc}
          </Text>
          <Text
            style={{
              fontSize: fontSize.mini,
              fontFamily: fonts.primary[400],
              color: colors.dark,
            }}>
            {date}
          </Text>
        </View>
        <Text
          style={{
            fontSize: fontSize.small,
            fontFamily: fonts.primary[600],
            color: colors.blue,
          }}>
          Baca selanjutnya
        </Text>
      </View>
      <View style={{width: 140, height: 80}}>
        <Image
          source={{uri: image}}
          style={{
            width: undefined,
            height: undefined,
            resizeMode: 'cover',
            flex: 1,
            borderRadius: 10,
          }}
        />
      </View>
    </TouchableOpacity>
  );
};

export default ItemBerita;

const styles = StyleSheet.create({});
