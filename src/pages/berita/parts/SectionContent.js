import {FlatList, StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import ItemBerita from './components/ItemBerita';
import {useDispatch, useSelector} from 'react-redux';
import {getBeritas} from '../../../redux/actions';
import {Gap} from '../../../components';
import {FUNCIndoDate} from '../../../utils';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const SectionContent = ({navigation}) => {
  const {dataBeritas} = useSelector(state => state.beritaReducers);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBeritas());
  }, []);

  return (
    <>
      {dataBeritas.length !== 0 ? (
        <FlatList
          data={dataBeritas}
          ListHeaderComponent={() => {
            return <Gap height={20} />;
          }}
          renderItem={({item}) => {
            return (
              <View style={{paddingHorizontal: 15}}>
                <ItemBerita
                  desc={item.judul}
                  image={item.fullImageUrl}
                  date={FUNCIndoDate(item.created)}
                  onPress={() =>
                    navigation.navigate('BeritaDetail', {id: item.id})
                  }
                />
              </View>
            );
          }}
        />
      ) : (
        <View style={{paddingHorizontal: 15, paddingVertical: 20}}>
          <LoadingSkeleton />
        </View>
      )}
    </>
  );
};

const LoadingSkeleton = () => {
  var loading = [];

  for (let i = 0; i < 10; i++) {
    loading.push(
      <SkeletonPlaceholder.Item
        key={i}
        flexDirection="row"
        justifyContent="space-between"
        marginBottom={i === 10 ? 0 : 20}>
        <SkeletonPlaceholder.Item>
          <SkeletonPlaceholder.Item
            width={wp('48%')}
            height={10}
            borderRadius={10}
            marginBottom={5}
            marginTop={10}
          />
          <SkeletonPlaceholder.Item
            width={wp('48%')}
            height={10}
            borderRadius={10}
            marginBottom={5}
          />
          <SkeletonPlaceholder.Item
            width={wp('48%')}
            height={10}
            borderRadius={10}
            marginBottom={25}
          />
          <SkeletonPlaceholder.Item
            width={wp('40%')}
            height={15}
            borderRadius={10}
          />
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item
          width={wp('40%')}
          height={100}
          borderRadius={10}
        />
      </SkeletonPlaceholder.Item>,
    );
  }
  return <SkeletonPlaceholder speed={1000}>{loading}</SkeletonPlaceholder>;
};

export default SectionContent;

const styles = StyleSheet.create({});
