import {StyleSheet, View, StatusBar} from 'react-native';
import React from 'react';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {colors} from '../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Header} from '../../components';
import SectionContent from './parts/SectionContent';

const BeritaList = ({navigation}) => {
  return (
    <>
      <StatusBar
        translucent
        barStyle={'light-content'}
        backgroundColor={colors.black}
      />
      <View style={styles.page}>
        {/* section header */}
        <View style={styles.header}>
          <Header
            title={'Berita'}
            iconLeft={
              <Icon name="keyboard-backspace" color={colors.white} size={25} />
            }
            onPressLeft={() => navigation.goBack()}
            type={'secondary'}
          />
        </View>
        {/* section content */}
        <View style={styles.content}>
          <SectionContent navigation={navigation} />
        </View>
      </View>
    </>
  );
};

export default BeritaList;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  header: {
    backgroundColor: colors.black,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingTop: getStatusBarHeight() + 20,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});
