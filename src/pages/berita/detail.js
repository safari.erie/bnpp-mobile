import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  Image,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {colors, fonts, fontSize, FUNCIndoDate} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Gap, Header} from '../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import {getBeritaDetail} from '../../redux/actions';

const BeritaDetail = ({route, navigation}) => {
  const {id} = route.params;
  const {formBerita} = useSelector(state => state.beritaReducers);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBeritaDetail(id));
  }, []);
  return (
    <>
      <StatusBar
        translucent
        barStyle={'light-content'}
        backgroundColor={colors.black}
      />
      <View style={styles.page}>
        <View style={styles.header}>
          <Header
            title={'Baca Berita'}
            iconLeft={
              <Icon name="keyboard-backspace" color={colors.white} size={25} />
            }
            onPressLeft={() => navigation.goBack()}
            iconRight={
              <Icon
                name="share-variant-outline"
                color={colors.white}
                size={25}
              />
            }
            type={'secondary'}
          />
        </View>
        <View style={styles.content}>
          <ScrollView>
            {/* section judul */}
            <View style={{paddingHorizontal: 15}}>
              <Gap height={20} />
              <Text
                style={{
                  fontSize: fontSize.large,
                  fontFamily: fonts.primary[600],
                  color: colors.dark,
                  textAlign: 'center',
                }}>
                {formBerita.judul}
              </Text>
              <Gap height={10} />
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon
                  name="clock-time-four-outline"
                  color={colors.grey}
                  size={15}
                />
                <Text
                  style={{
                    fontSize: fontSize.mini,
                    fontFamily: fonts.primary['normal'],
                    color: colors.grey,
                    textAlign: 'center',
                    paddingLeft: 5,
                  }}>
                  {formBerita.created} | oleh : {formBerita.email || 'admin'}
                </Text>
              </View>
            </View>

            {/* section image */}
            <View style={{paddingHorizontal: 15, alignItems: 'center'}}>
              <Gap height={20} />
              <View
                style={{
                  width: '95%',
                  height: 200,
                }}>
                <Image
                  source={{uri: formBerita.fullImageUrl}}
                  style={{
                    width: undefined,
                    height: undefined,
                    resizeMode: 'cover',
                    flex: 1,
                    borderRadius: 10,
                  }}
                />
              </View>
            </View>

            {/* section poster */}
            <View style={{paddingHorizontal: 15}}>
              <Gap height={20} />
              <Text
                style={{
                  fontSize: fontSize.small,
                  fontFamily: fonts.primary[400],
                  color: colors.dark,
                  textAlign: 'justify',
                  paddingLeft: 5,
                }}>
                {formBerita.poster}
              </Text>
            </View>
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default BeritaDetail;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  header: {
    backgroundColor: colors.black,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingTop: getStatusBarHeight() + 20,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});
