import React, {useCallback, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  Animated,
  RefreshControl,
  SafeAreaView,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {colors} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Gap} from '../../components';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import SectionHeader from './parts/SectionHeader';
import SectionHero from './parts/SectionHero';
import SectionStatik from './parts/SectionStatik';
import SectionBanner from './parts/SectionBanner';
import SectionBerita from './parts/SectionBerita';
import SectionGrafik from './parts/SectionGrafik';
import {getHomes} from '../../redux/actions';

const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

const Home = ({navigation}) => {
  const [fadeAnim] = useState(new Animated.Value(0));
  const {dataHomes} = useSelector(state => state.homeReducers);
  const dispatch = useDispatch();
  const [refreshing, setRefreshing] = useState(false);
  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 1000,
    }).start();
    dispatch(getHomes());

    console.log('getStatusBarHeight()', getStatusBarHeight());
    console.log('StatusBar.currentHeight()', StatusBar.currentHeight);
  }, [dispatch]);

  const onRefresh = useCallback(() => {
    dispatch(getHomes());
    wait(2000).then(() => setRefreshing(false));
  }, []);

  return (
    <>
      <StatusBar
        translucent
        barStyle={'dark-content'}
        backgroundColor="transparent"
      />
      <SafeAreaView style={styles.page}>
        <View style={styles.container}>
          <View style={styles.content}>
            <Animated.View style={styles.animate(fadeAnim)}>
              {/* section header */}
              <SectionHeader navigation={navigation} />

              {/* section content */}
              <ScrollView
                showsVerticalScrollIndicator={false}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                  />
                }>
                {/* section hero */}
                <SectionHero />

                {/* section statik */}
                <SectionStatik data={dataHomes?.countWilayah} />

                {/* section banner */}
                <SectionBanner
                  data={dataHomes?.banners}
                  navigation={navigation}
                />

                {/* section berita */}
                <SectionBerita
                  data={dataHomes?.dataBeritas}
                  navigation={navigation}
                />

                {/* section grafik */}
                {dataHomes?.graph && <SectionGrafik datax={dataHomes?.graph} />}

                <Gap height={hp('8%')} />
              </ScrollView>
            </Animated.View>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Home;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
  },
  container: {
    backgroundColor: colors.black,
    flex: 1,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  animate: fadeAnim => ({
    opacity: fadeAnim,
  }),
  banner_wrapper: {
    width: '100%',
    height: 150,
    backgroundColor: colors.white,
    padding: 0,
  },
  banner_container: {
    width: '100%',
    height: '100%',
  },
});
