import {StyleSheet, Text, View, Platform, Image} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Logo} from '../../../assets';
import {colors, fonts, fontSize} from '../../../utils';
import {TouchableOpacity} from 'react-native-gesture-handler';

const SectionHeader = ({navigation}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingBottom: 20,
        borderBottomWidth: 0.3,
        borderBottomColor: colors.greySecondary,
      }}>
      <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
        <Icon name="format-list-checkbox" color={colors.dark} size={25} />
      </TouchableOpacity>
      <View style={{paddingHorizontal: 15}}>
        <View style={{width: 35, height: 35}}>
          <Image
            source={Logo}
            style={{
              width: undefined,
              height: undefined,
              resizeMode: 'cover',
              flex: 1,
            }}
          />
        </View>
      </View>
      <View>
        <Text
          style={{
            fontSize: fontSize.large,
            fontFamily: fonts.primary[600],
            color: colors.dark,
          }}>
          BNPP APPS
        </Text>
      </View>
    </View>
  );
};

export default SectionHeader;

const styles = StyleSheet.create({});
