import {StyleSheet, View, Dimensions, Text} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  VictoryChart,
  VictoryBar,
  VictoryTheme,
  VictoryGroup,
  VictoryAxis,
} from 'victory-native';
import {Gap} from '../../../components';
import {colors, fonts, fontSize} from '../../../utils';
import {ScrollView} from 'react-native-gesture-handler';

const Header = () => {
  const jsonDatas = [
    {
      color: '#3AB0FF',
      title: 'LOKPRI',
    },
    {
      color: '#FFB562',
      title: 'PKSN',
    },
    {
      color: '#F87474',
      title: 'PPKT',
    },
  ];
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
      }}>
      <View>
        <Text
          style={{
            fontSize: fontSize.medium,
            fontFamily: fonts.primary[600],
            color: colors.dark,
          }}>
          Grafik
        </Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        {jsonDatas.map((v, i) => {
          return (
            <View
              key={i}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingRight: 10,
              }}>
              <View
                style={{
                  width: 10,
                  height: 10,
                  borderRadius: 10,
                  backgroundColor: v.color,
                }}
              />
              <Text
                style={{
                  fontSize: fontSize.mini,
                  fontFamily: fonts.primary['normal'],
                  color: colors.dark,
                  paddingLeft: 10,
                }}>
                {v.title}
              </Text>
            </View>
          );
        })}
      </View>
    </View>
  );
};

const SectionGrafik = ({datax}) => {
  const [countData, setCountData] = useState(null);

  useEffect(() => {
    var lokpri = [];
    var pksn = [];
    var ppkt = [];
    datax.label.forEach((v, i) => {
      datax.lokpri.map((vx, ix) => {
        if (i == ix) {
          if (i < 15)
            lokpri.push({
              x: v,
              y: 5 + vx,
            });
        }
      });
      datax.pksn.map((vx, ix) => {
        if (i == ix) {
          if (i < 15)
            pksn.push({
              x: v,
              y: 5 + vx,
            });
        }
      });
      datax.ppkt.map((vx, ix) => {
        if (i == ix) {
          if (i < 15)
            ppkt.push({
              x: v,
              y: 5 + vx,
            });
        }
      });
    });
    setCountData({
      lokpri,
      pksn,
      ppkt,
    });
  }, []);

  return (
    <>
      <View style={{paddingHorizontal: 0}}>
        <Gap height={15} />
        <View style={{width: '100%', height: 10, backgroundColor: '#FAFAFA'}} />
        <Gap height={20} />
        <Header />
        <Gap height={15} />
        {countData !== null ? (
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <VictoryChart
              theme={VictoryTheme.grayscale}
              width={1700}
              padding={{top: 20, bottom: 60, left: 20, right: 20}}>
              <VictoryGroup offset={25}>
                <VictoryBar
                  data={countData.lokpri}
                  style={{data: {fill: '#3AB0FF'}}}
                />
                <VictoryBar
                  data={countData.pksn}
                  style={{data: {fill: '#FFB562'}}}
                />
                <VictoryBar
                  data={countData.ppkt}
                  style={{data: {fill: '#F87474'}}}
                />
              </VictoryGroup>
              <VictoryAxis
                style={{
                  axis: {stroke: colors.dark},
                  axisLabel: {fontSize: 16},
                  ticks: {stroke: '#ccc'},
                  tickLabels: {
                    fontSize: fontSize.mini - 2,
                    fill: colors.dark,
                    fontFamily: fonts.primary[400],
                    angle: -1,
                  },
                }}
              />
            </VictoryChart>
          </ScrollView>
        ) : (
          <></>
        )}
      </View>
    </>
  );
};

export default SectionGrafik;
