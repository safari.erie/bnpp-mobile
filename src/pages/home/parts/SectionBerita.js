import {StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';
import {Gap} from '../../../components';
import {colors, fonts, fontSize, FUNCIndoDate} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ItemBerita from './components/ItemBerita';
import {TouchableOpacity} from 'react-native-gesture-handler';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const SectionBerita = ({data, navigation}) => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={10} />
      <View style={{width: '100%', height: 10, backgroundColor: '#FAFAFA'}} />
      <Gap height={10} />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View>
          <Text
            style={{
              fontSize: fontSize.medium,
              fontFamily: fonts.primary[600],
              color: colors.dark,
            }}>
            News
          </Text>
          <Text
            style={{
              fontSize: fontSize.small,
              fontFamily: fonts.primary[400],
              color: colors.dark,
            }}>
            Berita Terbaru
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.push('BeritaList')}
          style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text
            style={{
              fontSize: fontSize.small,
              fontFamily: fonts.primary[600],
              color: colors.blue,
              paddingRight: 10,
            }}>
            Selengkapnya
          </Text>
          <Icon name="trending-neutral" color={colors.blue} size={20} />
        </TouchableOpacity>
      </View>
      <Gap height={15} />
      {data !== undefined ? (
        data.map((v, i) => {
          return (
            <ItemBerita
              key={i}
              desc={v.judul}
              image={v.fileImage}
              date={FUNCIndoDate(v.created)}
              onPress={() => navigation.navigate('BeritaDetail', {id: v.id})}
            />
          );
        })
      ) : (
        <View>
          <LoadingSkeleton />
        </View>
      )}
      <Gap height={10} />
    </View>
  );
};

const LoadingSkeleton = () => {
  var loading = [];

  for (let i = 0; i < 3; i++) {
    loading.push(
      <SkeletonPlaceholder.Item
        key={i}
        flexDirection="row"
        justifyContent="space-between"
        marginBottom={i === 3 ? 0 : 20}>
        <SkeletonPlaceholder.Item>
          <SkeletonPlaceholder.Item
            width={wp('48%')}
            height={10}
            borderRadius={10}
            marginBottom={5}
          />
          <SkeletonPlaceholder.Item
            width={wp('48%')}
            height={10}
            borderRadius={10}
            marginBottom={5}
          />
          <SkeletonPlaceholder.Item
            width={wp('48%')}
            height={10}
            borderRadius={10}
            marginBottom={35}
          />
          <SkeletonPlaceholder.Item
            width={wp('40%')}
            height={15}
            borderRadius={10}
          />
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item
          width={wp('40%')}
          height={100}
          borderRadius={10}
        />
      </SkeletonPlaceholder.Item>,
    );
  }
  return <SkeletonPlaceholder speed={1000}>{loading}</SkeletonPlaceholder>;
};

export default SectionBerita;

const styles = StyleSheet.create({});
