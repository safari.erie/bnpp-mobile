import React, {useRef, useState} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {colors, fonts, fontSize} from '../../../utils';
import {Gap} from '../../../components';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {TouchableRipple} from 'react-native-paper';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const SectionBanner = ({data, navigation}) => {
  const bannerRef = useRef(null);
  const [activeSlide, setActiveSlide] = useState(0);

  const bannerItem = ({item}, parallaxProps) => {
    return (
      <TouchableRipple
        onPress={() =>
          navigation.navigate('PreviewImage', {link: item.fileGambar})
        }
        borderless={true}
        rippleColor={'rgb(152 152 152 / 24%)'}
        style={{
          borderRadius: 10,
        }}>
        <View style={styles.banner_wrapper}>
          <View style={styles.banner_container}>
            <Image
              source={{uri: `${item.fileGambar}`}}
              style={{
                width: undefined,
                height: undefined,
                flex: 1,
                resizeMode: 'cover',
                borderRadius: 10,
              }}
            />
          </View>
        </View>
      </TouchableRipple>
    );
  };

  return (
    <View>
      <Gap height={10} />
      <View style={{width: '100%', height: 10, backgroundColor: '#FAFAFA'}} />
      <Gap height={10} />
      <Text
        style={{
          fontSize: fontSize.medium,
          fontFamily: fonts.primary[600],
          color: colors.dark,
          paddingHorizontal: 15,
        }}>
        Banner
      </Text>
      <Gap height={15} />
      {data !== undefined ? (
        <>
          <Carousel
            ref={bannerRef}
            sliderWidth={wp('100%')}
            sliderHeight={wp('100%')}
            itemWidth={wp('100%') - 60}
            data={data}
            renderItem={bannerItem}
            hasParallaxImages={true}
            onSnapToItem={index => setActiveSlide(index)}
            loop
          />
          <Gap height={15} />

          <View style={{alignItems: 'center'}}>
            <Pagination
              dotsLength={data.length}
              activeDotIndex={activeSlide}
              containerStyle={{
                paddingVertical: 2,
                justifyContent: 'flex-start',
              }}
              dotContainerStyle={{
                marginHorizontal: 3,
              }}
              dotStyle={{
                width: 10,
                height: 10,
                borderRadius: 10,
                backgroundColor: colors.bluelight,
              }}
              inactiveDotStyle={{
                backgroundColor: colors.grey,
              }}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.5}
            />
          </View>
        </>
      ) : (
        <LoadingSkeleton />
      )}
      <Gap height={10} />
    </View>
  );
};

const LoadingSkeleton = () => {
  return (
    <SkeletonPlaceholder speed={1000}>
      <SkeletonPlaceholder.Item flexDirection="row" justifyContent="center">
        <SkeletonPlaceholder.Item
          width={wp('70%')}
          height={150}
          borderRadius={10}
          marginHorizontal={8}
        />
        <SkeletonPlaceholder.Item
          width={wp('70%')}
          height={150}
          borderRadius={10}
          marginHorizontal={8}
        />
        <SkeletonPlaceholder.Item
          width={wp('70%')}
          height={150}
          borderRadius={10}
          marginHorizontal={8}
        />
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

export default SectionBanner;

const styles = StyleSheet.create({
  banner_wrapper: {
    width: '100%',
    height: 150,
    backgroundColor: colors.white,
    padding: 0,
  },
  banner_container: {
    width: '100%',
    height: '100%',
  },
});
