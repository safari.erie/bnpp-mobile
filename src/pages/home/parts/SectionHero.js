import {StyleSheet, Text, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import {colors, fonts, fontSize} from '../../../utils';
import {Gap} from '../../../components';
import {ILHero} from '../../../assets/illustration';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const SectionHero = () => {
  const [selamat, setSelamat] = useState('');
  const selamatHari = () => {
    var c = new Date().getHours();
    if (c > 0 && c < 10) {
      setSelamat('Selamat Pagi');
    } else if (c >= 0 && c < 10) {
      setSelamat('Selamat Pagi');
    } else if (c >= 10 && c < 16) {
      setSelamat('Selamat Siang');
    } else if (c >= 16 && c < 18) {
      setSelamat('Selamat Sore');
    } else if (c >= 18 && c < 19) {
      setSelamat('Selamat Sore');
    } else if (c > 19 && c < 24) {
      setSelamat('Selamat Malam');
    }
  };
  useEffect(() => {
    selamatHari();
  }, []);

  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={10} />
      <View
        style={{
          backgroundColor: colors.blue,
          borderRadius: 10,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View style={{flex: 1}}>
          <View style={{paddingHorizontal: 20, paddingVertical: 30}}>
            <Text
              style={{
                fontSize: fontSize.medium,
                fontFamily: fonts.primary[600],
                color: colors.white,
              }}>
              Hai, {selamat} !
            </Text>
            <Gap height={5} />
            <Text
              numberOfLines={2}
              style={{
                fontSize: fontSize.small,
                fontFamily: fonts.primary[400],
                color: colors.white,
              }}>
              Semoga harimu selalu menyenangkan
            </Text>
          </View>
        </View>
        <View>
          <ILHero width={wp('35%')} height={hp('15%')} />
        </View>
      </View>
      {/* <View
        style={{
          flexDirection: 'row',
          right: 20,
          position: 'absolute',
          top: 10,
        }}>
        <View>
          <ILHero height={150} />
        </View>
      </View> */}
    </View>
  );
};

export default SectionHero;

const styles = StyleSheet.create({});
