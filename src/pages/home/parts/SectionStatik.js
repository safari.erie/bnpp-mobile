import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Gap} from '../../../components';
import {colors, fonts, fontSize} from '../../../utils';
import ItemStatik from './components/ItemStatik';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const SectionStatik = ({data}) => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={20} />
      <Text
        style={{
          fontSize: fontSize.medium,
          fontFamily: fonts.primary[600],
          color: colors.dark,
        }}>
        Statistik
      </Text>
      <Gap height={10} />
      {data !== undefined ? (
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            width: '100%',
            flexWrap: 'wrap',
            alignItems: 'center',
          }}>
          <ItemStatik
            name={'Provinsi'}
            count={data[0].provinsi}
            width={'31%'}
            bgcolor={'#3AB0FF'}
            icon={
              <Icon name="map-outline" color={colors.white} size={wp('8%')} />
            }
          />
          <ItemStatik
            name={'Kota'}
            count={data[1].Kota}
            width={'31%'}
            bgcolor={'#FFB562'}
            icon={
              <Icon
                name="home-city-outline"
                color={colors.white}
                size={wp('8%')}
              />
            }
          />
          <ItemStatik
            name={'Kecamatan'}
            count={data[2].kecamatan}
            width={'31%'}
            bgcolor={'#F87474'}
            icon={
              <Icon name="home-group" color={colors.white} size={wp('8%')} />
            }
          />
          <ItemStatik
            name={'Desa'}
            count={data[3].Desa}
            width={'48%'}
            bgcolor={'#11A887'}
            icon={
              <Icon
                name="image-filter-hdr"
                color={colors.white}
                size={wp('8%')}
              />
            }
          />
          <ItemStatik
            name={'Lokpri'}
            count={222}
            width={'48%'}
            bgcolor={'#AD8B73'}
            icon={
              <Icon name="google-maps" color={colors.white} size={wp('8%')} />
            }
          />
        </View>
      ) : (
        <LoadingSkeleton />
      )}
      <Gap height={10} />
    </View>
  );
};

const LoadingSkeleton = () => {
  return (
    <SkeletonPlaceholder speed={1000}>
      <SkeletonPlaceholder.Item
        flexDirection="row"
        justifyContent="space-between"
        flexWrap="wrap">
        <SkeletonPlaceholder.Item
          width={wp('29%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('29%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('29%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('45%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('45%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

export default SectionStatik;

const styles = StyleSheet.create({});
