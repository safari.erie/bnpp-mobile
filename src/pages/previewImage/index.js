import React from 'react';
import {
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  View,
  StatusBar,
} from 'react-native';
import {colors, FUNCDownloadFile} from '../../utils';
import ImageZoom from 'react-native-image-pan-zoom';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Button} from '../../components';

const PreviewImage = ({navigation, route}) => {
  const {link} = route.params;
  return (
    <>
      <StatusBar
        translucent
        barStyle={'light-content'}
        backgroundColor="transparent"
      />
      <View style={{flex: 1, backgroundColor: colors.black}}>
        <ImageZoom
          cropWidth={Dimensions.get('window').width}
          cropHeight={Dimensions.get('window').height}
          imageWidth={wp('100%')}
          imageHeight={hp('100%')}
          style={{backgroundColor: 'transparent'}}>
          <Image
            style={{
              width: undefined,
              height: undefined,
              flex: 1,
              resizeMode: 'contain',
            }}
            source={{uri: link}}
          />
        </ImageZoom>
        <View
          style={{
            width: wp('100%'),
            position: 'absolute',
            top: 0,
            padding: 15,
            marginTop: getStatusBarHeight(),
          }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon name="keyboard-backspace" color={colors.white} size={25} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: wp('100%'),
            // height: 80,
            padding: 20,
            // backgroundColor: 'transparent',
            // justifyContent: 'center',
            position: 'absolute',
            bottom: 0,
          }}>
          <Button
            type={'primary'}
            title={'Download'}
            onPress={() => FUNCDownloadFile(link)}
            width={'100%'}
          />
        </View>
      </View>
    </>
  );
};

export default PreviewImage;

const styles = StyleSheet.create({});
