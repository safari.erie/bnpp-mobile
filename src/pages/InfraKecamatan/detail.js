import {StyleSheet, StatusBar, View, ScrollView} from 'react-native';
import React, {useEffect} from 'react';
import {colors} from '../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Gap, Header} from '../../components';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import SectionStatik from './parts/Detail/SectionStatik';
import SectionDetail from './parts/Detail/SectionDetail';
import SectionHero from './parts/Detail/SectionHero';
import SectionMenu from './parts/Detail/SectionMenu';
import SectionBannerCamat from './parts/Detail/SectionBannerCamat';
import SectionInfoCamat from './parts/Detail/SectionInfoCamat';
import {useDispatch, useSelector} from 'react-redux';
import {getInfraKecamatan} from '../../redux/actions';

const InfraKecamatanDetail = ({navigation, route}) => {
  const {PARAMId} = route.params;
  const {dataInfraKecamatan} = useSelector(
    state => state.infraKecamatanReducers,
  );
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getInfraKecamatan(PARAMId));
  }, [dispatch]);

  return (
    <>
      <StatusBar
        translucent
        barStyle={'light-content'}
        backgroundColor={colors.black}
      />
      <View style={styles.page}>
        {/* section header */}
        <View style={styles.header}>
          <Header
            title={'Detail Kecamatan'}
            iconLeft={
              <Icon name="keyboard-backspace" color={colors.white} size={25} />
            }
            onPressLeft={() => navigation.goBack()}
            iconRight={
              <Icon name="pencil-outline" color={colors.white} size={25} />
            }
            onPressRight={() =>
              navigation.navigate('InfraKecamatanDetailInfoKec', {PARAMId})
            }
            type={'secondary'}
          />
        </View>
        {/* section content */}
        <View style={styles.content}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {/* section hero */}
            <SectionHero data={dataInfraKecamatan.kec} />

            {/* section detail */}
            <SectionDetail data={dataInfraKecamatan.detail} />

            {/* section statik */}
            <SectionStatik data={dataInfraKecamatan.kec?.detail} />

            {/* section menu */}
            <SectionMenu
              navigation={navigation}
              idKec={PARAMId}
              data={dataInfraKecamatan.camat}
            />

            {/* section banner camat */}
            <SectionBannerCamat data={dataInfraKecamatan.camat} />

            {/* section camat ingfo */}
            <SectionInfoCamat data={dataInfraKecamatan.camat} />

            <Gap height={50} />
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default InfraKecamatanDetail;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  header: {
    backgroundColor: colors.black,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingTop: getStatusBarHeight() + 20,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});
