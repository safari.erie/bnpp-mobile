import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Button, Gap, LabelTextInput} from '../../../../components';
import {colors} from '../../../../utils';
import {useDispatch, useSelector} from 'react-redux';
import {
  apiAddKecamatanAsset,
  setFormInfraKecamatan,
} from '../../../../redux/actions';

const SectionInputData = () => {
  const {formInfraKecamatan} = useSelector(
    state => state.infraKecamatanReducers,
  );
  const dispatch = useDispatch();
  return (
    <View style={{paddingHorizontal: 15, marginBottom: 10}}>
      <Gap height={20} />
      <View
        style={{
          paddingHorizontal: 15,
          paddingTop: 20,
          paddingBottom: 25,
          borderRadius: 10,
          backgroundColor: colors.white,
        }}>
        <LabelTextInput
          title={'Jenis ASN'}
          placeholder={'Ketikkan Jenis ASN...'}
          defaultValue={formInfraKecamatan.JenisAsn}
          onChangeText={e => dispatch(setFormInfraKecamatan('JenisAsn', e))}
        />
        <LabelTextInput
          title={'Bagian Operasional'}
          placeholder={'Ketikkan Bagian Operasional...'}
          defaultValue={formInfraKecamatan.BagianOperasional}
          onChangeText={e =>
            dispatch(setFormInfraKecamatan('BagianOperasional', e))
          }
        />
        <LabelTextInput
          title={'Kelembagaan'}
          placeholder={'Ketikkan Kelembagaan...'}
          defaultValue={formInfraKecamatan.Kelembagaan}
          onChangeText={e => dispatch(setFormInfraKecamatan('Kelembagaan', e))}
        />
        <LabelTextInput
          title={'Jumlah Pegawai'}
          placeholder={'Ketikkan Jumlah Pegawai...'}
          defaultValue={formInfraKecamatan.JumlahPegawai}
          onChangeText={e =>
            dispatch(setFormInfraKecamatan('JumlahPegawai', e))
          }
        />
        <Gap height={20} />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <Button
            type={'primary'}
            title={'Tambah Baru'}
            onPress={() => dispatch(apiAddKecamatanAsset(formInfraKecamatan))}
            width={'100%'}
          />
        </View>
      </View>
    </View>
  );
};

export default SectionInputData;

const styles = StyleSheet.create({});
