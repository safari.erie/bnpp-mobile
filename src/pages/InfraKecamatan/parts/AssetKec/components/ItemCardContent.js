import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts} from '../../../../../utils';

const ItemCardContent = ({title, value}) => {
  return (
    <View style={{width: '48%', paddingTop: 10}}>
      <Text
        style={{
          fontSize: 13,
          fontFamily: fonts.primary['normal'],
          color: colors.dark,
        }}>
        {title} :
      </Text>
      <Text
        style={{
          fontSize: 12,
          fontFamily: fonts.primary[400],
          color: colors.grey,
        }}>
        {value}
      </Text>
    </View>
  );
};

export default ItemCardContent;

const styles = StyleSheet.create({});
