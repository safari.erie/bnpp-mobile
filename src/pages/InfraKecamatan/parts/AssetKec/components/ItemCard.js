import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import ItemCardContent from './ItemCardContent';
import {colors, fonts} from '../../../../../utils';

const ItemCard = ({idKec, idItem, nama, jumBaik, jumRusak}) => {
  return (
    <View
      style={{
        padding: 20,
        borderRadius: 10,
        backgroundColor: colors.white,
        marginBottom: 15,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingBottom: 10,
          borderBottomWidth: 0.7,
          borderBottomColor: colors.greySecondary,
        }}>
        <View>
          <Text
            numberOfLines={1}
            style={{
              fontSize: 14,
              fontFamily: fonts.primary['normal'],
              color: colors.dark,
            }}>
            #ID ITEM. {idItem}
          </Text>
        </View>
      </View>
      {/* section assetname */}
      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          width: '100%',
        }}>
        <ItemCardContent title={'ID Kecamatan'} value={idKec} />
        <ItemCardContent title={'Nama Item'} value={nama} />
        <ItemCardContent
          title={'Jumlah Baik'}
          value={
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  width: 10,
                  height: 10,
                  borderRadius: 10,
                  backgroundColor: '#4AEF5A',
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: fonts.primary[400],
                  color: colors.grey,
                  paddingLeft: 5,
                }}>
                {jumBaik}
              </Text>
            </View>
          }
        />
        <ItemCardContent
          title={'Jumlah Rusak'}
          value={
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  width: 10,
                  height: 10,
                  borderRadius: 10,
                  backgroundColor: '#F87474',
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: fonts.primary[400],
                  color: colors.grey,
                  paddingLeft: 5,
                }}>
                {jumRusak}
              </Text>
            </View>
          }
        />
      </View>
    </View>
  );
};

export default ItemCard;

const styles = StyleSheet.create({});
