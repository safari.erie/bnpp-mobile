import {StyleSheet, Text, View, FlatList} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import ItemCard from './components/ItemCard';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const SectionContent = ({data}) => {
  return (
    <>
      <Gap height={20} />
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={data}
        keyExtractor={(v, i) => i.toString()}
        renderItem={({item}) => {
          return (
            <View style={{paddingHorizontal: 15}}>
              <ItemCard
                idItem={item.iditem}
                idKec={item.id}
                nama={item.nama}
                jumBaik={item.jumlah_baik}
                jumRusak={item.jumlah_rusak}
              />
            </View>
          );
        }}
        ListFooterComponent={() => {
          return <Gap height={60} />;
        }}
      />
      {/* {data.length !== 0 ? (
        <>
          <Gap height={20} />
          <FlatList
            showsHorizontalScrollIndicator={false}
            data={data}
            keyExtractor={(v, i) => i.toString()}
            renderItem={({item}) => {
              return (
                <View style={{paddingHorizontal: 15}}>
                  <ItemCard
                    idItem={item.iditem}
                    idKec={item.id}
                    nama={item.nama}
                    jumBaik={item.jumlah_baik}
                    jumRusak={item.jumlah_rusak}
                  />
                </View>
              );
            }}
            ListFooterComponent={() => {
              return <Gap height={60} />;
            }}
          />
        </>
      ) : (
        <>
          <View style={{paddingHorizontal: 15, paddingVertical: 20}}>
            <LoadingSkeleton />
          </View>
        </>
      )} */}
    </>
  );
};

const LoadingSkeleton = () => {
  var loading = [];

  for (let i = 0; i < 10; i++) {
    loading.push(
      <SkeletonPlaceholder.Item key={i} marginBottom={i === 10 ? 0 : 20}>
        <SkeletonPlaceholder.Item
          width={'100%'}
          height={100}
          borderRadius={10}
          marginBottom={5}
        />
      </SkeletonPlaceholder.Item>,
    );
  }
  return <SkeletonPlaceholder speed={1000}>{loading}</SkeletonPlaceholder>;
};

export default SectionContent;

const styles = StyleSheet.create({});
