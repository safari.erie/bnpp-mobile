import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Button, Gap, LabelTextInput, RadioButton} from '../../../../components';
import {colors, fonts} from '../../../../utils';
import {useDispatch, useSelector} from 'react-redux';
import {
  apiInfraKecUpdateCamat,
  setFormInfraKecamatan,
} from '../../../../redux/actions';

const SectionInputData = ({navigation}) => {
  const {formInfraKecamatan} = useSelector(
    state => state.infraKecamatanReducers,
  );
  const dispatch = useDispatch();
  return (
    <View style={{paddingHorizontal: 15, marginBottom: 10}}>
      <Gap height={20} />
      <View
        style={{
          paddingHorizontal: 15,
          paddingTop: 20,
          paddingBottom: 25,
          borderRadius: 10,
          backgroundColor: colors.white,
        }}>
        <LabelTextInput
          title={'Nama Camat'}
          placeholder={'Ketikkan Nama Camat...'}
          defaultValue={formInfraKecamatan.NamaCamat}
          onChangeText={e => dispatch(setFormInfraKecamatan('NamaCamat', e))}
        />
        <LabelTextInput
          title={'Pendidikan Camat'}
          placeholder={'Ketikkan Pendidikan Camat...'}
          defaultValue={formInfraKecamatan.PendidikanCamat}
          onChangeText={e =>
            dispatch(setFormInfraKecamatan('PendidikanCamat', e))
          }
        />
        {/* section gender */}
        <View style={{paddingHorizontal: 10, paddingTop: 5}}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: fonts.primary[400],
              color: colors.dark,
            }}>
            Jenis Kelamin :
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <RadioButton
              label={'Laki-laki'}
              status={
                formInfraKecamatan.GenderCamat === 'l' ? 'checked' : 'unchecked'
              }
              onPress={() =>
                dispatch(setFormInfraKecamatan('GenderCamat', 'l'))
              }
            />
            <RadioButton
              label={'Perempuan'}
              status={
                formInfraKecamatan.GenderCamat === 'p' ? 'checked' : 'unchecked'
              }
              onPress={() =>
                dispatch(setFormInfraKecamatan('GenderCamat', 'p'))
              }
            />
          </View>
        </View>
        <Gap height={10} />
        <View
          style={{
            width: '100%',
            height: 0.6,
            backgroundColor: colors.greySecondary,
          }}
        />
        <Gap height={10} />
        <LabelTextInput
          title={'Regulasi'}
          placeholder={'Ketikkan Regulasi...'}
          defaultValue={formInfraKecamatan.Regulasi}
          onChangeText={e => dispatch(setFormInfraKecamatan('Regulasi', e))}
        />
        <LabelTextInput
          title={'Alamat Kantor'}
          placeholder={'Ketikkan Alamat Kantor...'}
          defaultValue={formInfraKecamatan.AlamatKantor}
          onChangeText={e => dispatch(setFormInfraKecamatan('AlamatKantor', e))}
        />
        <LabelTextInput
          title={'Kode Pos Kantor'}
          placeholder={'Ketikkan Kode Pos Kantor...'}
          defaultValue={formInfraKecamatan.KodePosKantor}
          onChangeText={e =>
            dispatch(setFormInfraKecamatan('KodePosKantor', e))
          }
        />
        <View style={{paddingHorizontal: 10, paddingTop: 5}}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: fonts.primary[400],
              color: colors.dark,
            }}>
            Status Kantor :
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <RadioButton
              label={'Ada'}
              status={
                formInfraKecamatan.StatusKantor === 1 ? 'checked' : 'unchecked'
              }
              onPress={() => dispatch(setFormInfraKecamatan('StatusKantor', 1))}
            />
            <RadioButton
              label={'Tidak Ada'}
              status={
                formInfraKecamatan.StatusKantor === 0 ? 'checked' : 'unchecked'
              }
              onPress={() => dispatch(setFormInfraKecamatan('StatusKantor', 0))}
            />
          </View>
        </View>
        <View style={{paddingHorizontal: 10, paddingTop: 5}}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: fonts.primary[400],
              color: colors.dark,
            }}>
            Kondisi Kantor :
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <RadioButton
              label={'Baik'}
              status={
                formInfraKecamatan.KondisiKantor === 1 ? 'checked' : 'unchecked'
              }
              onPress={() =>
                dispatch(setFormInfraKecamatan('KondisiKantor', 1))
              }
            />
            <RadioButton
              label={'Rusak'}
              status={
                formInfraKecamatan.KondisiKantor === 2 ? 'checked' : 'unchecked'
              }
              onPress={() =>
                dispatch(setFormInfraKecamatan('KondisiKantor', 2))
              }
            />
          </View>
        </View>
        <Gap height={20} />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <Button
            type={'primary'}
            title={'Perbarui Data'}
            onPress={() =>
              dispatch(apiInfraKecUpdateCamat(formInfraKecamatan, navigation))
            }
            width={'100%'}
          />
        </View>
      </View>
    </View>
  );
};

export default SectionInputData;

const styles = StyleSheet.create({});
