import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import ItemCardContent from './ItemCardContent';
import {fonts, colors, fontSize} from '../../../../../utils';
import ItemBadge from './ItemBadge';

const Active = () => {
  return (
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <View
        style={{
          height: 15,
          width: 15,
          borderRadius: 15,
          backgroundColor: '#4AEF5A',
        }}
      />
      <Text
        numberOfLines={1}
        style={{
          fontSize: fontSize.mini,
          fontFamily: fonts.primary[400],
          color: colors.dark,
          paddingLeft: 5,
        }}>
        Active
      </Text>
    </View>
  );
};

const NonActive = () => {
  return (
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <View
        style={{
          height: 15,
          width: 15,
          borderRadius: 15,
          backgroundColor: '#F87474',
        }}
      />
      <Text
        numberOfLines={1}
        style={{
          fontSize: fontSize.mini,
          fontFamily: fonts.primary[400],
          color: colors.dark,
          paddingLeft: 5,
        }}>
        Non-Active
      </Text>
    </View>
  );
};

const ItemCard = ({
  id,
  provinsi,
  kecataman,
  kabkot,
  idKabkot,
  status,
  tipe,
  onPress,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        borderRadius: 10,
        padding: 15,
        backgroundColor: colors.white,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#F2F2F2',
      }}>
      {/* HEADER */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingBottom: 10,
          borderBottomWidth: 0.7,
          borderBottomColor: colors.greySecondary,
        }}>
        <View>
          <Text
            numberOfLines={1}
            style={{
              fontSize: fontSize.small,
              fontFamily: fonts.primary['normal'],
              color: colors.dark,
            }}>
            #{id}
          </Text>
        </View>
        <View>{status === 1 ? <Active /> : <NonActive />}</View>
      </View>

      {/* CONTENT */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          flexWrap: 'wrap',
          width: '100%',
        }}>
        <ItemCardContent title={'Provinsi'} value={provinsi} />
        <ItemCardContent title={'Kecamatan'} value={kecataman} />
        <ItemCardContent title={'Kab/Kot'} value={kabkot} />
        <ItemCardContent title={'ID Kab/Kot'} value={idKabkot} />
      </View>

      {/* LOKPRI */}
      <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
        {tipe.length !== 0 &&
          tipe.map((v, i) => {
            return <ItemBadge key={i} name={v.nickname} />;
          })}
      </View>
    </TouchableOpacity>
  );
};

export default ItemCard;

const styles = StyleSheet.create({});
