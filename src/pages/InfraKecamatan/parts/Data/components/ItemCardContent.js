import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../../../../utils';

const ItemCardContent = ({title, value}) => {
  return (
    <View style={{width: '50%', paddingTop: 10}}>
      <Text
        numberOfLines={1}
        style={{
          fontSize: fontSize.small - 1,
          fontFamily: fonts.primary['normal'],
          color: colors.dark,
        }}>
        {title} :
      </Text>
      <Text
        style={{
          fontSize: fontSize.mini,
          fontFamily: fonts.primary[400],
          color: colors.grey,
        }}>
        {value}
      </Text>
    </View>
  );
};

export default ItemCardContent;

const styles = StyleSheet.create({});
