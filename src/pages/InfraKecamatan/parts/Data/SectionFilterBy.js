import {StyleSheet, Text, View, Platform} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Gap} from '../../../../components';

const SectionFilterBy = () => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={20} />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View style={{paddingRight: 10}}>
          <Text
            style={{
              fontSize: fontSize.small,
              fontFamily: fonts.primary[600],
              color: colors.dark,
            }}>
            Filter :
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
          }}>
          {/* BY KECAMATAN */}
          <ItemFilterBy
            icon={
              <Icon
                name="map-marker-radius-outline"
                color={colors.white}
                size={13}
              />
            }
            title={'KABUPATEN MALUKU TENGGARA BARAT'}
          />
          {/* BY KECAMATAN */}
          <ItemFilterBy
            icon={<Icon name="sort" color={colors.white} size={13} />}
            title={'ASC : A-Z'}
          />
        </View>
      </View>
      <Gap height={20} />
    </View>
  );
};

const ItemFilterBy = ({icon, title}) => {
  return (
    <View style={{maxWidth: '60%', marginLeft: 5}}>
      <View
        style={{
          paddingHorizontal: 15,
          paddingVertical: 10,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: colors.yellow,
          borderRadius: 20,
          flexDirection: 'row',
        }}>
        {icon}

        <Text
          numberOfLines={1}
          style={{
            fontSize: fontSize.mini,
            fontFamily: fonts.primary[600],
            color: colors.white,
            textAlign: 'center',
            paddingLeft: 5,
            // marginBottom: Platform.OS === 'ios' ? 0 : -2,
          }}>
          {title}
        </Text>
      </View>
    </View>
  );
};

export default SectionFilterBy;

const styles = StyleSheet.create({});
