import {StyleSheet, Text, View, Platform} from 'react-native';
import React from 'react';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {colors, fonts, fontSize} from '../../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Button, Gap, LabelComboBox} from '../../../../components';

const SectionFilterContent = () => {
  return (
    <View style={styles.page}>
      <View style={styles.header}>
        <View style={{padding: 20}}>
          {/* section text filter */}
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon name="filter" color={colors.dark} size={20} />
            <Text
              style={{
                fontSize: fontSize.medium,
                fontFamily: fonts.primary[600],
                color: colors.dark,
                paddingLeft: 5,
              }}>
              Filter Data
            </Text>
          </View>

          {/* section filter sort */}
          <View>
            <Gap height={20} />
            <Text
              style={{
                fontSize: fontSize.small,
                fontFamily: fonts.primary['normal'],
                color: colors.dark,
              }}>
              Urutan Kab/Kot :
            </Text>
            <Gap height={10} />
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  borderWidth: 0.7,
                  borderColor: colors.yellow,
                  borderRadius: 20,
                  paddingVertical: 8,
                  paddingHorizontal: 12,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: colors.yellow,
                  marginRight: 10,
                }}>
                <Text
                  style={{
                    fontSize: fontSize.mini,
                    fontFamily: fonts.primary['normal'],
                    color: colors.white,
                    marginBottom: Platform.OS === 'ios' ? 0 : -2,
                  }}>
                  ASC : A-Z
                </Text>
              </View>
              <View
                style={{
                  borderWidth: 0.7,
                  borderColor: colors.grey,
                  borderRadius: 20,
                  paddingVertical: 8,
                  paddingHorizontal: 12,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 10,
                }}>
                <Text
                  style={{
                    fontSize: fontSize.mini,
                    fontFamily: fonts.primary['normal'],
                    color: colors.grey,
                    marginBottom: Platform.OS === 'ios' ? 0 : -2,
                  }}>
                  DESC : Z-A
                </Text>
              </View>
            </View>
          </View>

          {/* section filter combo */}
          <View>
            <Gap height={20} />
            <Text
              style={{
                fontSize: fontSize.small,
                fontFamily: fonts.primary['normal'],
                color: colors.dark,
              }}>
              Berdasarkan :
            </Text>
            <LabelComboBox
              title={'Provinsi'}
              label={null}
              onPress={() => showModal('Lokpri')}
            />
            <LabelComboBox
              title={'Kab/Kot'}
              label={null}
              onPress={() => showModal('Lokpri')}
            />
          </View>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Gap height={80} />
            <Button
              type={'secondary'}
              title={'Terapkan'}
              onPress={() => alert('ok')}
              width={'100%'}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default SectionFilterContent;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  header: {
    paddingTop: getStatusBarHeight() + 20,
  },
});
