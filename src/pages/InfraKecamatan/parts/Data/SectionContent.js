import {StyleSheet, Text, View, ScrollView, FlatList} from 'react-native';
import React from 'react';
import ItemCard from './components/ItemCard';
import {Gap} from '../../../../components';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const SectionContent = ({data, navigation}) => {
  return (
    <>
      {data.length !== 0 ? (
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={data}
          keyExtractor={(v, i) => i.toString()}
          renderItem={({item}) => {
            return (
              <>
                {item.Provinsi === 'ACEH' && (
                  <View style={{paddingHorizontal: 15}}>
                    <ItemCard
                      id={item.Id}
                      provinsi={item.Provinsi}
                      kabkot={item.Kota}
                      kecataman={item.Name}
                      idKabkot={item.KotaId}
                      status={item.Active}
                      tipe={item.Tipe}
                      onPress={() =>
                        navigation.navigate('InfraKecamatanDetail', {
                          PARAMId: item.Id,
                        })
                      }
                    />
                  </View>
                )}
                {item.Provinsi === 'MALUKU' && (
                  <View style={{paddingHorizontal: 15}}>
                    <ItemCard
                      id={item.Id}
                      provinsi={item.Provinsi}
                      kabkot={item.Kota}
                      kecataman={item.Name}
                      idKabkot={item.KotaId}
                      status={item.Active}
                      tipe={item.Tipe}
                      onPress={() =>
                        navigation.navigate('InfraKecamatanDetail', {
                          PARAMId: item.Id,
                        })
                      }
                    />
                  </View>
                )}
                {item.Provinsi === 'PAPUA BARAT' && (
                  <View style={{paddingHorizontal: 15}}>
                    <ItemCard
                      id={item.Id}
                      provinsi={item.Provinsi}
                      kabkot={item.Kota}
                      kecataman={item.Name}
                      idKabkot={item.KotaId}
                      status={item.Active}
                      tipe={item.Tipe}
                      onPress={() =>
                        navigation.navigate('InfraKecamatanDetail', {
                          PARAMId: item.Id,
                        })
                      }
                    />
                  </View>
                )}
              </>
            );
          }}
          ListFooterComponent={() => {
            return <Gap height={60} />;
          }}
        />
      ) : (
        <>
          <View style={{paddingHorizontal: 15}}>
            <LoadingSkeleton />
          </View>
        </>
      )}
    </>
  );
};

const LoadingSkeleton = () => {
  return (
    <SkeletonPlaceholder speed={1000}>
      <SkeletonPlaceholder.Item
        flexDirection="row"
        justifyContent="space-between"
        flexWrap="wrap">
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

export default SectionContent;

const styles = StyleSheet.create({});
