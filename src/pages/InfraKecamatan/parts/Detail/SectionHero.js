import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {IMGInfraKecDetail} from '../../../../assets/images';
import {colors, fonts, fontSize} from '../../../../utils';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const SectionHero = ({data}) => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={20} />
      {data !== undefined ? (
        <ImageBackground
          source={IMGInfraKecDetail}
          style={{
            backgroundColor: colors.black,
            borderRadius: 10,
            width: '100%',
            height: 160,
          }}
          resizeMode="cover"
          imageStyle={{
            width: '100%',
            height: 160,
            borderRadius: 10,
            opacity: 0.6,
          }}>
          <View
            style={{
              flex: 1,
              padding: 20,
              justifyContent: 'space-between',
            }}>
            <View style={{alignItems: 'flex-end'}}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View
                  style={{
                    height: 15,
                    width: 15,
                    borderRadius: 15,
                    backgroundColor: data?.active === 1 ? '#4AEF5A' : '#F87474',
                  }}
                />
                <Text
                  style={{
                    fontSize: 12,
                    color: colors.white,
                    fontFamily: fonts.primary['normal'],
                    paddingLeft: 5,
                  }}>
                  {data?.active === 1 ? 'Active' : 'Non-Active'}
                </Text>
              </View>
            </View>
            <View>
              <Text
                style={{
                  fontSize: fontSize.medium,
                  color: colors.white,
                  fontFamily: fonts.primary[600],
                }}>
                Kecamatan
              </Text>
              <Text
                style={{
                  fontSize: fontSize.xlarge,
                  color: colors.white,
                  fontFamily: fonts.primary[600],
                  marginTop: -5,
                }}>
                {data?.name}
              </Text>
            </View>
          </View>
        </ImageBackground>
      ) : (
        <LoadingSkeleton />
      )}
    </View>
  );
};

const LoadingSkeleton = () => {
  return (
    <SkeletonPlaceholder speed={1000}>
      <SkeletonPlaceholder.Item
        width={'100%'}
        height={160}
        borderRadius={10}
        marginBottom={5}
      />
    </SkeletonPlaceholder>
  );
};

export default SectionHero;

const styles = StyleSheet.create({});
