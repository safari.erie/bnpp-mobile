import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../../../utils';
import {KantorKecamatanDefault} from '../../../../assets/images';
import {Gap} from '../../../../components';
import {ScrollView} from 'react-native-gesture-handler';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import ItemBannerCamat from './components/ItemBannerCamat';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const SectionBannerCamat = ({data}) => {
  return (
    <View>
      <Gap height={15} />
      <Text
        style={{
          fontSize: fontSize.medium,
          color: colors.dark,
          fontFamily: fonts.primary['normal'],
          paddingHorizontal: 15,
        }}>
        Camat
      </Text>
      <Gap height={15} />
      {data !== undefined ? (
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          <View style={{paddingHorizontal: 5, flexDirection: 'row'}}>
            <ItemBannerCamat
              nama={'Kantor'}
              kondisi={data?.kon_kan}
              status={data?.sta_kan}
              img={data?.foto_kantor}
            />
            <ItemBannerCamat
              nama={'Balai'}
              kondisi={data?.kon_bal}
              status={data?.sta_bal}
              img={data?.foto_balai}
            />
          </View>
        </ScrollView>
      ) : (
        <LoadingSkeleton />
      )}
    </View>
  );
};

const LoadingSkeleton = () => {
  return (
    <SkeletonPlaceholder speed={1000}>
      <SkeletonPlaceholder.Item flexDirection="row" justifyContent="center">
        <SkeletonPlaceholder.Item
          width={wp('60%')}
          height={150}
          borderRadius={10}
          marginHorizontal={8}
        />
        <SkeletonPlaceholder.Item
          width={wp('60%')}
          height={150}
          borderRadius={10}
          marginHorizontal={8}
        />
        <SkeletonPlaceholder.Item
          width={wp('60%')}
          height={150}
          borderRadius={10}
          marginHorizontal={8}
        />
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

export default SectionBannerCamat;

const styles = StyleSheet.create({});
