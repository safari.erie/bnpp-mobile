import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {colors, fonts, fontSize} from '../../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ItemMenu from './components/ItemMenu';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const jsonMenus = [
  {
    name: 'Aset',
    icon: <Icon name="book-edit-outline" color={colors.white} size={30} />,
    page: 'InfraKecamatanDetailAset',
  },
  {
    name: 'Mobilitas',
    icon: <Icon name="car-pickup" color={colors.white} size={30} />,
    page: 'InfraKecamatanDetailMobilitas',
  },
  {
    name: 'Kepegawaian',
    icon: <Icon name="account-group-outline" color={colors.white} size={30} />,
    page: 'InfraKecamatanDetailPegawai',
  },
  {
    name: 'Kependudukan',
    icon: (
      <Icon name="map-marker-account-outline" color={colors.white} size={30} />
    ),
    page: 'InfraKecamatanDetailPenduduk',
  },
];

const SectionMenu = ({navigation, idKec, data}) => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={15} />
      <Text
        style={{
          fontSize: fontSize.medium,
          color: colors.dark,
          fontFamily: fonts.primary['normal'],
        }}>
        Menu
      </Text>
      <Gap height={15} />
      {data !== undefined ? (
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            flexWrap: 'wrap',
            alignItems: 'center',
          }}>
          {jsonMenus.map((v, i) => {
            return (
              <ItemMenu
                key={i}
                name={v.name}
                icon={v.icon}
                onPress={() => navigation.navigate(v.page, {PARAMId: idKec})}
              />
            );
          })}
        </View>
      ) : (
        <LoadingSkeleton />
      )}
    </View>
  );
};

const LoadingSkeleton = () => {
  return (
    <SkeletonPlaceholder speed={1000}>
      <SkeletonPlaceholder.Item
        flexDirection="row"
        justifyContent="space-between"
        flexWrap="wrap">
        <SkeletonPlaceholder.Item
          width={wp('20%')}
          height={90}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('20%')}
          height={90}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('20%')}
          height={90}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('20%')}
          height={90}
          borderRadius={10}
          marginBottom={13}
        />
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

export default SectionMenu;

const styles = StyleSheet.create({});
