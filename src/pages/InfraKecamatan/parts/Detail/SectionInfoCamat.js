import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {colors, fonts} from '../../../../utils';
import ItemTextInfo from './components/ItemTextInfo';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const SectionInfoCamat = ({data}) => {
  return (
    data?.nama_camat !== undefined && (
      <>
        <View style={{paddingHorizontal: 15}}>
          <Gap height={20} />
          <View
            style={{
              padding: 20,
              borderRadius: 10,
              backgroundColor: colors.snow,
            }}>
            <View style={{position: 'absolute', right: -10, top: -10}}>
              <View
                style={{
                  padding: 10,
                  borderRadius: 100,
                  backgroundColor: colors.yellow,
                }}>
                <Icon
                  name="information-outline"
                  color={colors.greySecondary}
                  size={25}
                />
              </View>
            </View>

            <ItemTextInfo
              title={'Nama Camat :'}
              desc={data?.nama_camat || '-'}
            />
            <Gap height={10} />
            <ItemTextInfo
              title={'Jenis Kelamin :'}
              desc={data?.gender_camat === 'l' ? 'Laki - laki' : 'Perempuan'}
            />
            <Gap height={10} />
            <ItemTextInfo
              title={'Pendidikan :'}
              desc={data?.pendidikan_camat || '-'}
            />
            <Gap height={10} />
            <ItemTextInfo
              title={'Alamat Kantor :'}
              desc={data?.alamat_kantor}
            />
            <Gap height={10} />
            <ItemTextInfo
              title={'Kodepos Kantor :'}
              desc={data?.kodepos_kantor || '-'}
            />
            <Gap height={10} />
            <ItemTextInfo title={'Regulasi :'} desc={data?.regulasi || '-'} />
          </View>
        </View>
      </>
    )
  );
};

export default SectionInfoCamat;

const styles = StyleSheet.create({});
