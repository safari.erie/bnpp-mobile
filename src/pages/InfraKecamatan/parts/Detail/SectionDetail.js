import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {colors, fonts, fontSize} from '../../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const SectionDetail = ({data}) => {
  console.log('data?.lokpri', data?.lokpri);
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={15} />

      {data?.lokpri !== undefined ? (
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View>
            <Text
              style={{
                fontSize: fontSize.medium,
                color: colors.dark,
                fontFamily: fonts.primary['normal'],
              }}>
              Detail :
            </Text>
          </View>
          <View>
            <View
              style={{
                backgroundColor: '#F87474',
                borderRadius: 20,
                alignItems: 'center',
                justifyContent: 'center',
                paddingVertical: 10,
                paddingHorizontal: 15,
                flexDirection: 'row',
              }}>
              <Icon name="chart-line-variant" color={colors.white} size={15} />
              <Text
                style={{
                  fontSize: 12,
                  color: colors.white,
                  fontFamily: fonts.primary[600],
                  paddingLeft: 8,
                }}>
                {data?.lokpri}
              </Text>
            </View>
          </View>
        </View>
      ) : (
        <LoadingSkeleton />
      )}
    </View>
  );
};

const LoadingSkeleton = () => {
  return (
    <SkeletonPlaceholder speed={1000}>
      <SkeletonPlaceholder.Item width={'100%'} height={30} borderRadius={10} />
    </SkeletonPlaceholder>
  );
};

export default SectionDetail;

const styles = StyleSheet.create({});
