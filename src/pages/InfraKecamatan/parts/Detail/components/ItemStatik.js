import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../../../../utils';

const ItemStatik = ({width, icon, name, count, bgcolor}) => {
  return (
    <View
      style={{
        width: width,
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: bgcolor,
        flexDirection: 'row',
        padding: 15,
        marginBottom: 15,
      }}>
      {icon}
      <View style={{flex: 1, paddingLeft: 7}}>
        <Text
          numberOfLines={1}
          style={{
            fontSize: fontSize.small,
            fontFamily: fonts.primary['normal'],
            color: colors.white,
            maxWidth: '90%',
          }}>
          {name}
        </Text>
        <Text
          style={{
            fontSize: fontSize.large,
            fontFamily: fonts.primary[600],
            color: colors.white,
          }}>
          {count}
        </Text>
      </View>
    </View>
  );
};

export default ItemStatik;

const styles = StyleSheet.create({});
