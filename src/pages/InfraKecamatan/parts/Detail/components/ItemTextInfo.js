import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../../../../utils';
import {Gap} from '../../../../../components';

const ItemTextInfo = ({title, desc}) => {
  return (
    <>
      <Text
        style={{
          fontSize: fontSize.small,
          fontFamily: fonts.primary[600],
          color: colors.dark,
        }}>
        {title}
      </Text>
      <Gap height={5} />
      <Text
        style={{
          fontSize: fontSize.small,
          fontFamily: fonts.primary[400],
          color: colors.dark,
        }}>
        {desc}
      </Text>
    </>
  );
};

export default ItemTextInfo;

const styles = StyleSheet.create({});
