import {StyleSheet, StatusBar, View} from 'react-native';
import React, {useEffect} from 'react';
import {colors} from '../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Header} from '../../components';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import SectionContent from './parts/Data/SectionContent';
import {getInfraKecamatans} from '../../redux/actions';
import {useDispatch, useSelector} from 'react-redux';
import {createDrawerNavigator} from '@react-navigation/drawer';
import SectionSearch from './parts/Data/SectionSearch';
import SectionFilterBy from './parts/Data/SectionFilterBy';
import SectionFilterContent from './parts/Data/SectionFilterContent';
import {useIsFocused} from '@react-navigation/native';

const Drawer = createDrawerNavigator();

const CompInfraKecamatan = ({navigation}) => {
  const {dataInfraKecamatans} = useSelector(
    state => state.infraKecamatanReducers,
  );
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  useEffect(() => {
    dispatch(getInfraKecamatans());
  }, [dispatch]);

  useEffect(() => {
    dispatch({type: 'DATA_INFRA_KECAMATAN', payload: []});
  }, [dispatch, isFocused]);

  return (
    <>
      <StatusBar
        translucent
        barStyle={'light-content'}
        backgroundColor={colors.black}
      />
      <View style={styles.page}>
        {/* section header */}
        <View style={styles.header}>
          <Header
            title={'Infrastruktur Kecamatan'}
            iconLeft={
              <Icon name="keyboard-backspace" color={colors.white} size={25} />
            }
            onPressLeft={() => navigation.goBack()}
            iconRight={
              <Icon name="filter-outline" color={colors.white} size={25} />
            }
            onPressRight={() => navigation.toggleDrawer()}
            type={'secondary'}
          />
        </View>
        {/* section content */}
        <View style={styles.content}>
          {/* section search */}
          <SectionSearch />

          {/* section filter */}
          <SectionFilterBy />

          {/* section card */}
          <SectionContent data={dataInfraKecamatans} navigation={navigation} />
        </View>
      </View>
    </>
  );
};

const InfraKecamatan = ({navigation, route}) => {
  return (
    <Drawer.Navigator
      drawerContent={props => <SectionFilterContent {...props} />}
      drawerPosition={'right'}
      screenOptions={{gestureEnabled: true}}
      initialRouteName={'CompInfraKecamatan'}>
      <Drawer.Screen
        name="CompInfraKecamatan"
        component={CompInfraKecamatan}
        initialParams={route.params}
      />
    </Drawer.Navigator>
  );
};

export default InfraKecamatan;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  header: {
    backgroundColor: colors.black,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingTop: getStatusBarHeight() + 20,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});
