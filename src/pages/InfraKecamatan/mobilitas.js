import {StyleSheet, Text, View, StatusBar, ScrollView} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors, fonts} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Button, Gap, Header} from '../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import SectionInputData from './parts/Mobilitas/SectionInputData';

const InfraKecamatanDetailMobilitas = ({navigation, route}) => {
  const {PARAMId} = route.params;
  const {dataInfraKecamatan} = useSelector(
    state => state.infraKecamatanReducers,
  );
  const [isAdd, setIsAdd] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
    //   dispatch(getInfraKecamatan(PARAMId));
  }, [dispatch]);
  return (
    <>
      <StatusBar translucent barStyle={'dark-content'} />
      <View style={styles.container}>
        {/* SECTION HEADER */}
        <Header
          title={'Mobilitas Kecamatan'}
          iconLeft={
            <Icon name="keyboard-backspace" color={colors.dark} size={25} />
          }
          onPressLeft={() => navigation.goBack()}
          iconRight={
            <Icon
              name={!isAdd ? 'plus' : 'minus'}
              color={colors.bluelight}
              size={25}
            />
          }
          onPressRight={() => setIsAdd(!isAdd)}
        />
        {/* SECTION CONTENT */}
        <View style={styles.content}>
          {/* section add */}
          {isAdd && <SectionInputData />}

          {/* section list */}
          {/* <SectionContent data={dataInfraKecamatanAssetKec} /> */}
        </View>
      </View>
    </>
  );
};

export default InfraKecamatanDetailMobilitas;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: getStatusBarHeight() + 20,
  },
  content: {
    flex: 1,
    backgroundColor: colors.snow,
  },
});
