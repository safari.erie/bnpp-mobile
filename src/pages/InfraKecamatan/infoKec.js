import {StyleSheet, Text, View, StatusBar, ScrollView} from 'react-native';
import React, {useEffect} from 'react';
import {colors, fonts} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Button, Gap, Header} from '../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import SectionInputData from './parts/InfoKec/SectionInputData';
import {setFormInfraKecamatan} from '../../redux/actions';

const InfraKecamatanDetailInfoKec = ({navigation, route}) => {
  const {PARAMId} = route.params;

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setFormInfraKecamatan('IdKecamatan', PARAMId));
  }, [dispatch]);
  return (
    <>
      <StatusBar translucent barStyle={'dark-content'} />
      <View style={styles.container}>
        {/* SECTION HEADER */}
        <Header
          title={'Info Kecamatan'}
          iconLeft={
            <Icon name="keyboard-backspace" color={colors.dark} size={25} />
          }
          onPressLeft={() => navigation.goBack()}
        />
        {/* SECTION CONTENT */}
        <View style={styles.content}>
          <ScrollView>
            <SectionInputData navigation={navigation} />
            <Gap height={60} />
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default InfraKecamatanDetailInfoKec;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: getStatusBarHeight() + 20,
  },
  content: {
    flex: 1,
    backgroundColor: colors.snow,
  },
});
