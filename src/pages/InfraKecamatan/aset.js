import {StyleSheet, Text, View, StatusBar, ScrollView} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors, fonts} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Button, Gap, Header, LabelTextInput} from '../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import SectionContent from './parts/AssetKec/SectionContent';
import SectionInputData from './parts/AssetKec/SectionInputData';
import {
  getInfraKecamatanAssetKec,
  setFormInfraKecamatan,
} from '../../redux/actions';

const InfraKecamatanDetailAset = ({navigation, route}) => {
  const {PARAMId} = route.params;
  const {dataInfraKecamatanAssetKec} = useSelector(
    state => state.infraKecamatanReducers,
  );
  const dispatch = useDispatch();
  const [isAdd, setIsAdd] = useState(false);
  useEffect(() => {
    dispatch(getInfraKecamatanAssetKec(PARAMId));
    dispatch(setFormInfraKecamatan('IdKecamatan', PARAMId));
  }, [dispatch]);
  return (
    <>
      <StatusBar translucent barStyle={'dark-content'} />
      <View style={styles.container}>
        {/* SECTION HEADER */}
        <Header
          title={'Aset Kecamatan'}
          iconLeft={
            <Icon name="keyboard-backspace" color={colors.dark} size={25} />
          }
          onPressLeft={() => navigation.goBack()}
          iconRight={
            <Icon
              name={!isAdd ? 'plus' : 'minus'}
              color={colors.bluelight}
              size={25}
            />
          }
          onPressRight={() => setIsAdd(!isAdd)}
        />
        {/* SECTION CONTENT */}
        <View style={styles.content}>
          {/* section add */}
          {isAdd && <SectionInputData />}

          {/* section list */}
          <SectionContent data={dataInfraKecamatanAssetKec} />
        </View>
      </View>
    </>
  );
};

export default InfraKecamatanDetailAset;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: getStatusBarHeight() + 20,
  },
  content: {
    flex: 1,
    backgroundColor: colors.snow,
  },
});
