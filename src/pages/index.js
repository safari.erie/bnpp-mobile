import Home from './home';
import Notifikasi from './notifikasi';
import DetailNotifikasi from './notifikasi/detail';
import Download from './download';
import Login from './login';
import Splash from './splash';

import InfraKecamatan from './infraKecamatan';
import InfraKecamatanDetail from './infraKecamatan/detail';
import InfraKecamatanDetailAset from './infraKecamatan/aset';
import InfraKecamatanDetailMobilitas from './infraKecamatan/mobilitas';
import InfraKecamatanDetailInfoKec from './infraKecamatan/infoKec';
import InfraKecamatanDetailPegawai from './infraKecamatan/pegawai';
import InfraKecamatanDetailPenduduk from './infraKecamatan/penduduk';

import InfraDesa from './infraDesa';
import InfraDesaDetail from './infraDesa/detail';
import InfraDesaDetailKades from './infraDesa/kades';
import InfraDesaDetailBalaiDesa from './infraDesa/balaiDesa';
import InfraDesaDetailKantorDesa from './infraDesa/kantorDesa';

import MonevInput from './monev/index';
import MonevInputStep from './monev/step';
import MonevList from './monev/list';
import MonevDetail from './monev/detail';
import MonevResult from './monev/result';

import BeritaList from './berita';
import BeritaDetail from './berita/detail';

import User from './user';
import EditProfil from './editProfil';
import UbahPassword from './ubahPassword';
import PreviewImage from './previewImage';

export {
  Home,
  Notifikasi,
  DetailNotifikasi,
  Download,
  Login,
  Splash,
  User,
  InfraKecamatan,
  InfraKecamatanDetail,
  InfraKecamatanDetailAset,
  InfraKecamatanDetailMobilitas,
  InfraKecamatanDetailInfoKec,
  InfraKecamatanDetailPegawai,
  InfraKecamatanDetailPenduduk,
  InfraDesa,
  InfraDesaDetail,
  InfraDesaDetailKades,
  InfraDesaDetailBalaiDesa,
  InfraDesaDetailKantorDesa,
  MonevList,
  MonevInput,
  MonevInputStep,
  MonevDetail,
  MonevResult,
  BeritaList,
  BeritaDetail,
  EditProfil,
  UbahPassword,
  PreviewImage,
};
