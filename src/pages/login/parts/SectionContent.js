import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {colors, fonts} from '../../../utils';
import {useSelector, useDispatch} from 'react-redux';
import {TextInput} from 'react-native-paper';
import {setFormLogin, apiLogin} from '../../../redux/actions';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Background} from '../../../assets';
import OneSignal from 'react-native-onesignal';

const Header = () => {
  return (
    <View style={{marginHorizontal: 20, marginVertical: 40}}>
      <Text
        style={{
          color: colors.white,
          fontFamily: fonts.primary[600],
          fontSize: 34,
        }}>
        {' '}
        Hello.
      </Text>
      <Text
        style={{
          color: colors.white,
          fontFamily: fonts.primary[600],
          fontSize: 34,
        }}>
        {' '}
        Welcome Back
      </Text>
    </View>
  );
};

const Form = ({navigation}) => {
  const dispatch = useDispatch();
  const {formLogin} = useSelector(state => state.loginReducer);
  const [passwordVisible, setPasswordVisible] = useState(true);
  const {email, password} = formLogin;

  useEffect(() => {
    async function fetchData() {
      const deviceState = await OneSignal.getDeviceState();
      console.log('token onesignal : ', deviceState.userId);
      dispatch(setFormLogin('token_fcm', deviceState.userId));
    }
    fetchData();
  }, []);

  const btnLogin = () => {
    dispatch(apiLogin(formLogin, navigation));
  };

  return (
    <View style={styles.ViewCard}>
      <TextInput
        style={{backgroundColor: 'transparent'}}
        theme={{
          colors: {placeholder: 'white', text: 'white', primary: 'white'},
        }}
        label="Input Username Or Email"
        value={email}
        defaultValue={email}
        onChangeText={e => dispatch(setFormLogin('email', e))}></TextInput>
      <TextInput
        style={{backgroundColor: 'transparent'}}
        theme={{
          colors: {placeholder: 'white', text: 'white', primary: 'white'},
        }}
        label="Password"
        value={password}
        onChangeText={val => dispatch(setFormLogin('password', val))}
        secureTextEntry={passwordVisible}
        right={
          <TextInput.Icon
            name={passwordVisible ? 'eye' : 'eye-off'}
            theme={{
              colors: {text: 'white', primary: 'white'},
            }}
            onPress={() => setPasswordVisible(!passwordVisible)}
          />
        }
      />
      {/* <View style={{alignItems: 'flex-end', marginTop: 10}}>
        <Text style={{color: 'white'}}> ForgotPassword?</Text>
      </View> */}
      <View style={styles.WrapperLogin}>
        <TouchableOpacity style={styles.buttonLoginWhite} onPress={btnLogin}>
          <Text style={styles.textLogin}> Sign In</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const SectionContent = ({navigation}) => {
  return (
    <ImageBackground
      source={Background}
      imageStyle={{
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
      }}
      style={styles.pageContainer}>
      <Header />
      <Form navigation={navigation} />
    </ImageBackground>
  );
};

export default SectionContent;

const styles = StyleSheet.create({
  pageContainer: {
    flex: 1,
    paddingTop: getStatusBarHeight() + 20,
  },
  formContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
  },
  ViewCard: {
    width: '80%',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    marginHorizontal: 30,
  },
  WrapperLogin: {
    marginHorizontal: 20,
    marginTop: 80,
  },
  buttonLogin: {
    backgroundColor: colors.primary,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
    elevation: 4,
  },
  buttonLoginWhite: {
    backgroundColor: 'transparent',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
    elevation: 4,
    borderWidth: 1,
    borderColor: colors.white,
  },
  textLogin: {
    color: '#FFFFFF',
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
  },
});
