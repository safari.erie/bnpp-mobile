import {StyleSheet, View, StatusBar} from 'react-native';
import React from 'react';
import {colors} from '../../utils';
import SectionContent from './parts/SectionContent';

const Login = ({navigation}) => {
  return (
    <>
      <StatusBar
        translucent
        barStyle={'light-content'}
        backgroundColor="transparent"
      />
      <View style={styles.page}>
        <SectionContent navigation={navigation} />
      </View>
    </>
  );
};

export default Login;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
});
