import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Button, Gap, LabelTextInput, RadioButton} from '../../../../components';
import {colors, fonts} from '../../../../utils';
import {useDispatch, useSelector} from 'react-redux';
import {
  apiInfraDesaUpdateKades,
  setFormInfraDesa,
} from '../../../../redux/actions';

const SectionInputData = ({navigation}) => {
  const {formInfraDesa} = useSelector(state => state.infraDesaReducers);
  const dispatch = useDispatch();
  return (
    <View style={{paddingHorizontal: 15, marginBottom: 10}}>
      <Gap height={20} />
      <View
        style={{
          paddingHorizontal: 15,
          paddingTop: 20,
          paddingBottom: 25,
          borderRadius: 10,
          backgroundColor: colors.white,
        }}>
        <LabelTextInput
          title={'Nama Kades'}
          placeholder={'Ketikkan Nama Kades...'}
          defaultValue={formInfraDesa.NamaKades}
          onChangeText={e => dispatch(setFormInfraDesa('NamaKades', e))}
        />
        {/* section gender */}
        <View style={{paddingHorizontal: 10, paddingTop: 5}}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: fonts.primary[400],
              color: colors.dark,
            }}>
            Jenis Kelamin :
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <RadioButton
              label={'Laki-laki'}
              status={
                formInfraDesa.GenderKades === 'l' ? 'checked' : 'unchecked'
              }
              onPress={() => dispatch(setFormInfraDesa('GenderKades', 'l'))}
            />
            <RadioButton
              label={'Perempuan'}
              status={
                formInfraDesa.GenderKades === 'p' ? 'checked' : 'unchecked'
              }
              onPress={() => dispatch(setFormInfraDesa('GenderKades', 'p'))}
            />
          </View>
        </View>
        <Gap height={10} />
        <LabelTextInput
          title={'Pendidikan Kades'}
          placeholder={'Ketikkan Pendidikan Kades...'}
          defaultValue={formInfraDesa.PendidikanKades}
          onChangeText={e => dispatch(setFormInfraDesa('PendidikanKades', e))}
        />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <Button
            type={'primary'}
            title={'Perbarui Data'}
            onPress={() =>
              dispatch(apiInfraDesaUpdateKades(formInfraDesa, navigation))
            }
            width={'100%'}
          />
        </View>
      </View>
    </View>
  );
};

export default SectionInputData;

const styles = StyleSheet.create({});
