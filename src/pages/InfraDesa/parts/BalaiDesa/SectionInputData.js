import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Button, Gap, LabelTextInput, RadioButton} from '../../../../components';
import {colors, fonts} from '../../../../utils';
import {useDispatch, useSelector} from 'react-redux';
import {
  apiInfraDesaUpdateBalaiDesa,
  apiInfraKecUpdateCamat,
  setFormInfraDesa,
} from '../../../../redux/actions';

const SectionInputData = ({navigation}) => {
  const {formInfraDesa} = useSelector(state => state.infraDesaReducers);
  const dispatch = useDispatch();
  return (
    <View style={{paddingHorizontal: 15, marginBottom: 10}}>
      <Gap height={20} />
      <View
        style={{
          paddingHorizontal: 15,
          paddingTop: 20,
          paddingBottom: 25,
          borderRadius: 10,
          backgroundColor: colors.white,
        }}>
        <View style={{paddingHorizontal: 10, paddingTop: 5}}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: fonts.primary[400],
              color: colors.dark,
            }}>
            Status Balai Desa :
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <RadioButton
              label={'Ada'}
              status={formInfraDesa.StatusBalai === 1 ? 'checked' : 'unchecked'}
              onPress={() => dispatch(setFormInfraDesa('StatusBalai', 1))}
            />
            <RadioButton
              label={'Tidak Ada'}
              status={formInfraDesa.StatusBalai === 0 ? 'checked' : 'unchecked'}
              onPress={() => dispatch(setFormInfraDesa('StatusBalai', 0))}
            />
          </View>
        </View>
        <View style={{paddingHorizontal: 10, paddingTop: 5}}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: fonts.primary[400],
              color: colors.dark,
            }}>
            Kondisi Balai Desa :
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <RadioButton
              label={'Baik'}
              status={
                formInfraDesa.KondisiBalai === 1 ? 'checked' : 'unchecked'
              }
              onPress={() => dispatch(setFormInfraDesa('KondisiBalai', 1))}
            />
            <RadioButton
              label={'Rusak'}
              status={
                formInfraDesa.KondisiBalai === 2 ? 'checked' : 'unchecked'
              }
              onPress={() => dispatch(setFormInfraDesa('KondisiBalai', 2))}
            />
          </View>
        </View>
        <Gap height={20} />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <Button
            type={'primary'}
            title={'Perbarui Data'}
            onPress={() =>
              dispatch(apiInfraDesaUpdateBalaiDesa(formInfraDesa, navigation))
            }
            width={'100%'}
          />
        </View>
      </View>
    </View>
  );
};

export default SectionInputData;

const styles = StyleSheet.create({});
