import {Platform, StyleSheet, TextInput, View} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {colors, fonts} from '../../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const SectionSearch = () => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={20} />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: colors.white,
          paddingHorizontal: 20,
          paddingVertical: Platform.OS == 'ios' ? 10 : 7,
          borderRadius: 10,
          borderWidth: 1,
          borderColor: '#F2F2F2',
        }}>
        <View style={{paddingRight: 15}}>
          <Icon name="magnify" color={colors.dark} size={25} />
        </View>
        <View style={{flex: 1}}>
          <TextInput
            style={{
              fontFamily: fonts.primary['normal'],
              color: colors.dark,
              fontSize: 14,
            }}
            placeholder="Cari Kecamatan..."
          />
        </View>
      </View>
    </View>
  );
};

export default SectionSearch;

const styles = StyleSheet.create({});
