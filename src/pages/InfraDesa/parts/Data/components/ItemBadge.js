import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts} from '../../../../../utils';

const ItemBadge = ({name}) => {
  return (
    <View
      style={{
        paddingVertical: 5,
        paddingHorizontal: 15,
        borderRadius: 20,
        backgroundColor: colors.bluelight,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 5,
      }}>
      <Text
        numberOfLines={1}
        style={{
          fontSize: 10,
          fontFamily: fonts.primary['normal'],
          color: colors.white,
        }}>
        {name}
      </Text>
    </View>
  );
};

export default ItemBadge;

const styles = StyleSheet.create({});
