import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import ItemCardContent from './ItemCardContent';
import {fonts, colors} from '../../../../../utils';
import ItemBadge from './ItemBadge';

const Active = () => {
  return (
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <View
        style={{
          height: 15,
          width: 15,
          borderRadius: 15,
          backgroundColor: '#4AEF5A',
        }}
      />
      <Text
        numberOfLines={1}
        style={{
          fontSize: 11,
          fontFamily: fonts.primary[400],
          color: colors.dark,
          paddingLeft: 5,
        }}>
        Active
      </Text>
    </View>
  );
};

const NonActive = () => {
  return (
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <View
        style={{
          height: 15,
          width: 15,
          borderRadius: 15,
          backgroundColor: '#F87474',
        }}
      />
      <Text
        numberOfLines={1}
        style={{
          fontSize: 11,
          fontFamily: fonts.primary[400],
          color: colors.dark,
          paddingLeft: 5,
        }}>
        Non-Active
      </Text>
    </View>
  );
};

const ItemCard = ({
  id,
  kecamatanid,
  name,
  nama_kecamatan,
  nama_kota,
  status,
  onPress,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        borderRadius: 10,
        padding: 15,
        backgroundColor: colors.white,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#F2F2F2',
      }}>
      {/* HEADER */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingBottom: 10,
          borderBottomWidth: 0.7,
          borderBottomColor: colors.greySecondary,
        }}>
        <View>
          <Text
            numberOfLines={1}
            style={{
              fontSize: 14,
              fontFamily: fonts.primary['normal'],
              color: colors.dark,
            }}>
            #{id}
          </Text>
        </View>
        <View>{status === 1 ? <Active /> : <NonActive />}</View>
      </View>

      {/* CONTENT */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          flexWrap: 'wrap',
          width: '100%',
        }}>
        <ItemCardContent title={'Desa'} value={name} />
        <ItemCardContent title={'Kecamatan'} value={nama_kecamatan} />
        <ItemCardContent title={'Kabupaten / Kota'} value={nama_kota} />
        <ItemCardContent title={'ID Kecamatan'} value={kecamatanid} />
      </View>

      {/* LOKPRI */}
      {/* <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
        {tipe && tipe.length !== 0 &&
          tipe.map((v, i) => {
            return <ItemBadge key={i} name={v.nickname} />;
          })}
      </View> */}
    </TouchableOpacity>
  );
};

export default ItemCard;

const styles = StyleSheet.create({});
