import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts} from '../../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Gap} from '../../../../components';

const SectionFilter = () => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={20} />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <Text
          style={{
            fontSize: 13,
            fontFamily: fonts.primary[600],
            color: colors.dark,
            textAlign: 'center',
          }}>
          Filter Kab/Kot :
        </Text>
        <View
          style={{
            paddingHorizontal: 15,
            paddingVertical: 10,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colors.yellow,
            borderRadius: 20,
            flexDirection: 'row',
          }}>
          <Icon
            name="map-marker-radius-outline"
            color={colors.white}
            size={15}
          />
          <Text
            style={{
              fontSize: 13,
              fontFamily: fonts.primary[600],
              color: colors.white,
              textAlign: 'center',
              paddingLeft: 5,
            }}>
            Kota Sabang
          </Text>
        </View>
      </View>
      <Gap height={20} />
    </View>
  );
};

export default SectionFilter;

const styles = StyleSheet.create({});
