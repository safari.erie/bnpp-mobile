import {StyleSheet, Text, View, FlatList} from 'react-native';
import React from 'react';
import ItemCard from './components/ItemCard';
import {Gap} from '../../../../components';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const SectionContent = ({data, navigation}) => {
  return (
    <>
      <Gap height={20} />
      {data && data.length !== 0 ? (
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={data}
          keyExtractor={(v, i) => i.toString()}
          renderItem={({item}) => {
            return (
              <View style={{paddingHorizontal: 15}}>
                <ItemCard
                  id={item.id}
                  kecamatanid={item.kecamatanid}
                  name={item.name}
                  nama_kecamatan={item.nama_kecamatan}
                  nama_kota={item.nama_kota}
                  status={item.active}
                  onPress={() =>
                    navigation.navigate('InfraDesaDetail', {PARAMId: item.id})
                  }
                />
              </View>
            );
          }}
          ListFooterComponent={() => {
            return <Gap height={60} />;
          }}
        />
      ) : (
        <>
          <View style={{paddingHorizontal: 15}}>
            <LoadingSkeleton />
          </View>
        </>
      )}
    </>
  );
};

const LoadingSkeleton = () => {
  return (
    <SkeletonPlaceholder speed={1000}>
      <SkeletonPlaceholder.Item
        flexDirection="row"
        justifyContent="space-between"
        flexWrap="wrap">
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
        <SkeletonPlaceholder.Item
          width={wp('100%')}
          height={100}
          borderRadius={10}
          marginBottom={13}
        />
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

export default SectionContent;

const styles = StyleSheet.create({});
