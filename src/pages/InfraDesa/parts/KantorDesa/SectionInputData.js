import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Button, Gap, LabelTextInput, RadioButton} from '../../../../components';
import {colors, fonts} from '../../../../utils';
import {useDispatch, useSelector} from 'react-redux';
import {
  apiInfraDesaUpdateKantorDesa,
  apiInfraKecUpdateCamat,
  setFormInfraDesa,
} from '../../../../redux/actions';

const SectionInputData = ({navigation}) => {
  const {formInfraDesa} = useSelector(state => state.infraDesaReducers);

  const dispatch = useDispatch();
  return (
    <View style={{paddingHorizontal: 15, marginBottom: 10}}>
      <Gap height={20} />
      <View
        style={{
          paddingHorizontal: 15,
          paddingTop: 20,
          paddingBottom: 25,
          borderRadius: 10,
          backgroundColor: colors.white,
        }}>
        <View
          style={{
            width: '100%',
            height: 0.6,
            backgroundColor: colors.greySecondary,
          }}
        />
        <Gap height={10} />
        <LabelTextInput
          title={'Regulasi'}
          placeholder={'Ketikkan Regulasi...'}
          defaultValue={formInfraDesa.Regulasi}
          onChangeText={e => dispatch(setFormInfraDesa('Regulasi', e))}
        />
        <LabelTextInput
          title={'Alamat Kantor'}
          placeholder={'Ketikkan Alamat Kantor...'}
          defaultValue={formInfraDesa.AlamatKantor}
          onChangeText={e => dispatch(setFormInfraDesa('AlamatKantor', e))}
        />
        <View style={{paddingHorizontal: 10, paddingTop: 5}}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: fonts.primary[400],
              color: colors.dark,
            }}>
            Status Kantor :
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <RadioButton
              label={'Ada'}
              status={
                formInfraDesa.StatusKantor === 1 ? 'checked' : 'unchecked'
              }
              onPress={() => dispatch(setFormInfraDesa('StatusKantor', 1))}
            />
            <RadioButton
              label={'Tidak Ada'}
              status={
                formInfraDesa.StatusKantor === 0 ? 'checked' : 'unchecked'
              }
              onPress={() => dispatch(setFormInfraDesa('StatusKantor', 0))}
            />
          </View>
        </View>
        <View style={{paddingHorizontal: 10, paddingTop: 5}}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: fonts.primary[400],
              color: colors.dark,
            }}>
            Kondisi Kantor :
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <RadioButton
              label={'Baik'}
              status={
                formInfraDesa.KondisiKantor === 1 ? 'checked' : 'unchecked'
              }
              onPress={() => dispatch(setFormInfraDesa('KondisiKantor', 1))}
            />
            <RadioButton
              label={'Rusak'}
              status={
                formInfraDesa.KondisiKantor === 2 ? 'checked' : 'unchecked'
              }
              onPress={() => dispatch(setFormInfraDesa('KondisiKantor', 2))}
            />
          </View>
        </View>
        <Gap height={20} />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <Button
            type={'primary'}
            title={'Perbarui Data'}
            onPress={() =>
              dispatch(apiInfraDesaUpdateKantorDesa(formInfraDesa, navigation))
            }
            width={'100%'}
          />
        </View>
      </View>
    </View>
  );
};

export default SectionInputData;

const styles = StyleSheet.create({});
