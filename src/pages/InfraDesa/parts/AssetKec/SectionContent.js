import {StyleSheet, Text, View, FlatList} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import ItemCard from './components/ItemCard';

const SectionContent = ({data}) => {
  return (
    <>
      <Gap height={20} />
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={data}
        keyExtractor={(v, i) => i.toString()}
        renderItem={({item}) => {
          return (
            <View style={{paddingHorizontal: 15}}>
              <ItemCard
                idItem={item.iditem}
                idKec={item.id}
                nama={item.nama}
                jumBaik={item.jumlah_baik}
                jumRusak={item.jumlah_rusak}
              />
            </View>
          );
        }}
        ListFooterComponent={() => {
          return <Gap height={60} />;
        }}
      />
    </>
  );
};

export default SectionContent;

const styles = StyleSheet.create({});
