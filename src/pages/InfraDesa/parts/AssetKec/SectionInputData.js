import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Button, Gap, LabelTextInput} from '../../../../components';
import {colors} from '../../../../utils';
import {useDispatch, useSelector} from 'react-redux';
import {
  apiAddKecamatanAsset,
  setFormInfraKecamatan,
} from '../../../../store/actions';

const SectionInputData = () => {
  const {formInfraKecamatan} = useSelector(
    state => state.infraKecamatanReducers,
  );
  const dispatch = useDispatch();
  return (
    <View style={{paddingHorizontal: 15, marginBottom: 10}}>
      <Gap height={20} />
      <View
        style={{
          paddingHorizontal: 15,
          paddingTop: 20,
          paddingBottom: 25,
          borderRadius: 10,
          backgroundColor: colors.white,
        }}>
        <LabelTextInput
          title={'Nama'}
          placeholder={'Ketikkan Nama...'}
          defaultValue={formInfraKecamatan.Nama}
          onChangeText={e => dispatch(setFormInfraKecamatan('Nama', e))}
        />
        <LabelTextInput
          title={'Jumlah Baik'}
          placeholder={'Ketikkan Baik...'}
          defaultValue={formInfraKecamatan.Baik}
          onChangeText={e => dispatch(setFormInfraKecamatan('Baik', e))}
        />
        <LabelTextInput
          title={'Jumlah Rusak'}
          placeholder={'Ketikkan Rusak...'}
          defaultValue={formInfraKecamatan.Rusak}
          onChangeText={e => dispatch(setFormInfraKecamatan('Rusak', e))}
        />
        <Gap height={20} />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <Button
            type={'primary'}
            title={'Tambah Baru'}
            onPress={() => dispatch(apiAddKecamatanAsset(formInfraKecamatan))}
            width={'100%'}
          />
        </View>
      </View>
    </View>
  );
};

export default SectionInputData;

const styles = StyleSheet.create({});
