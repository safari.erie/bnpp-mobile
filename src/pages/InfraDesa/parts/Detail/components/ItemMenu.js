import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import {colors, fonts} from '../../../../../utils';

const ItemMenu = ({icon, name, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: '25%',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          width: 60,
          height: 60,
          borderRadius: 10,
          backgroundColor: '#AD8B73',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {icon}
      </View>
      <Text
        numberOfLines={1}
        style={{
          fontSize: 13,
          color: colors.dark,
          fontFamily: fonts.primary[400],
          paddingTop: 5,
          maxWidth: '80%',
        }}>
        {name}
      </Text>
    </TouchableOpacity>
  );
};

export default ItemMenu;
