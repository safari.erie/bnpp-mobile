import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {colors, fonts} from '../../../../../utils';

const ItemBannerKades = ({nama, kondisi, status, img}) => {
  return (
    <View style={{width: wp('90%'), marginHorizontal: 10}}>
      <ImageBackground
        source={{uri: img}}
        style={{
          backgroundColor: colors.black,
          borderRadius: 10,
          width: wp('90%'),
          height: 160,
        }}
        resizeMode="cover"
        imageStyle={{
          width: '100%',
          height: 160,
          borderRadius: 10,
          opacity: 0.6,
        }}>
        <View
          style={{
            flex: 1,
            padding: 20,
            justifyContent: 'space-between',
          }}>
          <View>
            <Text
              style={{
                fontSize: 20,
                color: colors.white,
                fontFamily: fonts.primary[600],
              }}>
              {nama}
            </Text>
          </View>

          <View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View>
                <Text
                  style={{
                    fontSize: 17,
                    color: colors.white,
                    fontFamily: fonts.primary['normal'],
                  }}>
                  Kondisi :
                </Text>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <View
                    style={{
                      height: 15,
                      width: 15,
                      borderRadius: 15,
                      backgroundColor:
                        kondisi === 'Baik' ? '#4AEF5A' : '#F87474',
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 12,
                      color: colors.white,
                      fontFamily: fonts.primary['normal'],
                      paddingLeft: 5,
                    }}>
                    {kondisi}
                  </Text>
                </View>
              </View>
              <View>
                <Text
                  style={{
                    fontSize: 17,
                    color: colors.white,
                    fontFamily: fonts.primary['normal'],
                  }}>
                  Status :
                </Text>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <View
                    style={{
                      height: 15,
                      width: 15,
                      borderRadius: 15,
                      backgroundColor: status === 'Ada' ? '#4AEF5A' : '#F87474',
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 12,
                      color: colors.white,
                      fontFamily: fonts.primary['normal'],
                      paddingLeft: 5,
                    }}>
                    {status}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export default ItemBannerKades;

const styles = StyleSheet.create({});
