import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {colors, fonts} from '../../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ItemMenu from './components/ItemMenu';

const jsonMenus = [
  {
    name: 'Kades',
    icon: <Icon name="account-outline" color={colors.white} size={30} />,
    page: 'InfraDesaDetailKades',
  },
  {
    name: 'Balai Desa',
    icon: <Icon name="hospital-building" color={colors.white} size={30} />,
    page: 'InfraDesaDetailBalaiDesa',
  },
  {
    name: 'Kantor Desa',
    icon: <Icon name="office-building" color={colors.white} size={30} />,
    page: 'InfraDesaDetailKantorDesa',
  },
  // {
  //   name: 'Asset Camat',
  //   icon: <Icon name="account-group-outline" color={colors.white} size={30} />,
  //   page: 'InfraKecamatanDetailAssetCamat',
  // },
  // {
  //   name: 'Info Camat',
  //   icon: <Icon name="google-maps" color={colors.white} size={30} />,
  //   page: 'InfraKecamatanDetailInfoCamat',
  // },
];

const SectionMenu = ({navigation, idDesa}) => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={15} />
      <Text
        style={{
          fontSize: 15,
          color: colors.dark,
          fontFamily: fonts.primary['normal'],
        }}>
        Menu
      </Text>
      <Gap height={15} />
      <View
        style={{
          // justifyContent: 'space-between',
          flexDirection: 'row',
          width: '100%',
          flexWrap: 'wrap',
          alignItems: 'center',
        }}>
        {jsonMenus.map((v, i) => {
          return (
            <ItemMenu
              key={i}
              name={v.name}
              icon={v.icon}
              onPress={() => navigation.navigate(v.page, {PARAMId: idDesa})}
            />
          );
        })}
      </View>
    </View>
  );
};

export default SectionMenu;

const styles = StyleSheet.create({});
