import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {colors, fonts} from '../../../../utils';
import ItemTextInfo from './components/ItemTextInfo';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const SectionInfoKades = ({data}) => {
  return (
    data?.nama_kades !== undefined && (
      <>
        <View style={{paddingHorizontal: 15}}>
          <Gap height={20} />
          <View
            style={{
              padding: 20,
              borderRadius: 10,
              backgroundColor: colors.snow,
            }}>
            <View style={{position: 'absolute', right: -10, top: -10}}>
              <View
                style={{
                  padding: 10,
                  borderRadius: 100,
                  backgroundColor: colors.yellow,
                }}>
                <Icon
                  name="information-outline"
                  color={colors.greySecondary}
                  size={25}
                />
              </View>
            </View>

            <ItemTextInfo
              title={'Nama Kades :'}
              desc={data?.nama_kades || '-'}
            />
            <Gap height={10} />
            <ItemTextInfo
              title={'Jenis Kelamin :'}
              desc={data?.gender_kades === 'l' ? 'Laki - laki' : 'Perempuan'}
            />
            <Gap height={10} />
            <ItemTextInfo
              title={'Pendidikan :'}
              desc={data?.pendidikan_kades || '-'}
            />
            <Gap height={10} />
            <ItemTextInfo title={'Alamat Kantor :'} desc={data?.alamat_desa} />
            <Gap height={10} />
            <ItemTextInfo
              title={'Kodepos Kantor :'}
              desc={data?.kodepos_kantor || '-'}
            />
            <Gap height={10} />
            <ItemTextInfo title={'Regulasi :'} desc={data?.regulasi || '-'} />
          </View>
        </View>
      </>
    )
  );
};

export default SectionInfoKades;

const styles = StyleSheet.create({});
