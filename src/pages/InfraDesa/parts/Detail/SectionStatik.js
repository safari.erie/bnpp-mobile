import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {colors, fonts} from '../../../../utils';
import ItemStatik from './components/ItemStatik';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const SectionStatik = ({data}) => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={15} />
      <View
        style={{
          justifyContent: 'space-between',
          flexDirection: 'row',
          width: '100%',
          flexWrap: 'wrap',
          alignItems: 'center',
        }}>
        <ItemStatik
          name={'Jumlah Penduduk'}
          count={data?.jumlah_penduduk || 0}
          width={'48%'}
          bgcolor={'#3AB0FF'}
          icon={
            <Icon name="account-group-outline" color={colors.white} size={40} />
          }
        />
        <ItemStatik
          name={'Jumlah KK'}
          count={data?.jumlah_kk || 0}
          width={'48%'}
          bgcolor={'#FFB562'}
          icon={
            <Icon
              name="card-account-details-outline"
              color={colors.white}
              size={40}
            />
          }
        />
      </View>
    </View>
  );
};

export default SectionStatik;

const styles = StyleSheet.create({});
