import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {colors, fonts} from '../../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const SectionDetail = ({data}) => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={15} />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View>
          <Text
            style={{
              fontSize: 15,
              color: colors.dark,
              fontFamily: fonts.primary['normal'],
            }}>
            Detail :
          </Text>
        </View>
        <View>
          <View
            style={{
              backgroundColor: '#F87474',
              borderRadius: 20,
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 10,
              paddingHorizontal: 15,
              flexDirection: 'row',
            }}>
            <Icon name="chart-line-variant" color={colors.white} size={15} />
            <Text
              style={{
                fontSize: 12,
                color: colors.white,
                fontFamily: fonts.primary[600],
                paddingLeft: 8,
              }}>
              {data?.lokpri}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default SectionDetail;

const styles = StyleSheet.create({});
