import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {KantorKecamatanDefault} from '../../../../assets/images';
import {colors, fonts} from '../../../../utils';

const SectionHero = ({data}) => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={20} />
      <ImageBackground
        source={data?.detail?.kades?.foto_kantor != null ? {uri:data?.detail?.kades?.foto_kantor} : KantorKecamatanDefault}
        style={{
          backgroundColor: colors.black,
          borderRadius: 10,
          width: '100%',
          height: 160,
        }}
        resizeMode="cover"
        imageStyle={{
          width: '100%',
          height: 160,
          borderRadius: 10,
          opacity: 0.6,
        }}>
        <View
          style={{
            flex: 1,
            padding: 20,
            justifyContent: 'space-between',
          }}>
          <View style={{alignItems: 'flex-end'}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  height: 15,
                  width: 15,
                  borderRadius: 15,
                  backgroundColor: data?.active === 1 ? '#4AEF5A' : '#F87474',
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  color: colors.white,
                  fontFamily: fonts.primary['normal'],
                  paddingLeft: 5,
                }}>
                {data?.active === 1 ? 'Active' : 'Non-Active'}
              </Text>
            </View>
          </View>
          <View>
            <Text
              style={{
                fontSize: 18,
                color: colors.white,
                fontFamily: fonts.primary[600],
              }}>
              Desa
            </Text>
            <Text
              style={{
                fontSize: 25,
                color: colors.white,
                fontFamily: fonts.primary[600],
                marginTop: -5,
              }}>
              {data?.name}
            </Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export default SectionHero;

const styles = StyleSheet.create({});
