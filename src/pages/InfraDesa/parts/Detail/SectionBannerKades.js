import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import React from 'react';
import {colors, fonts} from '../../../../utils';
import {KantorKecamatanDefault} from '../../../../assets/images';
import {Gap} from '../../../../components';
import {ScrollView} from 'react-native-gesture-handler';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import ItemBannerKades from './components/ItemBannerKades';

const SectionBannerKades = ({data}) => {
  return (
    <View>
      <Gap height={15} />
      <Text
        style={{
          fontSize: 15,
          color: colors.dark,
          fontFamily: fonts.primary['normal'],
          paddingHorizontal: 15,
        }}>
        Kades
      </Text>
      <Gap height={15} />
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        <View style={{paddingHorizontal: 5, flexDirection: 'row'}}>
          <ItemBannerKades
            nama={'Kantor'}
            kondisi={data?.kades?.kon_kan}
            status={data?.camat?.sta_kan}
            img={data?.kades?.foto_kantor}
          />
          <ItemBannerKades
            nama={'Balai'}
            kondisi={data?.kades?.kon_bal}
            status={data?.kades?.sta_bal}
            img={data?.kades?.foto_balai}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default SectionBannerKades;

const styles = StyleSheet.create({});
