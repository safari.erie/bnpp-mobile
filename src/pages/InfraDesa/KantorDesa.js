import {StyleSheet, Text, View, StatusBar, ScrollView} from 'react-native';
import React, {useEffect} from 'react';
import {colors, fonts} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Button, Gap, Header} from '../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import SectionInputData from './parts/KantorDesa/SectionInputData';
import {getInfraDesaKantorDesa, setFormInfraDesa} from '../../redux/actions';

const InfraDesaDetailKantorDesa = ({navigation, route}) => {
  const {PARAMId} = route.params;

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getInfraDesaKantorDesa(PARAMId));
    dispatch(setFormInfraDesa('IdDesa', PARAMId));
  }, [dispatch]);
  return (
    <>
      <StatusBar translucent barStyle={'dark-content'} />
      <View style={styles.container}>
        {/* SECTION HEADER */}
        <Header
          title={'Kantor Desa'}
          iconLeft={
            <Icon name="keyboard-backspace" color={colors.dark} size={25} />
          }
          onPressLeft={() => navigation.goBack()}
        />
        {/* SECTION CONTENT */}
        <View style={styles.content}>
          <ScrollView>
            <SectionInputData navigation={navigation} />
            <Gap height={60} />
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default InfraDesaDetailKantorDesa;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: getStatusBarHeight() + 20,
  },
  content: {
    flex: 1,
    backgroundColor: colors.snow,
  },
});
