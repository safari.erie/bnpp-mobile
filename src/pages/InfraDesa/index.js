import {
  StyleSheet,
  StatusBar,
  View,
  TextInput,
  TouchableOpacity,
  Platform,
  Text,
} from 'react-native';
import React, {useEffect} from 'react';
import {colors, fonts} from '../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Gap, Header} from '../../components';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ItemCard from './parts/Data/components/ItemCard';
import SectionContent from './parts/Data/SectionContent';
import SectionFilter from './parts/Data/SectionFilter';
import {getInfraDesas} from '../../redux/actions';
import {useDispatch, useSelector} from 'react-redux';
import SectionSearch from './parts/Data/SectionSearch';

const InfraDesa = ({navigation}) => {
  const {dataInfraDesas} = useSelector(state => state.infraDesaReducers);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getInfraDesas());
  }, [dispatch]);

  return (
    <>
      <StatusBar
        translucent
        barStyle={'light-content'}
        backgroundColor={colors.black}
      />
      <View style={styles.page}>
        {/* section header */}
        <View style={styles.header}>
          <Header
            title={'Infrastruktur Desa'}
            iconLeft={
              <Icon name="keyboard-backspace" color={colors.white} size={25} />
            }
            onPressLeft={() => navigation.goBack()}
            // iconRight={
            //   <Icon name="filter-outline" color={colors.white} size={25} />
            // }
            type={'secondary'}
          />
        </View>
        {/* section content */}
        <View style={styles.content}>
          {/* section search */}
          {/* <SectionSearch /> */}

          {/* section filter */}
          {/* <SectionFilter /> */}

          {/* section card */}
          <SectionContent data={dataInfraDesas} navigation={navigation} />
        </View>
      </View>
    </>
  );
};

export default InfraDesa;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  header: {
    backgroundColor: colors.black,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingTop: getStatusBarHeight() + 20,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});
