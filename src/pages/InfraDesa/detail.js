import {StyleSheet, StatusBar, View, ScrollView} from 'react-native';
import React, {useEffect} from 'react';
import {colors} from '../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Gap, Header} from '../../components';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import SectionStatik from './parts/Detail/SectionStatik';
import SectionDetail from './parts/Detail/SectionDetail';
import SectionHero from './parts/Detail/SectionHero';
import SectionMenu from './parts/Detail/SectionMenu';
import SectionBannerKades from './parts/Detail/SectionBannerKades';
import SectionInfoKades from './parts/Detail/SectionInfoKades';
import {useDispatch, useSelector} from 'react-redux';
import {getInfraDesa} from '../../redux/actions';

const InfraDesaDetail = ({navigation, route}) => {
  const {PARAMId} = route.params;
  const {dataInfraDesa} = useSelector(state => state.infraDesaReducers);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getInfraDesa(PARAMId));
  }, [dispatch]);

  return (
    <>
      <StatusBar
        translucent
        barStyle={'light-content'}
        backgroundColor={colors.black}
      />
      <View style={styles.page}>
        {/* section header */}
        <View style={styles.header}>
          <Header
            title={'Detail Desa'}
            iconLeft={
              <Icon name="keyboard-backspace" color={colors.white} size={25} />
            }
            onPressLeft={() => navigation.goBack()}
            type={'secondary'}
          />
        </View>
        {/* section content */}
        <View style={styles.content}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {dataInfraDesa && (
              <>
                {/* section hero */}
                <SectionHero data={dataInfraDesa.kec} />

                {/* section detail */}
                {/* <SectionDetail data={dataInfraDesa.detail} /> */}

                {/* section statik */}
                <SectionStatik data={dataInfraDesa.kec?.detail} />

                {/* section menu */}
                <SectionMenu navigation={navigation} idDesa={PARAMId} />

                {/* section banner camat */}
                <SectionBannerKades data={dataInfraDesa} />

                {/* section camat ingfo */}
                <SectionInfoKades data={dataInfraDesa.detail?.kades} />
              </>
            )}

            <Gap height={50} />
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default InfraDesaDetail;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.black,
    flex: 1,
  },
  header: {
    backgroundColor: colors.black,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingTop: getStatusBarHeight() + 20,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});
