import React, {useEffect} from 'react';
import {StyleSheet, Text, View, StatusBar} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {ProgressBar, List, Modal} from 'react-native-paper';
import {Button, Header} from '../../components';
import {saveMonevStep, setFormMonev} from '../../redux/actions/monevAction';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {colors, fonts, FUNCToast} from '../../utils';
import ItemModalType1 from './parts/InputData/components/ItemModalType1';
import ItemTextInput from './parts/InputData/components/ItemTextInput';
import SubHeaderAccordion from './parts/InputData/components/SubHeaderAccordion';
import ItemCombo from './parts/InputData/components/ItemCombo';

const jsonKetersediaan = [
  {
    label: 'Ya',
    value: 'Y',
  },
  {
    label: 'Tidak',
    value: 'T',
  },
];
const jsonKecukupan = [
  {
    label: 'Tidak Cukup',
    value: 'T',
  },
  {
    label: 'Lumayan Cukup',
    value: 'L',
  },
  {
    label: 'Cukup',
    value: 'Y',
  },
];

const FormDynamic = ({data}) => {
  const {
    dataAddMonevs,
    dataAddMonevSingle,
    modalKetersediaan,
    modalKecukupan,
    modalKelayakan,
    expandTabInputMonev,
    jawabanInputMonev,
    formMonev,
    dataBobots,
  } = useSelector(state => state.monevReducers);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({type: 'DATA_ADD_MONEV_SINGLE', payload: data});
  }, [data]);

  const expandSave = async (variabelId, indikator) => {
    const expand = {
      status: !expandTabInputMonev.status,
      type: `${indikator.indikatorEvaluasi}`,
    };
    dispatch({
      type: 'EXPAND_TAB_INPUTMONEV',
      payload: expand,
    });

    ///SET JAWABAN
    dispatch({type: 'JAWABAN_INPUTMONEV', payload: indikator});

    dispatch(saveMonevStep(dataAddMonevs));
  };

  const modalType1 = (pertanyaanId, objectname, jawaban, variabelId) => {
    let modalName = '';
    let objModal = false;
    let selectName = '';
    if (objectname === 'ketersediaan') {
      modalName = 'MODAL_KETERSEDIAAN';
      objModal = !modalKetersediaan;
      selectName = 'SELECT_KETERSEDIAAN';
    } else if (objectname === 'kecukupan') {
      modalName = 'MODAL_KECUKUPAN';
      objModal = !modalKecukupan;
      selectName = 'SELECT_KETERSEDIAAN';
    } else if (objectname === 'kondisi') {
      modalName = 'MODAL_KELAYAKAN';
      objModal = !modalKelayakan;
      selectName = 'SELECT_KETERSEDIAAN';
    }
    dispatch({
      type: modalName,
      payload: objModal,
    });
    dispatch({
      type: selectName,
      payload: {
        pertanyaanId,
        objectname,
        jawaban,
        modalName,
        variabelId,
      },
    });
  };

  const setValueType2Dispatch = (key, text) => {
    let newText = '';
    let numbers = '0123456789';

    for (var i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText = newText + text[i];
      } else {
        FUNCToast('WARN', {msg: 'Number Only'});
      }
    }
    dispatch(setFormMonev(key, newText));
  };

  const setValueType2 = async (
    variabelId,
    pertanyaanId,
    objectname,
    jawaban,
  ) => {
    let newArr = jawabanInputMonev.pertanyaan.map((item, i) => {
      if (pertanyaanId === item.pertanyaanId) {
        return {...item, [objectname]: jawaban};
      } else {
        return {...item};
      }
    });
    const jawabanApi = {
      indikatorId: jawabanInputMonev.indikatorId,
      type: jawabanInputMonev.type,
      pertanyaan: newArr,
    };

    ///MIGRATE
    await dataAddMonevSingle.indikator.map((v, i) => {
      if (v.indikatorId == jawabanInputMonev.indikatorId) {
        v.pertanyaan = newArr;
        console.log('aya');
      }
    });
    dispatch({type: 'DATA_ADD_MONEV_SINGLE', payload: dataAddMonevSingle});

    await dataAddMonevs.form.map((v, i) => {
      if (variabelId === v.variabelId) {
        v.indikator = dataAddMonevSingle.indikator;
      }
    });
    dispatch({type: 'DATA_ADD_MONEVS', payload: dataAddMonevs});
    ///MIGRATE

    dispatch({
      type: 'JAWABAN_INPUTMONEV',
      payload: jawabanApi,
    });
  };

  const getValueCombo = (key, item) => {
    if (key === 'ketersediaan') {
      for (let index = 0; index < jsonKetersediaan.length; index++) {
        if (jsonKetersediaan[index].value == item)
          return jsonKetersediaan[index].label;
      }
    }
    if (key === 'kecukupan') {
      for (let index = 0; index < jsonKecukupan.length; index++) {
        if (jsonKecukupan[index].value == item)
          return jsonKecukupan[index].label;
      }
    }
    if (key === 'kondisi') {
      for (let index = 0; index < dataBobots.length; index++) {
        if (dataBobots[index].bobot == item)
          return `${dataBobots[index].bobot} - ${dataBobots[index].nama}`;
      }
    }
  };

  return (
    <View style={{paddingHorizontal: 15, paddingVertical: 20}}>
      <View
        style={{
          backgroundColor: colors.white,
          marginBottom: 10,
          padding: 15,
          borderRadius: 5,
        }}>
        {dataAddMonevSingle && (
          <List.Section title={`${dataAddMonevSingle?.variabel}`}>
            {dataAddMonevSingle?.indikator?.map((v, i) => {
              var type = v.type;
              return (
                <List.Accordion
                  key={i}
                  expanded={
                    expandTabInputMonev.type === `${v.indikatorEvaluasi}` &&
                    !expandTabInputMonev.status
                  }
                  onPress={() => expandSave(data.variabelId, v)}
                  titleNumberOfLines={3}
                  title={
                    <Text
                      style={{
                        fontSize: 14,
                        fontFamily: fonts.primary['normal'],
                        color: colors.dark,
                      }}>
                      {v.indikatorEvaluasi}
                    </Text>
                  }>
                  {/* section content */}
                  {jawabanInputMonev?.pertanyaan?.map((v, i) => {
                    return (
                      <View
                        key={i + 1}
                        style={{marginVertical: 10, marginHorizontal: 20}}>
                        <SubHeaderAccordion
                          name={`${i + 1}. ${v.pertanyaan || v.pertanyaanId}`}
                        />

                        {/* TYPE 1 */}
                        {type === 'Type 1' && (
                          <>
                            <ItemCombo
                              title={'Ketersediaan'}
                              label={getValueCombo(
                                'ketersediaan',
                                v.ketersediaan,
                              )}
                              onPress={() =>
                                modalType1(
                                  v.pertanyaanId,
                                  'ketersediaan',
                                  v.ketersediaan,
                                  data.variabelId,
                                )
                              }
                            />
                            <ItemCombo
                              title={'Kecukupan'}
                              label={getValueCombo('kecukupan', v.kecukupan)}
                              onPress={() =>
                                modalType1(
                                  v.pertanyaanId,
                                  'kecukupan',
                                  v.kecukupan,
                                  data.variabelId,
                                )
                              }
                            />
                            <ItemCombo
                              title={'Kelayakan'}
                              label={getValueCombo('kondisi', v.kondisi)}
                              onPress={() =>
                                modalType1(
                                  v.pertanyaanId,
                                  'kondisi',
                                  v.kondisi,
                                  data.variabelId,
                                )
                              }
                            />
                          </>
                        )}

                        {/* TYPE 2 */}
                        {type === 'Type 2' && (
                          <>
                            <ItemTextInput
                              title={'Kebutuhan'}
                              placeholder={'Ketikkan Kebutuhan...'}
                              defaultValue={v.kebutuhan?.toString() || ''}
                              keyboardType="numeric"
                              onChangeText={val => {
                                setValueType2(
                                  data.variabelId,
                                  v.pertanyaanId,
                                  'kebutuhan',
                                  val,
                                );
                                setValueType2Dispatch(
                                  `Kebutuhan${v.pertanyaanId?.toString()}`,
                                  val,
                                );
                              }}
                              value={
                                formMonev[
                                  `Kebutuhan${v.pertanyaanId?.toString()}`
                                ]
                              }
                            />
                            <ItemTextInput
                              title={'Keterisian'}
                              placeholder={'Ketikkan Keterisian...'}
                              defaultValue={v.keterisian?.toString() || ''}
                              keyboardType="numeric"
                              onChangeText={val => {
                                setValueType2(
                                  data.variabelId,
                                  v.pertanyaanId,
                                  'keterisian',
                                  val,
                                );
                                setValueType2Dispatch(
                                  `Keterisian${v.pertanyaanId?.toString()}`,
                                  val,
                                );
                              }}
                              value={
                                formMonev[
                                  `Keterisian${v.pertanyaanId?.toString()}`
                                ]
                              }
                            />
                          </>
                        )}

                        {/* TYPE 3 */}
                        {type === 'Type 3' && (
                          <ItemTextInput
                            title={'Banyaknya'}
                            placeholder={'Ketikkan Jumlah...'}
                            defaultValue={v.banyakPendidikan?.toString() || ''}
                            keyboardType="numeric"
                            onChangeText={val => {
                              setValueType2(
                                data.variabelId,
                                v.pertanyaanId,
                                'banyakPendidikan',
                                val,
                              );
                              setValueType2Dispatch(
                                `Jumlah${v.pertanyaanId?.toString()}`,
                                val,
                              );
                            }}
                            value={
                              formMonev[`Jumlah${v.pertanyaanId?.toString()}`]
                            }
                          />
                        )}

                        {/* TYPE 4 */}
                        {type === 'Type 4' && (
                          <ItemCombo
                            title={'Ketersediaan'}
                            label={getValueCombo(
                              'ketersediaan',
                              v.ketersediaan,
                            )}
                            onPress={() =>
                              modalType1(
                                v.pertanyaanId,
                                'ketersediaan',
                                v.ketersediaan,
                                data.variabelId,
                              )
                            }
                          />
                        )}
                      </View>
                    );
                  })}
                </List.Accordion>
              );
            })}
          </List.Section>
        )}
      </View>
    </View>
  );
};

const SectionContent = () => {
  const {
    dataAddMonevs,
    dataAddMonevSingle,
    dataSteps,
    currentStep,
    dataBobots,
    modalKetersediaan,
    modalKecukupan,
    modalKelayakan,
    jawabanInputMonev,
    selectKetersediaan,
  } = useSelector(state => state.monevReducers);
  const dispatch = useDispatch();

  const containerStyle = {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
  };

  const setJawabanType1Ketersediaan = async jawaban => {
    // SELECT JAWABANNYA
    dispatch({
      type: 'SELECT_KETERSEDIAAN',
      payload: {
        pertanyaanId: selectKetersediaan.pertanyaanId,
        objectname: selectKetersediaan.objectname,
        jawaban,
      },
    });

    // UPDATE JAWABAN UNTUK KE API
    let newArr = jawabanInputMonev.pertanyaan.map((item, i) => {
      if (selectKetersediaan.pertanyaanId === item.pertanyaanId) {
        return {...item, [selectKetersediaan.objectname]: jawaban};
      } else {
        return {...item};
      }
    });
    const jawabanApi = {
      indikatorId: jawabanInputMonev.indikatorId,
      type: jawabanInputMonev.type,
      pertanyaan: newArr,
    };
    dispatch({
      type: 'JAWABAN_INPUTMONEV',
      payload: jawabanApi,
    });

    ///MIGRATE
    await dataAddMonevSingle.indikator.map((v, i) => {
      if (v.indikatorId == jawabanInputMonev.indikatorId) {
        v.pertanyaan = newArr;
        console.log('aya');
      }
    });
    dispatch({type: 'DATA_ADD_MONEV_SINGLE', payload: dataAddMonevSingle});

    await dataAddMonevs.form.map((v, i) => {
      if (selectKetersediaan.variabelId === v.variabelId) {
        v.indikator = dataAddMonevSingle.indikator;
      }
    });
    dispatch({type: 'DATA_ADD_MONEVS', payload: dataAddMonevs});
    ///MIGRATE

    // TUTUP MODALNYA
    let payloadModal = false;
    if (selectKetersediaan.modalName === 'MODAL_KETERSEDIAAN')
      payloadModal = !modalKetersediaan;
    if (selectKetersediaan.modalName === 'MODAL_KECUKUPAN')
      payloadModal = !modalKecukupan;
    if (selectKetersediaan.modalName === 'MODAL_KELAYAKAN')
      payloadModal = !modalKelayakan;
    // setTimeout(() => {
    dispatch({
      type: selectKetersediaan.modalName,
      payload: payloadModal,
    });
    // }, 500);
  };

  return (
    <>
      <ScrollView>
        {dataSteps.length > 0 ? dataSteps[currentStep - 1].component() : <></>}
        <View style={{height: 80}} />
      </ScrollView>
      <Modal
        style={{marginHorizontal: 20, borderRadius: 10, marginTop: -40}}
        visible={modalKetersediaan}
        onDismiss={() =>
          dispatch({
            type: 'MODAL_KETERSEDIAAN',
            payload: !modalKetersediaan,
          })
        }
        contentContainerStyle={containerStyle}>
        <View style={{marginBottom: 15}}>
          <Text
            style={{
              fontSize: 15,
              fontFamily: fonts.primary['normal'],
              color: colors.black,
            }}>
            Pilih Ketersediaan
          </Text>
        </View>
        <ScrollView style={{paddingHorizontal: 5}}>
          {jsonKetersediaan.map((v, i) => {
            return selectKetersediaan.jawaban == v.value ? (
              <ItemModalType1 key={i} label={v.label} isChecked />
            ) : (
              <ItemModalType1
                key={i}
                label={v.label}
                onPress={() => setJawabanType1Ketersediaan(v.value)}
              />
            );
          })}
        </ScrollView>
      </Modal>
      <Modal
        style={{marginHorizontal: 20, borderRadius: 10, marginTop: -40}}
        visible={modalKecukupan}
        onDismiss={() =>
          dispatch({
            type: 'MODAL_KECUKUPAN',
            payload: !modalKecukupan,
          })
        }
        contentContainerStyle={containerStyle}>
        <View style={{marginBottom: 15}}>
          <Text
            style={{
              fontSize: 15,
              fontFamily: fonts.primary['normal'],
              color: colors.black,
            }}>
            Pilih Kecukupan
          </Text>
        </View>
        <ScrollView style={{paddingHorizontal: 5}}>
          {jsonKecukupan.map((v, i) => {
            return selectKetersediaan.jawaban == v.value ? (
              <ItemModalType1 key={i} label={v.label} isChecked />
            ) : (
              <ItemModalType1
                key={i}
                label={v.label}
                onPress={() => setJawabanType1Ketersediaan(v.value)}
              />
            );
          })}
        </ScrollView>
      </Modal>
      <Modal
        style={{marginHorizontal: 20, borderRadius: 10, marginTop: -40}}
        visible={modalKelayakan}
        onDismiss={() =>
          dispatch({
            type: 'MODAL_KELAYAKAN',
            payload: !modalKelayakan,
          })
        }
        contentContainerStyle={containerStyle}>
        <View style={{marginBottom: 15}}>
          <Text
            style={{
              fontSize: 15,
              fontFamily: fonts.primary['normal'],
              color: colors.black,
            }}>
            Pilih Kelayakan
          </Text>
        </View>
        <ScrollView style={{paddingHorizontal: 5}}>
          {dataBobots.map((v, i) => {
            return selectKetersediaan.jawaban == v.bobot ? (
              <ItemModalType1
                key={i}
                label={`${v.bobot} - ${v.nama}`}
                isChecked
              />
            ) : (
              <ItemModalType1
                key={i}
                label={`${v.bobot} - ${v.nama}`}
                onPress={() => setJawabanType1Ketersediaan(`${v.bobot}`)}
              />
            );
          })}
        </ScrollView>
      </Modal>
    </>
  );
};

const SectionFooter = ({navigation}) => {
  const {
    dataSteps,
    currentStep,
    expandTabInputMonev,
    jawabanInputMonev,
    dataAddMonevs,
  } = useSelector(state => state.monevReducers);
  const dispatch = useDispatch();

  const modalReset = () => {
    dispatch({
      type: 'MODAL_KETERSEDIAAN',
      payload: false,
    });
    dispatch({
      type: 'MODAL_KECUKUPAN',
      payload: false,
    });
    dispatch({
      type: 'MODAL_KELAYAKAN',
      payload: false,
    });
  };

  const previous = () => {
    dispatch({
      type: 'CURRENT_STEP',
      payload: currentStep <= 1 ? currentStep : currentStep - 1,
    });
    modalReset();
  };
  const next = () => {
    modalReset();
    dispatch({
      type: 'CURRENT_STEP',
      payload: currentStep >= dataSteps.length ? currentStep : currentStep + 1,
    });

    //set jawaban data akhir
    let data = dataSteps[currentStep - 1].data;
    const expand = {
      status: false,
      type: '',
    };
    dispatch({
      type: 'EXPAND_TAB_INPUTMONEV',
      payload: expand,
    });

    dispatch(
      saveMonevStep(dataAddMonevs, currentStep == dataSteps.length, navigation),
    );
  };

  var progressWidth = `0.${(currentStep / dataSteps.length) * 100}`;
  progressWidth = parseFloat(progressWidth === '0.100' ? 1 : progressWidth);
  return (
    <>
      <View
        style={{
          position: 'absolute',
          bottom: 0,
          backgroundColor: colors.black,
          paddingBottom: 30,
          width: '100%',
        }}>
        <ProgressBar progress={progressWidth} color={colors.yellow} />
        <View
          style={{
            paddingTop: 20,
            paddingHorizontal: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{width: '30%'}}>
            <Text
              style={{
                fontSize: 15,
                fontFamily: fonts.primary[600],
                color: colors.white,
              }}>
              {currentStep} of {dataSteps.length}
            </Text>
          </View>
          <View style={{width: '70%'}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              {currentStep !== 1 ? (
                <Button
                  type={'secondary'}
                  title={'Kembali'}
                  onPress={() => previous()}
                  width={'48%'}
                />
              ) : (
                <View style={{width: '48%'}} />
              )}
              <Button
                type={'primary'}
                title={
                  currentStep === dataSteps.length ? 'Selesaikan' : 'Lanjutkan'
                }
                onPress={() => next()}
                width={'48%'}
              />
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

const MonevInputStep = ({navigation}) => {
  const {dataSteps, currentStep, dataAddMonevs, dataBobots, formMonev} =
    useSelector(state => state.monevReducers);
  const dispatch = useDispatch();

  useEffect(() => {
    const steps = [];
    dataAddMonevs?.form?.map((v, i) => {
      steps.push({
        title: `${v.variabel}`,
        data: v,
        component: () => <FormDynamic data={v} />,
      });
    });
    dispatch({type: 'DATA_STEPS', payload: steps});
    dispatch({type: 'CURRENT_STEP', payload: 1});
  }, [dispatch]);

  return (
    <>
      <StatusBar translucent barStyle={'dark-content'} />
      <View style={styles.container}>
        {/* SECTION HEADER */}
        <Header
          title={'Form Input'}
          iconLeft={
            <Icon name="keyboard-backspace" color={colors.dark} size={25} />
          }
          onPressLeft={() => navigation.goBack()}
        />
        {/* SECTION CONTENT */}

        <View
          style={{
            flex: 1,
            backgroundColor: colors.snow,
          }}>
          <SectionContent />
        </View>

        {/* SECTION FOOTER */}
        <SectionFooter navigation={navigation} />
      </View>
    </>
  );
};

export default MonevInputStep;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: getStatusBarHeight() + 20,
  },
});
