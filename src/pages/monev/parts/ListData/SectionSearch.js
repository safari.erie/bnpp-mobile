import {StyleSheet, TextInput, View, Platform} from 'react-native';
import React, {useState} from 'react';
import {Gap} from '../../../../components';
import {fonts, colors} from '../../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import {getMonevs, setFormMonev} from '../../../../redux/actions';

const SectionSearch = () => {
  const {dataMonevs, formMonev, modalComboMonev} = useSelector(
    state => state.monevReducers,
  );
  const dispatch = useDispatch();
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={15} />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: colors.white,
          paddingHorizontal: 20,
          paddingVertical: Platform.OS == 'ios' ? 10 : 7,
          borderRadius: 10,
        }}>
        <View style={{paddingRight: 15}}>
          <Icon name="map-search" color={colors.dark} size={25} />
        </View>
        <View style={{flex: 1}}>
          <TextInput
            style={{
              fontFamily: fonts.primary['normal'],
              color: colors.dark,
              fontSize: 14,
            }}
            placeholder="Cari Kecamatan..."
            onChangeText={e => dispatch(setFormMonev('FilterPencarian', e))}
            value={formMonev.FilterPencarian}
          />
        </View>
      </View>
    </View>
  );
};

export default SectionSearch;

const styles = StyleSheet.create({});
