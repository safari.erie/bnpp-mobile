import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import ItemCardContent from './ItemCardContent';
import {fonts, colors} from '../../../../../utils';
import {IconKecMonev} from '../../../../../assets';

const ItemCard = ({no, kecataman, idKec, nama, provinsi, ciq, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        borderRadius: 10,
        padding: 15,
        backgroundColor: colors.white,
        marginBottom: 10,
      }}>
      {/* HEADER */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingBottom: 10,
          borderBottomWidth: 0.7,
          borderBottomColor: colors.greySecondary,
        }}>
        <View style={{flex: 1}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              width: '100%',
            }}>
            <View style={{paddingRight: 5}}>
              <IconKecMonev width={30} height={30} />
            </View>
            <View style={{flex: 1}}>
              <Text
                numberOfLines={1}
                style={{
                  fontSize: 13,
                  fontFamily: fonts.primary['normal'],
                  color: colors.dark,
                  maxWidth: '90%',
                }}>
                {kecataman}
              </Text>
              <Text
                style={{
                  fontSize: 10,
                  fontFamily: fonts.primary[400],
                  color: colors.grey,
                }}>
                {idKec}
              </Text>
            </View>
          </View>
        </View>
        <View>
          <Text
            style={{
              fontSize: 16,
              fontFamily: fonts.primary['normal'],
              color: colors.dark,
            }}>
            #{no}
          </Text>
        </View>
      </View>

      {/* CONTENT */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          flexWrap: 'wrap',
          width: '100%',
        }}>
        <ItemCardContent title={'Nama'} value={nama} />
        <ItemCardContent title={'Kecamatan'} value={kecataman} />
        <ItemCardContent title={'Provinsi'} value={provinsi} />
        <ItemCardContent title={'Rata - Rata'} value={ciq} />
      </View>
    </TouchableOpacity>
  );
};

export default ItemCard;

const styles = StyleSheet.create({});
