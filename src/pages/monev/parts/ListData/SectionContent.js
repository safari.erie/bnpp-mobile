import {
  FlatList,
  Platform,
  RefreshControl,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React, {useState, useEffect, useCallback} from 'react';
import ItemCard from './components/ItemCard';
import {Gap} from '../../../../components';
import {useDispatch, useSelector} from 'react-redux';
import {getMonevs, setFormMonev} from '../../../../redux/actions';
import {colors, fonts} from '../../../../utils';
import {ActivityIndicator} from 'react-native-paper';
import SectionSearch from './SectionSearch';
import SectionFilter from './SectionFilter';

const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

const SectionContent = ({navigation}) => {
  const {dataMonevs, formMonev, filterPencarianMonev} = useSelector(
    state => state.monevReducers,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setFormMonev('IsLoading', true));
    getData();
    return () => {};
  }, [formMonev.PageCurrent]);

  const onRefresh = useCallback(() => {
    dispatch(setFormMonev('FilterPencarian', ''));
    dispatch(setFormMonev('Refreshing', true));
    getData();
    dispatch(setFormMonev('PageCurrent', 1));
    wait(2000).then(() => dispatch(setFormMonev('Refreshing', false)));
  }, []);

  const getData = () => {
    dispatch(
      getMonevs(formMonev.FilterTahun, formMonev.PageCurrent, 30, dataMonevs),
    );
  };

  const dataList = !formMonev.FilterPencarian
    ? dataMonevs
    : dataMonevs?.filter(i =>
        i?.nama
          ?.toLowerCase()
          .includes(formMonev.FilterPencarian.toLowerCase()),
      );

  const renderHeader = () => {
    return <></>;
  };

  const renderFooter = () => {
    return formMonev.IsLoading ? (
      <View style={{marginVertical: 15, alignItems: 'center'}}>
        <ActivityIndicator size="small" color={colors.yellow} />
        <Gap height={70} />
      </View>
    ) : (
      <View style={{marginVertical: 15, alignItems: 'center'}}>
        <Text
          style={{
            fontSize: 11,
            fontFamily: fonts.primary[400],
            color: colors.dark,
          }}>
          ✨ Tidak ada lagi data
        </Text>
        <Gap height={100} />
      </View>
    );
  };

  const renderItem = v => {
    return (
      <View style={{paddingHorizontal: 15}}>
        <ItemCard
          no={v.item.id}
          kecataman={v.item.districtName?.toUpperCase()}
          idKec={v.item.distictId}
          nama={v.item.nama?.toUpperCase() || 'No Name'}
          provinsi={v.item.provName?.toUpperCase()}
          ciq={`${v.item.rataRataCiq || 0} (CIQ) / ${
            v.item.rataRataPap || 0
          } (PAP)`}
          onPress={() =>
            navigation.navigate('MonevDetail', {
              PARAMId: v.item.id,
              PARAMData: v.item,
            })
          }
        />
      </View>
    );
  };

  const handleLoadMore = () => {
    !formMonev.CekData
      ? dispatch(setFormMonev('IsLoading', false))
      : dispatch(setFormMonev('IsLoading', true));
    if (formMonev.IsLoading)
      dispatch(setFormMonev('PageCurrent', formMonev.PageCurrent + 1));
  };

  return (
    <>
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={formMonev.Refreshing}
            onRefresh={onRefresh}
          />
        }
        numColumns={1}
        data={dataList}
        renderItem={renderItem}
        keyExtractor={(v, i) => i.toString()}
        ListHeaderComponent={renderHeader}
        ListFooterComponent={renderFooter}
        onEndReached={handleLoadMore}
        onEndReachedThreshold={Platform.OS === 'ios' ? 0 : 1}
      />
    </>
  );
};

export default SectionContent;

const styles = StyleSheet.create({});
