import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Gap} from '../../../../components';
import {fonts, colors} from '../../../../utils';

const SectionFilter = ({tahun}) => {
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={25} />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View style={{paddingRight: 15}}>
          <Text
            style={{
              fontSize: 15,
              fontFamily: fonts.primary['normal'],
              color: colors.dark,
            }}>
            Filter Pencarian :
          </Text>
        </View>
        <View>
          <View
            style={{
              paddingHorizontal: 13,
              paddingVertical: 7,
              borderRadius: 20,
              backgroundColor: colors.yellow,
            }}>
            <Text
              style={{
                fontSize: 13,
                fontFamily: fonts.primary[600],
                color: colors.white,
              }}>
              Tahun : {tahun}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default SectionFilter;

const styles = StyleSheet.create({});
