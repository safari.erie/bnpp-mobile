import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts} from '../../../../utils';
import ItemVariabel from './components/ItemVariabel';
import {Gap} from '../../../../components';
import {useDispatch, useSelector} from 'react-redux';

const SectionContent = ({data, navigation}) => {
  const {modalComboMonev} = useSelector(state => state.monevReducers);
  const dispatch = useDispatch();

  const showModal = (variabel, data) => {
    dispatch({
      type: 'DATA_MONEV_DETAIL_INDIKATOR',
      payload: {
        variabel,
        data,
      },
    });
    dispatch({
      type: 'MODAL_COMBO_MONEV',
      payload: !modalComboMonev,
    });
  };
  return (
    <View style={{paddingHorizontal: 15}}>
      <Gap height={15} />
      <Text
        style={{
          fontSize: 15,
          fontFamily: fonts.primary['normal'],
          color: colors.dark,
        }}>
        Daftar Jawaban
      </Text>
      <Gap height={10} />
      {data?.detail_monev.map((v, i) => {
        return (
          <ItemVariabel
            key={i}
            title={v.variabel.nama}
            onPress={() =>
              navigation.navigate('MonevResult', {
                PARAMVariabel: `${v.variabel.nama}`,
                PARAMIndikator: v.indikator,
              })
            }
          />
        );
      })}
    </View>
  );
};

export default SectionContent;

const styles = StyleSheet.create({});
