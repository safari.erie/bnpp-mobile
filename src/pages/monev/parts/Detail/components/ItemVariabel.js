import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts} from '../../../../../utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {TouchableOpacity} from 'react-native-gesture-handler';

const ItemVariabel = ({title, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        padding: 20,
        borderRadius: 10,
        backgroundColor: colors.white,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10,
      }}>
      <Text
        style={{
          fontSize: 13,
          fontFamily: fonts.primary['normal'],
          color: colors.dark,
        }}>
        {title}
      </Text>
      <Icon name="arrow-right" color={colors.dark} size={15} />
    </TouchableOpacity>
  );
};

export default ItemVariabel;

const styles = StyleSheet.create({});
