import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts} from '../../../../../utils';

const ItemIndikatorJawaban = ({width, title, value}) => {
  return (
    <View style={{width: width, backgroundColor: colors.blue}}>
      <Text
        numberOfLines={1}
        style={{
          fontSize: 12,
          fontFamily: fonts.primary[400],
          color: colors.white,
          textAlign: 'center',
        }}>
        {title}
      </Text>
      <Text
        numberOfLines={1}
        style={{
          fontSize: 14,
          fontFamily: fonts.primary[600],
          color: colors.white,
          textAlign: 'center',
        }}>
        {value}
      </Text>
    </View>
  );
};

export default ItemIndikatorJawaban;

const styles = StyleSheet.create({});
