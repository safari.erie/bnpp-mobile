import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {fonts} from '../../../../../utils/fonts';
import {colors} from '../../../../../utils/colors';

const ItemCombo = ({onPress, label, title}) => {
  return (
    <View style={{paddingHorizontal: 10, paddingTop: 5}}>
      <Text
        style={{
          fontSize: 13,
          fontFamily: fonts.primary[400],
          color: colors.dark,
        }}>
        {title} :
      </Text>
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: 10,
          borderRadius: 5,
          marginBottom: 10,
          borderWidth: 2,
          borderColor: '#F5F5F5',
          marginTop: 10,
        }}
        onPress={onPress}>
        <Text
          style={{
            fontSize: 13,
            fontFamily: fonts.primary[300],
            color: colors.dark,
          }}>
          {label || 'Pilih Salah Satu'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default ItemCombo;

const styles = StyleSheet.create({});
