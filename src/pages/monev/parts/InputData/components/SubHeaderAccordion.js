import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {fonts} from '../../../../../utils/fonts';

const SubHeaderAccordion = ({name}) => {
  return (
    <Text
      style={{
        fontSize: 14,
        fontFamily: fonts.primary[4400],
        color: '#3F3D56',
        marginBottom: 5,
      }}>
      {name}
    </Text>
  );
};

export default SubHeaderAccordion;

const styles = StyleSheet.create({});
