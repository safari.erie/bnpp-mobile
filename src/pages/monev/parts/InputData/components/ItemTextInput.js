import {StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';
import {colors, fonts} from '../../../../../utils';

const ItemTextInput = ({...item}) => {
  return (
    <View style={{paddingHorizontal: 10, paddingTop: 5}}>
      <Text
        style={{
          fontSize: 13,
          fontFamily: fonts.primary[400],
          color: colors.dark,
        }}>
        {item.title}
      </Text>
      <TextInput
        style={{
          borderWidth: 2,
          borderColor: '#F5F5F5',
          padding: 10,
          marginVertical: 10,
          fontSize: 13,
          fontFamily: fonts.primary[300],
          color: colors.dark,
          borderRadius: 5,
        }}
        {...item}
      />
    </View>
  );
};

export default ItemTextInput;
