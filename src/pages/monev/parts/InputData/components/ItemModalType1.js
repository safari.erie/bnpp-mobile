import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {MAIN_COLOR} from '../../../../../utils/constant_color';
import {colors} from '../../../../../utils/colors';
import Icon from 'react-native-vector-icons/AntDesign';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {fonts} from '../../../../../utils';

const ItemModalType1 = ({label, onPress, isChecked = false}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingBottom: 10,
        borderBottomWidth: 0.7,
        borderBottomColor: '#ededed',
      }}>
      <Text
        style={{
          fontSize: 14,
          fontFamily: fonts.primary[400],
          color: colors.dark,
        }}>
        {label}
      </Text>
      {isChecked && <Icon name="check" color={colors.dark} size={20} />}
    </TouchableOpacity>
  );
};

export default ItemModalType1;

const styles = StyleSheet.create({});
