import {StyleSheet, Text, View, StatusBar, ScrollView} from 'react-native';
import React, {useEffect} from 'react';
import {colors, fonts, FUNCToast} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Button, Gap, Header} from '../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ItemCombo from './parts/InputData/components/ItemCombo';
import ItemTextInfo from './parts/InputData/components/ItemTextInfo';
import {useDispatch, useSelector} from 'react-redux';
import {
  addMonevs,
  getComboCity,
  getComboDistrict,
  getComboProvince,
  getJenisLokpris,
  setFormMonev,
} from '../../redux/actions';
import {Modal} from 'react-native-paper';
import ItemModalType1 from './parts/InputData/components/ItemModalType1';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import ItemTextInput from './parts/InputData/components/ItemTextInput';

const MonevInput = ({navigation}) => {
  const {modalComboMonev, dataJenisLokpris, formMonev} = useSelector(
    state => state.monevReducers,
  );
  const {dataComboProvince, dataComboCity, dataComboDistrict} = useSelector(
    state => state.comboReducers,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getJenisLokpris());
    dispatch(getComboProvince());
  }, [dispatch]);

  const showModal = (val = null) => {
    if (val) dispatch(setFormMonev('ObjName', val));
    dispatch({
      type: 'MODAL_COMBO_MONEV',
      payload: !modalComboMonev,
    });
  };

  const onNumberTelepon = (key, text) => {
    let newText = '';
    let numbers = '0123456789';

    for (var i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText = newText + text[i];
      } else {
        FUNCToast('WARN', {msg: 'Wajib Numerik'});
      }
    }
    if (text.length > 13) {
      FUNCToast('WARN', {msg: 'Tidak boleh lebih dari 13 digit'});
      return;
    }
    dispatch(setFormMonev(key, newText));
  };

  const btnNext = async () => {
    const session = await AsyncStorageLib.getItem('dataUser');
    var user = JSON.parse(session);
    dispatch(addMonevs(user.iduser, formMonev, navigation));
  };

  return (
    <>
      <StatusBar translucent barStyle={'dark-content'} />
      <View style={styles.container}>
        {/* SECTION HEADER */}
        <Header
          title={'Input Monev'}
          iconLeft={
            <Icon name="keyboard-backspace" color={colors.dark} size={25} />
          }
          onPressLeft={() => navigation.goBack()}
        />
        {/* SECTION CONTENT */}
        <View
          style={{
            flex: 1,
            backgroundColor: colors.snow,
          }}>
          <ScrollView>
            <View
              style={{
                padding: 15,
              }}>
              <View
                style={{
                  padding: 20,
                  borderRadius: 10,
                  backgroundColor: colors.white,
                  marginBottom: 20,
                }}>
                <ItemCombo
                  title={'Provinsi'}
                  label={formMonev?.IdProv?.label}
                  onPress={() => showModal('IdProv')}
                />
                <ItemCombo
                  title={'Kabupaten/Kota'}
                  label={formMonev?.IdCity?.label}
                  onPress={() =>
                    dataComboCity.length !== 0 && showModal('IdCity')
                  }
                />
                <ItemCombo
                  title={'Kecamatan'}
                  label={formMonev?.IdDistrict?.label}
                  onPress={() =>
                    dataComboDistrict.length !== 0 && showModal('IdDistrict')
                  }
                />
                <ItemCombo
                  title={'Kategori'}
                  label={formMonev?.Lokpri?.label}
                  onPress={() => showModal('Lokpri')}
                />
                <ItemTextInput
                  title={'Nama'}
                  placeholder={'Ketikkan Nama...'}
                  onChangeText={val => dispatch(setFormMonev('Nama', val))}
                  value={formMonev.Nama}
                />
                <ItemTextInput
                  title={'Jabatan'}
                  placeholder={'Ketikkan Jabatan...'}
                  onChangeText={val => dispatch(setFormMonev('Jabatan', val))}
                  value={formMonev.Jabatan}
                />
                <ItemTextInput
                  title={'No. Handphone'}
                  placeholder={'Ketikkan No. Handphone...'}
                  keyboardType="numeric"
                  onChangeText={e => onNumberTelepon('Nohp', e)}
                  value={formMonev.Nohp}
                />
                <ItemTextInput
                  title={'Email'}
                  placeholder={'Ketikkan Email...'}
                  onChangeText={val => dispatch(setFormMonev('Email', val))}
                  value={formMonev.Email}
                />
                <ItemTextInput
                  title={'Alamat Kantor'}
                  placeholder={'Ketikkan Alamat Kantor...'}
                  onChangeText={val =>
                    dispatch(setFormMonev('AlamatKantor', val))
                  }
                  value={formMonev.AlamatKantor}
                />
                {/* <ItemCombo
                  title={'Jabatan'}
                  label={null}
                  onPress={() => alert('oi')}
                />
                <ItemCombo
                  title={'Nama'}
                  label={null}
                  onPress={() => alert('oi')}
                /> */}

                {/* section info kantor */}
                {/* <View
                  style={{
                    margin: 10,
                    padding: 20,
                    borderRadius: 10,
                    backgroundColor: colors.bluelight,
                  }}>
                  <View style={{position: 'absolute', right: -10, top: -10}}>
                    <View
                      style={{
                        padding: 10,
                        borderRadius: 100,
                        backgroundColor: colors.yellow,
                      }}>
                      <Icon
                        name="information-outline"
                        color={colors.greySecondary}
                        size={25}
                      />
                    </View>
                  </View>
                  <ItemTextInfo
                    title={'Alamat Kantor :'}
                    desc={'Perum RSCM Blok E.2 / 1 RT 005/009'}
                  />
                  <Gap height={10} />
                  <ItemTextInfo
                    title={'Email :'}
                    desc={'gmail@salimseal.com'}
                  />
                  <Gap height={10} />
                  <ItemTextInfo title={'Nomor HP :'} desc={'0111117201'} />
                </View>
                 */}
              </View>

              {/* section button */}
              <Button
                type={'primary'}
                title={'Lanjutkan'}
                onPress={() => btnNext()}
                width={'100%'}
              />
            </View>

            <View style={{height: 80}} />
          </ScrollView>
        </View>
      </View>

      <Modal
        style={{marginHorizontal: 20, borderRadius: 10}}
        visible={modalComboMonev}
        onDismiss={() => showModal()}
        contentContainerStyle={{
          backgroundColor: colors.white,
          padding: 20,
          borderRadius: 10,
          maxHeight: hp('60%'),
        }}>
        <View style={{marginBottom: 15}}>
          <Text
            style={{
              fontSize: 15,
              fontFamily: fonts.primary['normal'],
              color: colors.black,
            }}>
            Pilih Salah Satu
          </Text>
        </View>
        <ScrollView
          style={{paddingHorizontal: 5}}
          showsVerticalScrollIndicator={false}>
          {formMonev.ObjName === 'IdProv' &&
            dataComboProvince?.map((v, i) => {
              return formMonev?.IdProv?.value == v.IdProvinsi ? (
                <ItemModalType1 key={i} label={v.NamaProvinsi} isChecked />
              ) : (
                <ItemModalType1
                  key={i}
                  label={v.NamaProvinsi}
                  onPress={() => {
                    dispatch(
                      setFormMonev(formMonev.ObjName, {
                        label: v.NamaProvinsi,
                        value: v.IdProvinsi,
                      }),
                    );
                    dispatch(getComboCity(v.IdProvinsi));
                    showModal();
                  }}
                />
              );
            })}
          {formMonev.ObjName === 'IdCity' &&
            dataComboCity?.map((v, i) => {
              return formMonev?.IdCity?.value == v.IdCity ? (
                <ItemModalType1 key={i} label={v.NamaKota} isChecked />
              ) : (
                <ItemModalType1
                  key={i}
                  label={v.NamaKota}
                  onPress={() => {
                    dispatch(
                      setFormMonev(formMonev.ObjName, {
                        label: v.NamaKota,
                        value: v.IdCity,
                      }),
                    );
                    dispatch(getComboDistrict(v.IdCity));
                    showModal();
                  }}
                />
              );
            })}
          {formMonev.ObjName === 'IdDistrict' &&
            dataComboDistrict?.map((v, i) => {
              return formMonev?.IdDistrict?.value == v.IdKecamatan ? (
                <ItemModalType1 key={i} label={v.NamaKecamatan} isChecked />
              ) : (
                <ItemModalType1
                  key={i}
                  label={v.NamaKecamatan}
                  onPress={() => {
                    dispatch(
                      setFormMonev(formMonev.ObjName, {
                        label: v.NamaKecamatan,
                        value: v.IdKecamatan,
                      }),
                    );
                    dispatch(getComboDistrict(v.IdKecamatan));
                    showModal();
                  }}
                />
              );
            })}
          {formMonev.ObjName === 'Lokpri' &&
            dataJenisLokpris.map((v, i) => {
              return formMonev?.Lokpri?.value == v.value ? (
                <ItemModalType1 key={i} label={v.label} isChecked />
              ) : (
                <ItemModalType1
                  key={i}
                  label={v.label}
                  onPress={() => {
                    dispatch(
                      setFormMonev(formMonev.ObjName, {
                        label: v.label,
                        value: v.value,
                      }),
                    );
                    showModal();
                  }}
                />
              );
            })}
        </ScrollView>
      </Modal>
    </>
  );
};

export default MonevInput;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: getStatusBarHeight() + 20,
  },
});
