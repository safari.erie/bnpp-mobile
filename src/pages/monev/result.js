import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getDetailMonev} from '../../redux/actions';
import {Gap, Header} from '../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {colors, fonts} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ItemCard from './parts/ListData/components/ItemCard';
import SectionContent from './parts/Detail/SectionContent';
import {Modal, List} from 'react-native-paper';
import SubHeaderAccordion from './parts/InputData/components/SubHeaderAccordion';
import ItemIndikatorJawaban from './parts/Detail/components/ItemIndikatorJawaban';

const MonevResult = ({navigation, route}) => {
  const {PARAMVariabel, PARAMIndikator} = route.params;
  const [expanded, setExpanded] = useState({status: false});

  const expandSave = indikator => {
    const expandx = {
      status: !expanded.status,
      type: `${indikator}`,
    };
    setExpanded(expandx);
  };
  return (
    <>
      <StatusBar translucent barStyle={'dark-content'} />
      <View style={styles.wrapper}>
        {/* SECTION HEADER */}
        <Header
          title={'Jawaban Monev'}
          iconLeft={
            <Icon name="keyboard-backspace" color={colors.dark} size={25} />
          }
          onPressLeft={() => navigation.goBack()}
        />
        <View style={styles.container}>
          <ScrollView>
            {/* section content */}
            <View style={{paddingHorizontal: 15}}>
              <Gap height={20} />
              <View
                style={{
                  padding: 20,
                  borderRadius: 10,
                  backgroundColor: colors.white,
                }}>
                <List.Section title={PARAMVariabel}>
                  {PARAMIndikator?.map((v, i) => {
                    let type = v.type;
                    return (
                      <List.Accordion
                        key={i}
                        title={
                          <Text
                            style={{
                              fontSize: 14,
                              fontFamily: fonts.primary['normal'],
                              color: colors.dark,
                            }}>
                            {v.indikator}
                          </Text>
                        }
                        expanded={
                          expanded.type === `${v.indikator}` && !expanded.status
                        }
                        onPress={() => expandSave(`${v.indikator}`)}>
                        <View
                          style={{marginVertical: 10, marginHorizontal: 20}}>
                          {v.pertanyaan.map((v, i) => {
                            return (
                              <View key={i} style={{marginBottom: 10}}>
                                <SubHeaderAccordion name={v.pertanyaan} />

                                {/* section jawaban type 1 */}
                                {type === 'Type 1' && (
                                  <View
                                    style={{
                                      justifyContent: 'center',
                                      flex: 1,
                                      width: '100%',
                                      backgroundColor: colors.snow,
                                    }}>
                                    <View
                                      style={{
                                        flexDirection: 'row',
                                        flexWrap: 'wrap',
                                        width: '100%',
                                        justifyContent: 'center',
                                      }}>
                                      <ItemIndikatorJawaban
                                        width={'33%'}
                                        title="Ketersediaan"
                                        value={v.ketersediaan || '-'}
                                      />
                                      <ItemIndikatorJawaban
                                        width={'33%'}
                                        title="Kecukupan"
                                        value={v.kecukupan || '-'}
                                      />
                                      <ItemIndikatorJawaban
                                        width={'33%'}
                                        title="Kelayakan"
                                        value={v.kondisi || '-'}
                                      />
                                    </View>
                                  </View>
                                )}

                                {/* section jawaban type 2 */}
                                {type === 'Type 2' && (
                                  <View
                                    style={{
                                      justifyContent: 'center',
                                      flex: 1,
                                      width: '100%',
                                      backgroundColor: colors.snow,
                                    }}>
                                    <View
                                      style={{
                                        flexDirection: 'row',
                                        flexWrap: 'wrap',
                                        width: '100%',
                                        justifyContent: 'center',
                                      }}>
                                      <ItemIndikatorJawaban
                                        width={'50%'}
                                        title="Kebutuhan"
                                        value={v.kebutuhan}
                                      />
                                      <ItemIndikatorJawaban
                                        width={'50%'}
                                        title="Keterisian"
                                        value={v.keterisian}
                                      />
                                    </View>
                                  </View>
                                )}

                                {/* section jawaban type 3 */}
                                {type === 'Type 3' && (
                                  <View
                                    style={{
                                      justifyContent: 'center',
                                      flex: 1,
                                      width: '100%',
                                      backgroundColor: colors.snow,
                                    }}>
                                    <View
                                      style={{
                                        flexDirection: 'row',
                                        flexWrap: 'wrap',
                                        width: '100%',
                                        justifyContent: 'center',
                                      }}>
                                      <ItemIndikatorJawaban
                                        width={'100%'}
                                        title="Banyaknya"
                                        value={v.banyakPendidikan}
                                      />
                                    </View>
                                  </View>
                                )}

                                {/* section jawaban type 4 */}
                                {type === 'Type 4' && (
                                  <View
                                    style={{
                                      justifyContent: 'center',
                                      flex: 1,
                                      width: '100%',
                                      backgroundColor: colors.snow,
                                    }}>
                                    <View
                                      style={{
                                        flexDirection: 'row',
                                        flexWrap: 'wrap',
                                        width: '100%',
                                        justifyContent: 'center',
                                      }}>
                                      <ItemIndikatorJawaban
                                        width={'100%'}
                                        title="Ketersediaan"
                                        value={v.ketersediaan}
                                      />
                                    </View>
                                  </View>
                                )}
                              </View>
                            );
                          })}
                        </View>
                      </List.Accordion>
                    );
                  })}
                </List.Section>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default MonevResult;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: getStatusBarHeight() + 20,
  },
  container: {
    flex: 1,
    backgroundColor: colors.snow,
  },
});
