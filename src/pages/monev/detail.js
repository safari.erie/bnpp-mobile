import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getDetailMonev} from '../../redux/actions';
import {Gap, Header} from '../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {colors, fonts} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import ItemCard from './parts/ListData/components/ItemCard';
import SectionContent from './parts/Detail/SectionContent';

const MonevDetail = ({route, navigation}) => {
  const {PARAMId, PARAMData} = route.params;
  const {dataMonevDetail, dataMonevDetailIndikator, modalComboMonev} =
    useSelector(state => state.monevReducers);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDetailMonev(PARAMId));
  }, []);

  return (
    <>
      <StatusBar translucent barStyle={'dark-content'} />
      <View style={styles.wrapper}>
        {/* SECTION HEADER */}
        <Header
          title={'Detail Monev'}
          iconLeft={
            <Icon name="keyboard-backspace" color={colors.dark} size={25} />
          }
          onPressLeft={() => navigation.goBack()}
        />
        <View style={styles.container}>
          {/* section card */}
          {PARAMData !== null && (
            <View style={{paddingHorizontal: 15}}>
              <Gap height={20} />
              <ItemCard
                no={PARAMData.id}
                kecataman={PARAMData.districtName?.toUpperCase()}
                idKec={PARAMData.distictId}
                nama={PARAMData.nama?.toUpperCase() || 'No Name'}
                provinsi={PARAMData.provName?.toUpperCase()}
                ciq={`${PARAMData.rataRataCiq || 0} (CIQ) / ${
                  PARAMData.rataRataPap || 0
                } (PAP)`}
              />
            </View>
          )}

          {/* section variabel */}
          <SectionContent data={dataMonevDetail} navigation={navigation} />
        </View>
      </View>
    </>
  );
};

export default MonevDetail;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: getStatusBarHeight() + 20,
  },
  container: {
    flex: 1,
    backgroundColor: colors.snow,
  },
});
