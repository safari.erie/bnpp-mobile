import {StyleSheet, Text, View, StatusBar, ScrollView} from 'react-native';
import React from 'react';
import {colors, fonts, FUNCPreviousYears} from '../../utils';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Gap, Header} from '../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import {setFormMonev, getMonevs} from '../../redux/actions';
import SectionContent from './parts/ListData/SectionContent';
import ItemModalType1 from './parts/InputData/components/ItemModalType1';
import {Modal} from 'react-native-paper';
import SectionSearch from './parts/ListData/SectionSearch';
import SectionFilter from './parts/ListData/SectionFilter';

const MonevList = ({navigation}) => {
  const {formMonev, modalComboMonev} = useSelector(
    state => state.monevReducers,
  );
  const dispatch = useDispatch();

  const showModal = () => {
    dispatch(setFormMonev('Tahuns', FUNCPreviousYears(5)));

    dispatch({
      type: 'MODAL_COMBO_MONEV',
      payload: !modalComboMonev,
    });
  };

  return (
    <>
      <StatusBar translucent barStyle={'dark-content'} />
      <View style={styles.wrapper}>
        {/* SECTION HEADER */}
        <Header
          title={'Data Monev'}
          iconLeft={
            <Icon name="keyboard-backspace" color={colors.dark} size={25} />
          }
          onPressLeft={() => navigation.goBack()}
          iconRight={
            <Icon name="filter-outline" color={colors.dark} size={25} />
          }
          onPressRight={() => showModal()}
        />
        {/* SECTION CONTENT */}
        <View style={styles.container}>
          <SectionSearch
          // setFilterPencarian={setFilterPencarian}
          // filterPencarian={filterPencarian}
          />

          {/* SECTION FILTER */}
          <SectionFilter tahun={formMonev.FilterTahun} />
          <Gap height={20} />

          {/* SECTION CARD */}
          <SectionContent navigation={navigation} />
        </View>
      </View>
      <Modal
        style={{marginHorizontal: 20, borderRadius: 10}}
        visible={modalComboMonev}
        onDismiss={() => showModal()}
        contentContainerStyle={{
          backgroundColor: 'white',
          padding: 20,
          borderRadius: 10,
        }}>
        <View style={{marginBottom: 15}}>
          <Text
            style={{
              fontSize: 15,
              fontFamily: fonts.primary['normal'],
              color: colors.black,
            }}>
            Pilih Tahun
          </Text>
        </View>
        <ScrollView style={{paddingHorizontal: 5}}>
          {formMonev?.Tahuns?.map((v, i) => {
            return formMonev.FilterTahun === v ? (
              <ItemModalType1 key={i} label={v} isChecked />
            ) : (
              <ItemModalType1
                key={i}
                label={v}
                onPress={() => {
                  dispatch(setFormMonev('FilterTahun', v));
                  showModal();
                  dispatch(getMonevs(v, 1, 30));
                }}
              />
            );
          })}
        </ScrollView>
      </Modal>
    </>
  );
};

export default MonevList;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: getStatusBarHeight() + 20,
  },
  container: {
    flex: 1,
    backgroundColor: colors.snow,
  },
});
