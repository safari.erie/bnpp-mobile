import React from 'react';

import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
const Stack = createStackNavigator();

import {
  Splash,
  InfraKecamatan,
  InfraKecamatanDetail,
  InfraKecamatanDetailAset,
  InfraKecamatanDetailMobilitas,
  InfraKecamatanDetailInfoKec,
  InfraKecamatanDetailPegawai,
  InfraKecamatanDetailPenduduk,
  InfraDesa,
  InfraDesaDetail,
  InfraDesaDetailKades,
  InfraDesaDetailBalaiDesa,
  InfraDesaDetailKantorDesa,
  MonevList,
  MonevInput,
  MonevInputStep,
  MonevDetail,
  MonevResult,
  BeritaList,
  BeritaDetail,
  DetailNotifikasi,
  EditProfil,
  UbahPassword,
  PreviewImage,
} from '../pages';
import MainApp from './MainApp';
import Drawer from './drawer';

const itemConfigOpenClose = {
  animation: 'spring',
  config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};

const configBottomBar = {
  cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
  transitionSpec: {
    open: itemConfigOpenClose,
    close: itemConfigOpenClose,
  },
  headerShown: false,
};

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="MainApp" screenOptions={configBottomBar}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="MainApp" component={Drawer} />
      <Stack.Screen name="InfraKecamatan" component={InfraKecamatan} />
      <Stack.Screen
        name="InfraKecamatanDetail"
        component={InfraKecamatanDetail}
      />
      <Stack.Screen
        name="InfraKecamatanDetailAset"
        component={InfraKecamatanDetailAset}
      />
      <Stack.Screen
        name="InfraKecamatanDetailMobilitas"
        component={InfraKecamatanDetailMobilitas}
      />
      <Stack.Screen
        name="InfraKecamatanDetailInfoKec"
        component={InfraKecamatanDetailInfoKec}
      />
      <Stack.Screen
        name="InfraKecamatanDetailPegawai"
        component={InfraKecamatanDetailPegawai}
      />
      <Stack.Screen
        name="InfraKecamatanDetailPenduduk"
        component={InfraKecamatanDetailPenduduk}
      />

      <Stack.Screen name="InfraDesa" component={InfraDesa} />
      <Stack.Screen name="InfraDesaDetail" component={InfraDesaDetail} />
      <Stack.Screen
        name="InfraDesaDetailKades"
        component={InfraDesaDetailKades}
      />
      <Stack.Screen
        name="InfraDesaDetailBalaiDesa"
        component={InfraDesaDetailBalaiDesa}
      />
      <Stack.Screen
        name="InfraDesaDetailKantorDesa"
        component={InfraDesaDetailKantorDesa}
      />

      <Stack.Screen name="MonevList" component={MonevList} />
      <Stack.Screen name="MonevInput" component={MonevInput} />
      <Stack.Screen name="MonevInputStep" component={MonevInputStep} />
      <Stack.Screen name="MonevDetail" component={MonevDetail} />
      <Stack.Screen name="MonevResult" component={MonevResult} />

      <Stack.Screen name="BeritaList" component={BeritaList} />
      <Stack.Screen name="BeritaDetail" component={BeritaDetail} />
      <Stack.Screen name="DetailNotifikasi" component={DetailNotifikasi} />
      <Stack.Screen name="EditProfil" component={EditProfil} />
      <Stack.Screen name="UbahPassword" component={UbahPassword} />
      <Stack.Screen name="PreviewImage" component={PreviewImage} />
    </Stack.Navigator>
  );
};

export default Router;
