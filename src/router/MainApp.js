import React, {useEffect, useState} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {ButtomNavigator} from '../components';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import {Home, Notifikasi, Download, Login, User} from '../pages';

const Tab = createBottomTabNavigator();
const MainApp = () => {
  const [dataUser, setDataUser] = useState(null);

  useEffect(() => {
    const _validasiSession = async () => {
      const session = await AsyncStorageLib.getItem('dataUser');
      setDataUser(session !== null && JSON.parse(session));
    };
    _validasiSession();
  }, []);

  return (
    <Tab.Navigator
      tabBar={props => <ButtomNavigator {...props} />}
      screenOptions={{headerShown: false}}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Notifikasi" component={Notifikasi} />
      <Tab.Screen name="Download" component={Download} />
      <Tab.Screen
        name={dataUser ? 'User' : 'Login'}
        component={dataUser ? User : Login}
      />
    </Tab.Navigator>
  );
};

export default MainApp;
