import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerContent from './DrawerContent';
import MainApp from './MainApp';
const DrawerC = createDrawerNavigator();

const Drawer = () => {
  return (
    <DrawerC.Navigator
      initialRouteName="Home"
      drawerContent={props => <DrawerContent {...props} />}>
      <DrawerC.Screen name="Root" component={MainApp} />
    </DrawerC.Navigator>
  );
};

export default Drawer;
