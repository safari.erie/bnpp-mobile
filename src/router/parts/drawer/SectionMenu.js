import {StyleSheet, Text, Linking} from 'react-native';
import React from 'react';
import {Drawer} from 'react-native-paper';
import {colors, fonts} from '../../../utils';
import {Gap} from '../../../components';
import {useLinking} from '@react-navigation/native';

const jsonMenuUser = [
  {
    subHeader: 'Infrastruktur',
    menus: [
      {
        name: 'Data Kecamatan',
        navigate: 'InfraKecamatan',
      },
      {
        name: 'Data Desa',
        navigate: 'InfraDesa',
      },
    ],
  },
  {
    subHeader: 'Monev',
    menus: [
      {
        name: 'Data Monev',
        navigate: 'MonevList',
      },
      {
        name: 'Tambah Data',
        navigate: 'MonevInput',
      },
    ],
  },
];
const jsonMenuGuest = [
  {
    subHeader: 'Menu',
    menus: [
      {
        name: 'Kunjungi Website',
        navigate: 'http://datasarprasip.bnpp.go.id/',
      },
      {
        name: 'Beri Rating Aplikasi',
        navigate: 'https://play.google.com/store/apps/details?id=com.bnppapp',
      },
    ],
  },
];

const SectionMenu = ({route, session}) => {
  return (
    <>
      <Gap height={30} />

      {session
        ? jsonMenuUser.map((v, i) => {
            return (
              <Drawer.Section key={i} style={styles.drawwerContent}>
                <Text
                  style={{
                    color: colors.dark,
                    fontFamily: fonts.primary[600],
                    fontSize: 14,
                    paddingHorizontal: 13,
                  }}>
                  {v.subHeader}
                </Text>
                {v.menus.map((v, i) => {
                  return (
                    <Drawer.Item
                      key={i}
                      label={v.name}
                      onPress={() => {
                        route.navigation.navigate(v.navigate);
                      }}
                    />
                  );
                })}
              </Drawer.Section>
            );
          })
        : jsonMenuGuest.map((v, i) => {
            return (
              <Drawer.Section key={i} style={styles.drawwerContent}>
                <Text
                  style={{
                    color: colors.dark,
                    fontFamily: fonts.primary[600],
                    fontSize: 14,
                    paddingHorizontal: 13,
                  }}>
                  {v.subHeader}
                </Text>
                {v.menus.map((v, i) => {
                  return (
                    <Drawer.Item
                      key={i}
                      label={v.name}
                      onPress={() => {
                        Linking.openURL(v.navigate);
                      }}
                    />
                  );
                })}
              </Drawer.Section>
            );
          })}
    </>
  );
};

export default SectionMenu;

const styles = StyleSheet.create({
  drawwerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingHorizontal: 15,
  },

  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
});
