import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Avatar, Caption, Title} from 'react-native-paper';
import {colors} from '../../../utils';
import {Logo} from '../../../assets';

const SectionUser = ({img, name, email}) => {
  return (
    <View style={{paddingHorizontal: 13}}>
      <View style={{flexDirection: 'row', marginTop: 15}}>
        <View
          style={{
            borderRadius: 50,
            borderWidth: 3,
            borderColor: colors.blue,
          }}>
          <Avatar.Image
            source={
              img == null
                ? Logo
                : {
                    uri: img,
                  }
            }
            size={50}
          />
        </View>
        <View style={{marginLeft: 15}}>
          <Title style={styles.title}>{name}</Title>
          <Caption style={styles.caption}>{email}</Caption>
        </View>
      </View>
    </View>
  );
};

export default SectionUser;

const styles = StyleSheet.create({
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
});
