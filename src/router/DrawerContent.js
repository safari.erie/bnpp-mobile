import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Drawer} from 'react-native-paper';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import SectionUser from './parts/drawer/SectionUser';
import SectionMenu from './parts/drawer/SectionMenu';
import {useDispatch, useSelector} from 'react-redux';
import {FUNCToast, VERSIAPP} from '../utils';

const DrawerContent = props => {
  const [dataUser, setDataUser] = useState(null);
  const {sessionUser} = useSelector(state => state.loginReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    const _validasiSession = async () => {
      const session = await AsyncStorageLib.getItem('dataUser');
      const datasesi = session !== null && JSON.parse(session);
      setDataUser(datasesi);
      dispatch({type: 'SESSION_USER', payload: datasesi});
    };
    _validasiSession();
  }, [sessionUser]);
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawwerContent}>
          {/* section header */}
          <SectionUser
            name={dataUser?.username || 'Guest'}
            email={dataUser?.email || 'tidak dapat akses fitur'}
            img={dataUser?.profile_photo_url}
          />

          {/* section menu */}
          <SectionMenu route={props} session={dataUser} />
        </View>
      </DrawerContentScrollView>

      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={({color, size}) => (
            <Icon name="information-variant" color={color} size={size} />
          )}
          label={`Versi ${VERSIAPP}`}
          onPress={() => {
            FUNCToast('INFO', {msg: `BNPP APP. \nVersi ${VERSIAPP}`});
          }}
        />
      </Drawer.Section>
    </View>
  );
};

export default DrawerContent;

const styles = StyleSheet.create({
  drawwerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingHorizontal: 15,
  },

  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
});
