import {RFValue} from 'react-native-responsive-fontsize';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const fontSize = {
  mini: RFValue(12, hp('100%')),
  small: RFValue(14, hp('100%')),
  medium: RFValue(17, hp('100%')),
  large: RFValue(20, hp('100%')),
  xlarge: RFValue(24, hp('100%')),
};
