const mainColors = {
  bluedark: '#06283D',
  bluelight: '#1363DF',
  blue1: '#0E71C3',
  yellow: '#FFB562',
  grey1: '#727272',
  grey2: '#E6E6E6',
  dark1: '#3A3845',
  dark2: '#1B1A17',
  white1: '#F3F5FC',
  red1: '#FF4500',
};

export const colors = {
  primary: mainColors.green1,
  secondary: mainColors.blue1,
  dark: mainColors.dark1,
  grey: mainColors.grey1,
  greySecondary: mainColors.grey2,
  snow: mainColors.white1,
  red: mainColors.red1,
  white: 'white',
  black: mainColors.bluedark,
  bluelight: mainColors.bluelight,
  blue: mainColors.blue1,
  yellow: mainColors.yellow,
  text: {
    primary: mainColors.dark1,
    secondary: mainColors.dark2,
  },
};
