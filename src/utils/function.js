import Tips from 'react-native-root-tips';
import {colors} from './colors';
import RNFetchBlob from 'rn-fetch-blob';
import _ from 'lodash';

export const FUNCToast = (status, {...obj}) => {
  if (status === 'SUCCESS') {
    Tips.showSuccess(obj.msg, {
      duration: obj.duration === undefined ? 2000 : obj.duration,
      position: Tips.positions.CENTER,
      shadow: false,
      animation: true,
      hideOnPress: false,
      mask: true,
      maskColor: colors.black,
      delay: 0,
    });
  }
  if (status === 'FAIL') {
    Tips.showFail(obj.msg, {
      duration: 2000,
      position: Tips.positions.CENTER,
      shadow: false,
      animation: true,
      hideOnPress: false,
      mask: true,
      maskColor: colors.black,
      delay: 0,
    });
  }
  if (status === 'WARN') {
    Tips.showWarn(obj.msg, {
      duration: obj.duration === undefined ? 2000 : obj.duration,
      position: Tips.positions.CENTER,
      shadow: false,
      animation: true,
      hideOnPress: false,
      mask: true,
      maskColor: colors.black,
      delay: 0,
    });
  }
  if (status === 'INFO') {
    Tips.showInfo(obj.msg, {
      duration: obj.duration === undefined ? 2000 : obj.duration,
      position: Tips.positions.CENTER,
      shadow: false,
      animation: true,
      hideOnPress: false,
      mask: true,
      maskColor: colors.black,
      delay: 0,
    });
  }
  if (status === 'LOADING') {
    Tips.showLoading(obj.msg, {
      duration: obj.duration === undefined ? 30000 : obj.duration,
      position: Tips.positions.CENTER,
      shadow: false,
      animation: true,
      hideOnPress: false,
      mask: true,
      maskColor: colors.black,
      delay: 0,
    });
  }
  if (status === 'HIDE') {
    Tips.hide();
  }
};

export const FUNCGetExtention = fileUrl => {
  return /[.]/.exec(fileUrl) ? /[^.]+$/.exec(fileUrl) : undefined;
};

export const FUNCDownloadFile = async fileUrl => {
  try {
    FUNCToast('LOADING', {msg: 'memuat file...'});
    let date = new Date();
    let FILE_URL = fileUrl;
    let file_ext = FUNCGetExtention(FILE_URL);

    file_ext = '.' + file_ext[0];

    const {config, fs} = RNFetchBlob;
    const RootDir = (await fs.dirs.PictureDir) + `/BnppApp/`;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        path:
          RootDir +
          '/bnppapp_file' +
          Math.floor(date.getTime() + date.getSeconds() / 2) +
          file_ext,
        description: 'downloading file from bnpp app...',
        notification: true,
        useDownloadManager: true,
      },
    };
    config(options)
      .fetch('GET', FILE_URL)
      .then(res => {
        FUNCToast('SUCCESS', {
          msg: `File Downloaded Successfully ${
            Platform.OS !== 'ios'
              ? '\n\n' + options.addAndroidDownloads.path
              : ''
          }`,
        });
      });
  } catch (e) {
    console.log(e);
    FUNCToast('WARN', {msg: `File Downloaded Error`});
  }
};

export const FUNCIndoDate = date => {
  var SplitTanggal = date.split('-');
  var Hari = SplitTanggal[0];
  var Bulan = SplitTanggal[1];
  var Tahun = SplitTanggal[2];

  var ArrayBulan = [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember',
  ];
  if (Bulan < 10) {
    Bulan = Bulan.replace('0', '');
  }

  return Hari + ' ' + ArrayBulan[Bulan - 1] + ' ' + Tahun;
};

export const FUNCPreviousYears = back => {
  const year = new Date().getFullYear();
  const arr = Array.from({length: back}, (v, i) => year - back + i + 1);
  return _.sortBy(arr).reverse();
};
