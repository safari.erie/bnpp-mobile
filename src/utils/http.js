import axios from 'axios';
import {FUNCToast} from './function';
import {URL_BNPP_VERSION1} from './url_services';
//const baseAPIUrl = process.env.REACT_APP_ROOT_API;
export const http = axios.create({
  baseURL: URL_BNPP_VERSION1,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

// Add a request interceptor
axios.interceptors.request.use(
  function (config) {
    // Do something before request is sent, like we're inserting a timeout for only requests with a particular baseURL
    if (config.baseURL === '') {
      config.timeout = 4000;
    } else {
      return config;
    }
    console.log(config);
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  },
);

// Add a response interceptor
http.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response) {
      // const { data } = error.response;
      if (error.request) {
        // Swal.fire('Error Connection', `There is problem connecting to server. Please check your connection!`, 'error').then(() => window.location = '/')
        // localStorage.clear();
        FUNCToast('FAIL', {msg: error.message});
        return Promise.reject({
          message:
            'There is problem connecting to server. Please check your connection!',
        });
      } else {
        return Promise.reject({message: error.message});
      }
    }
  },
);

export default http;
