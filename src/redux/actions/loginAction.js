import {http} from '../../utils/http';
import {FUNCToast} from '../../utils';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const setFormLogin = (formType, formValue) => {
  return {type: 'FORM_LOGIN', formType, formValue};
};

export const apiLogin = (iData, navigation) => {
  return dispatch => {
    if (!iData.email) {
      FUNCToast('WARN', {msg: 'Masukan email atau username'});
      return;
    } else if (!iData.password) {
      FUNCToast('WARN', {msg: 'Masukan password'});
      return;
    }
    var Url = `https://admin.bnpp.swg.co.id/api/mobile/login?email=${iData.email}&password=${iData.password}&token_fcm=${iData.token_fcm}`;
    FUNCToast('LOADING', {msg: 'sedang memuat...'});
    http.post(`${Url}`).then(res => {
      let data = res.data;
      if (data.Status) {
        FUNCToast('SUCCESS', {msg: 'Anda berhasil login'});
        dispatch({type: 'SESSION_USER', payload: data.Data});
        AsyncStorageLib.setItem('dataUser', JSON.stringify(data.Data));
        setTimeout(() => {
          navigation.replace('Splash');
        }, 2000);
      } else {
        FUNCToast('FAIL', {msg: data.Message});
      }
    });
  };
};
export const apiEditProfil = (idUser, iData, navigation) => {
  return dispatch => {
    if (!iData.email) {
      FUNCToast('WARN', {msg: 'Masukan email atau username'});
      return;
    } else if (!iData.password) {
      FUNCToast('WARN', {msg: 'Masukan password'});
      return;
    }
    const json = JSON.stringify({
      idUser: idUser,
      username: iData.username,
      email: iData.email,
      name: iData.nama,
      gender: iData.gender,
      address: iData.alamat,
      phone: iData.telepon,
    });
    axios
      .post(`https://admin.bnpp.swg.co.id/api/mobile/updateProfileUser`, json, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        let data = res.data;
        if (data.Status) {
          FUNCToast('SUCCESS', {msg: 'Anda berhasil memperbarui profil'});
          setTimeout(() => {
            navigation.goBack();
          }, 2000);
        } else {
          FUNCToast('FAIL', {msg: data.Message});
        }
      });
  };
};
export const apiChangePassword = (iData, navigation) => {
  return dispatch => {
    if (iData.PasswordBaru !== iData.PasswordBaru2) {
      FUNCToast('WARN', {msg: 'Konfirmasi password baru salah !'});
      return;
    }
    const json = JSON.stringify({
      idUser: iData.idUser,
      passwordOld: iData.PasswordLama,
      passwordNew: iData.PasswordBaru,
    });
    FUNCToast('LOADING', {msg: 'sedang memuat...'});
    axios
      .post(`https://admin.bnpp.swg.co.id/api/mobile/changePassword`, json, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        let data = res.data;
        if (data.Status) {
          FUNCToast('SUCCESS', {msg: 'Anda berhasil memperbarui password'});
          navigation.goBack();
        } else {
          FUNCToast('FAIL', {msg: data.Message});
        }
      });
  };
};
