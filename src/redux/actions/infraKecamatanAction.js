import {http} from '../../utils/http';
import axios from 'axios';
import {FUNCToast} from '../../utils';

const API_URL = 'https://admin.bnpp.swg.co.id/api/mobile/';

export const setFormKecAction = (formType, formValue) => {
  return {type: 'ADD_KECAMATAN', formType, formValue};
};

export const setFormKecAsetAction = (formType, formValue) => {
  return {type: 'ADD_KECAMATAN_ASET', formType, formValue};
};

export const getInfraKecamatas = type => {
  return async dispatch => {
    dispatch({type: 'LOADING', payload: true});
    await http.get(`${API_URL}infra-kecamatans`).then(res => {
      let data = res.data;
      if (data.Status) {
        dispatch({type, payload: data.Data});
        dispatch({type: 'LOADING', payload: false});
      } else {
        console.log('gagal');
        //Swal.fire('Gagal', `${data.ReturnMessage}`, 'error');
        dispatch({type: 'LOADING', payload: false});
      }
    });
  };
};

export const getInfraKecamatanDetail = id => {
  return async dispatch => {
    dispatch({type: 'LOADING', payload: true});
    await http.get(`${API_URL}infra-kecamatan-detil?id=${id}`).then(res => {
      let data = res.data;
      if (data.Status) {
        //dispatch(setFormKecAction('Id', data.Data.kec.id));
        dispatch(setFormKecAction('Id', data.Data.kec.id));
        dispatch(
          setFormKecAction(
            'NamaCamat',
            data.Data.camat.nama_camat == null
              ? '-'
              : data.Data.camat.nama_camat,
          ),
        );
        dispatch(
          setFormKecAction(
            'AlamatKantor',
            data.Data.camat.alamat_kantor == null
              ? '--'
              : data.Data.camat.alamat_kantor,
          ),
        );
        dispatch(
          setFormKecAction(
            'PendidikanCamat',
            data.Data.camat.pendidikan_camat == null
              ? '--'
              : data.Data.camat.pendidikan_camat,
          ),
        );
        dispatch(
          setFormKecAction(
            'Regulasi',
            data.Data.camat.regulasi == null ? '--' : data.Data.camat.regulasi,
          ),
        );
      } else {
        alert('ERROR');
      }
    });
  };
};

export const prosesKecamatanEditAction = data => {
  return async dispatch => {
    await http.post(`${API_URL}update-info-kecamatan`, data).then(res => {
      let data = res.data;

      if (data.Status) {
        //dispatch(getInfraKecamatanDetail(data.Id));
        alert(` ${data.Message} `);
      } else {
        alert(`Error ${data} `);
      }
    });
  };
};

export const getKecamatanAsetList = (type, id) => {
  console.log(id);
  return async dispatch => {
    dispatch({type: 'LOADING', payload: true});
    await http.get(`${API_URL}kecamatan-asset?IdKecamatan=${id}`).then(res => {
      let data = res.data;
      if (data.Status) {
        dispatch({type, payload: data.Data});
        dispatch({type: 'LOADING', payload: false});
      } else {
        //console.log('gagal');
        dispatch({type, payload: data.Data});
        //Swal.fire('Gagal', `${data.ReturnMessage}`, 'error');
        dispatch({type: 'LOADING', payload: false});
      }
    });
  };
};

export const prosesAddKecamatanAset = (data, id) => {
  return async dispatch => {
    await http.post(`${API_URL}AddKecamatanAsset`, data).then(res => {
      let data = res.data;

      if (data.Status) {
        dispatch({type: 'MODAL_KEC_ASET', payload: false});
        //dispatch(getInfraKecamatanDetail(data.Id));
        alert(` ${data.Message} `);
        dispatch(getKecamatanAsetList('INFRA_KECAMATAN_ASETS', id));
      } else {
        alert(`Error ${data.Message} `);
      }
    });
  };
};

////////// =======================================================
////////// =======================================================
////////// =======================================================
////////// salim
////////// =======================================================
////////// =======================================================
////////// =======================================================

export const setFormInfraKecamatan = (formType, formValue) => {
  return {type: 'FORM_INFRA_KECAMATAN', formType, formValue};
};

export const getInfraKecamatans = () => {
  return dispatch => {
    dispatch({type: 'DATA_INFRA_KECAMATANS', payload: []});
    axios
      .get(`https://admin.bnpp.swg.co.id/api/mobile/infra-kecamatans`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          // setTimeout(() => {
          dispatch({type: 'DATA_INFRA_KECAMATANS', payload: data.Data});
          // }, 3000);
        } else {
          dispatch({type: 'DATA_INFRA_KECAMATANS', payload: []});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const getInfraKecamatan = id => {
  return dispatch => {
    dispatch({type: 'DATA_INFRA_KECAMATAN', payload: []});
    let Url = `https://admin.bnpp.swg.co.id/api/mobile/infra-kecamatan-detil?id=${id}`;
    axios
      .get(`${Url}`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          // setTimeout(() => {
          dispatch({type: 'DATA_INFRA_KECAMATAN', payload: data.Data});

          dispatch(
            setFormInfraKecamatan('NamaCamat', data.Data.camat?.nama_camat),
          );
          dispatch(
            setFormInfraKecamatan('GenderCamat', data.Data.camat?.gender_camat),
          );
          dispatch(
            setFormInfraKecamatan(
              'PendidikanCamat',
              data.Data.camat?.pendidikan_camat,
            ),
          );
          dispatch(
            setFormInfraKecamatan(
              'StatusKantor',
              data.Data.camat?.status_kantor,
            ),
          );
          dispatch(
            setFormInfraKecamatan(
              'KondisiKantor',
              data.Data.camat?.kondisi_kantor,
            ),
          );
          dispatch(
            setFormInfraKecamatan(
              'AlamatKantor',
              data.Data.camat?.alamat_kantor,
            ),
          );
          dispatch(
            setFormInfraKecamatan(
              'KodePosKantor',
              data.Data.camat?.kodepos_kantor,
            ),
          );
          dispatch(
            setFormInfraKecamatan('Regulasi', data.Data.camat?.regulasi),
          );
          // }, 3000);
        } else {
          dispatch({type: 'DATA_INFRA_KECAMATAN', payload: []});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const getInfraKecamatanAssetKec = id => {
  return dispatch => {
    dispatch({type: 'DATA_INFRA_KECAMATAN_ASSET_KEC', payload: []});
    let Url = `https://admin.bnpp.swg.co.id/api/mobile/kecamatan-asset?IdKecamatan=${id}`;
    FUNCToast('LOADING', {msg: 'sedang memuat...'});
    axios
      .get(`${Url}`)
      .then(res => {
        let data = res.data;
        setTimeout(() => {
          if (data.Status) {
            dispatch({
              type: 'DATA_INFRA_KECAMATAN_ASSET_KEC',
              payload: data.Data,
            });
            FUNCToast('HIDE');
          } else {
            dispatch({type: 'DATA_INFRA_KECAMATAN_ASSET_KEC', payload: []});
            FUNCToast('FAIL', {msg: 'data tidak ada'});
          }
        }, 2000);
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const apiAddKecamatanAsset = iData => {
  return dispatch => {
    let Url = `https://admin.bnpp.swg.co.id/api/mobile/AddKecamatanAsset`;
    const json = JSON.stringify({
      idKec: iData.IdKecamatan,
      nama: iData.Nama,
      baik: iData.Baik,
      rusak: iData.Rusak,
    });
    axios
      .post(`${Url}`, json, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        let data = res.data;
        if (data.Status) {
          FUNCToast('SUCCESS', {msg: 'Berhasil Input Asset Kecamatan'});
          dispatch(getInfraKecamatanAssetKec(iData.IdKecamatan));

          dispatch(setFormInfraKecamatan('Nama', ''));
          dispatch(setFormInfraKecamatan('Baik', ''));
          dispatch(setFormInfraKecamatan('Rusak', ''));
        } else {
          FUNCToast('FAIL', {msg: data.Message});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const getInfraKecamatanInfoCamat = id => {
  return dispatch => {
    dispatch(setFormInfraKecamatan('NamaCamat', ''));
    dispatch(setFormInfraKecamatan('GenderCamat', ''));
    dispatch(setFormInfraKecamatan('PendidikanCamat', ''));

    let Url = `https://admin.bnpp.swg.co.id/api/mobile/infra-kecamatan-camat?id=${id}`;
    axios
      .get(`${Url}`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch(setFormInfraKecamatan('NamaCamat', data.Data.nama_camat));
          dispatch(
            setFormInfraKecamatan('GenderCamat', data.Data.gender_camat),
          );
          dispatch(
            setFormInfraKecamatan(
              'PendidikanCamat',
              data.Data.pendidikan_camat,
            ),
          );
        } else {
          dispatch({type: 'DATA_INFRA_KECAMATAN_ASSET_KEC', payload: []});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const apiInfraKecUpdateCamat = (iData, navigation) => {
  return dispatch => {
    let Url = `https://admin.bnpp.swg.co.id/api/mobile/update-info-kecamatan`;
    const json = JSON.stringify({
      Id: iData.IdKecamatan,
      NamaCamat: iData.NamaCamat,
      GenderCamat: iData.GenderCamat,
      PendidikanCamat: iData.PendidikanCamat,
      Regulasi: iData.Regulasi,
      AlamatKantor: iData.AlamatKantor,
      KodePosKantor: iData.KodePosKantor,
      StatusKantor: iData.StatusKantor,
      KondisiKantor: iData.KondisiKantor,
    });
    axios
      .post(`${Url}`, json, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        let data = res.data;
        if (data.Status) {
          FUNCToast('SUCCESS', {msg: 'Berhasil Perbarui Camat'});
          dispatch(getInfraKecamatan(iData.IdKecamatan));
          navigation.goBack();
        } else {
          FUNCToast('FAIL', {msg: data.Message});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};
