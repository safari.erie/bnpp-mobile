import {http} from '../../utils/http';
import {FUNCToast} from '../../utils';

export const getHomes = type => {
  return async dispatch => {
    // FUNCToast('LOADING', {msg: 'sedang memuat...'});
    dispatch({type: 'DATA_HOMES', payload: null});
    await http.get(`https://admin.bnpp.swg.co.id/api/mobile/home`).then(res => {
      let data = res.data;
      if (data.Status) {
        setTimeout(() => {
          dispatch({type: 'DATA_HOMES', payload: data.Data});
        }, 1000);
      } else {
        dispatch({type: 'DATA_HOMES', payload: null});
      }
      // setTimeout(() => {
      //   FUNCToast('HIDE');
      // }, 2000);
    });
  };
};
