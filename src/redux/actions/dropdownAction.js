import {http} from '../../utils/http';

const API_URL = 'https://admin.bnpp.swg.co.id/api/mobile/';

export const getProvinsis = (type, str) => {
  return async dispatch => {
    await http.get(`${API_URL}provinces`).then(res => {
      let data = res.data;

      if (data.Status) {
        let buildArrOption = [];
        data.Data.map((v, i) => {
          buildArrOption.push({id: v.IdProvinsi, item: v.NamaProvinsi});
        });
        dispatch({type, payload: buildArrOption});
      }
    });
  };
};

export const getKabupatens = (type, idProv) => {
  return async dispatch => {
    await http.get(`${API_URL}citys?idProvinsi=${idProv}`).then(res => {
      let data = res.data;

      if (data.Status) {
        let buildArrOption = [];
        data.Data.map((v, i) => {
          buildArrOption.push({id: v.IdCity, item: v.NamaKota});
        });
        dispatch({type, payload: buildArrOption});
      }
    });
  };
};

export const getKecamatans = (type, idKota) => {
  return async dispatch => {
    await http.get(`${API_URL}districts?idKota=${idKota}`).then(res => {
      let data = res.data;
      console.log(data);
      if (data.Status) {
        let buildArrOption = [];
        data.Data.map((v, i) => {
          buildArrOption.push({id: v.IdKecamatan, item: v.NamaKecamatan});
        });
        dispatch({type, payload: buildArrOption});
      }
    });
  };
};

export const getDataJenisKelamins = type => {
  return async dispatch => {
    await http.get(`${API_URL}UtilsJenisKelamins`).then(res => {
      let data = res.data;

      if (data.Status) {
        dispatch({type, payload: data.Data});
      }
    });
  };
};

export const getDataYaTidak = type => {
  return async dispatch => {
    await http.get(`${API_URL}UtilsYaTidak`).then(res => {
      let data = res.data;

      if (data.Status) {
        dispatch({type, payload: data.Data});
      }
    });
  };
};
export const getKondisis = type => {
  return async dispatch => {
    await http.get(`${API_URL}UtilsKondisis`).then(res => {
      let data = res.data;

      if (data.Status) {
        dispatch({type, payload: data.Data});
      }
    });
  };
};
