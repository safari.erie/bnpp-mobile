import {http} from '../../utils/http';

export const getComboProvince = () => {
  return dispatch => {
    http.get(`https://admin.bnpp.swg.co.id/api/mobile/provinces`).then(res => {
      let data = res.data;
      if (data.Status) {
        dispatch({type: 'DATA_COMBO_PROVINCE', payload: data.Data});
      } else {
        console.log('gagal');
      }
    });
  };
};
export const getComboCity = idProv => {
  return dispatch => {
    http
      .get(`https://admin.bnpp.swg.co.id/api/mobile/citys?idProvinsi=${idProv}`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch({type: 'DATA_COMBO_CITY', payload: data.Data});
        } else {
          console.log('gagal');
        }
      });
  };
};
export const getComboDistrict = idKota => {
  return dispatch => {
    http
      .get(`https://admin.bnpp.swg.co.id/api/mobile/districts?idKota=${idKota}`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch({type: 'DATA_COMBO_DISTRICT', payload: data.Data});
        } else {
          console.log('gagal');
        }
      });
  };
};
