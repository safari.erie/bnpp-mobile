import {CommonActions} from '@react-navigation/native';
import axios from 'axios';
import {FUNCToast} from '../../utils';
import {http} from '../../utils/http';

export const setFormMonev = (formType, formValue) => {
  return {type: 'FORM_MONEV', formType, formValue};
};

export const getMonevs = (year, page = 1, rows = 30, dataMonevs = []) => {
  return dispatch => {
    var url = `https://admin.bnpp.swg.co.id/api/mobile/monev/Getmonevs?Tahun=${year}&page=${page}&sort=desc&perpage=${rows}`;
    if (dataMonevs.length === 0) dispatch(setFormMonev('PageCurrent', 1));
    http
      .get(`${url}`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch({
            type: 'DATA_MONEVS',
            payload: page === 1 ? data.Data : dataMonevs.concat(data.Data),
          });
          FUNCToast('HIDE');
          dispatch(setFormMonev('IsLoading', true));
          dispatch(setFormMonev('CekData', true));
        } else {
          dispatch(setFormMonev('IsLoading', false));
          dispatch(setFormMonev('CekData', false));
          FUNCToast('FAIL', {msg: 'terjadi kesalahan'});
        }
      })
      .catch(err => {
        dispatch(setFormMonev('IsLoading', false));
        dispatch(setFormMonev('CekData', false));
      });
  };
};

export const addMonevs = (idUser, iData = null, navigation) => {
  return dispatch => {
    dispatch({type: 'LOADING', payload: true});
    if (!iData.IdProv) {
      FUNCToast('WARN', {msg: 'Provinsi harus dipilih!'});
      return;
    }
    if (!iData.IdCity) {
      FUNCToast('WARN', {msg: 'Kabupaten/Kota harus dipilih!'});
      return;
    }
    if (!iData.IdDistrict) {
      FUNCToast('WARN', {msg: 'Kecamatan harus dipilih!'});
      return;
    }
    if (!iData.Lokpri) {
      FUNCToast('WARN', {msg: 'Kategori harus dipilih!'});
      return;
    }
    if (!iData.Nama) {
      FUNCToast('WARN', {msg: 'Nama harus diisi!'});
      return;
    }
    if (!iData.Jabatan) {
      FUNCToast('WARN', {msg: 'Jabatan harus diisi!'});
      return;
    }
    if (!iData.Nohp) {
      FUNCToast('WARN', {msg: 'Nohp harus diisi!'});
      return;
    }
    if (!iData.Email) {
      FUNCToast('WARN', {msg: 'Email harus diisi!'});
      return;
    }
    if (!iData.AlamatKantor) {
      FUNCToast('WARN', {msg: 'Alamat Kantor harus diisi!'});
      return;
    }
    var fd = JSON.stringify({
      IdUser: idUser,
      IdPegawai: 3,
      IdJenisLokpri: 0,
      IdKecamatan: iData.IdDistrict.value,
      kategori: iData.Lokpri.value,
      Nama: iData.Nama,
      Jabatan: iData.Jabatan,
      AlamatKantor: iData.AlamatKantor,
      Email: iData.Email,
      Nohp: iData.Nohp,
    });
    console.log(fd);
    FUNCToast('LOADING', {msg: 'sedang memuat data...'});
    axios
      .post(`https://admin.bnpp.swg.co.id/api/mobile/monev/temp`, fd, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        let data = res.data;
        FUNCToast('HIDE');
        dispatch({type: 'DATA_ADD_MONEVS', payload: data.data});
        dispatch(getBobotKondisis());
        navigation.navigate('MonevInputStep');
      })
      .catch(err => {
        FUNCToast('FAIL', {msg: err.message});
      });
  };
};

export const getBobotKondisis = () => {
  return dispatch => {
    dispatch({type: 'LOADING', payload: true});
    axios
      .get(`https://admin.bnpp.swg.co.id/api/mobile/master/GetBobotKondisis`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch({type: 'DATA_BOBOTS', payload: data.Data});
          dispatch({type: 'LOADING', payload: false});
        } else {
          console.log('gagal');
          //Swal.fire('Gagal', `${data.ReturnMessage}`, 'error');
          dispatch({type: 'LOADING', payload: false});
        }
      })
      .catch(err => {
        console.log('errr:', err);
      });
  };
};

export const setGetJawaban = (
  idVariabel = 1,
  idIndikator = 2,
  expand = {},
  jawaban = {},
  finish = false,
) => {
  return dispatch => {
    dispatch({type: 'LOADING', payload: true});
    var fd = JSON.stringify(jawaban);
    var url = `https://svcmonev.swg.co.id/api/Mnvs/SetGetJawaban?IdUser=5&IdMonev=10&IdVariabel=${idVariabel}&IdIndikator=${idIndikator}`;
    // console.log('url : ', url);
    var config = {
      method: 'post',
      url: url,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      data: fd,
    };

    axios(config)
      .then(function (res) {
        let data = res.data;
        if (data.isSuccess) {
          dispatch(expand);
          dispatch({type: 'JAWABAN_INPUTMONEV', payload: data.data});
          dispatch({type: 'LOADING', payload: false});
          if (finish) {
            alert('done bro');
          }
          // data.data.pertanyaans.map((v, i) => {
          //   dispatch(
          //     setFormMonev(
          //       `Keterisian${v.idPertanyaan?.toString()}`,
          //       v.kebutuhan?.toString(),
          //     ),
          //   );
          // });
        } else {
          console.log('gagal');
          //Swal.fire('Gagal', `${data.ReturnMessage}`, 'error');
          dispatch({type: 'LOADING', payload: false});
        }
      })
      .catch(function (error) {
        console.log(error);
      });

    // var data = JSON.stringify(jawaban);
    // console.log(
    //   `https://svcmonev.swg.co.id/api/Mnvs/SetGetJawaban?IdUser=5&IdMonev=10&IdVariabel=${idVariabel}&IdIndikator=${idIndikator}`,
    // );
    // axios
    //   .post(
    //     `https://svcmonev.swg.co.id/api/Mnvs/SetGetJawaban?IdUser=5&IdMonev=10&IdVariabel=${idVariabel}&IdIndikator=${idIndikator}`,
    //     data,
    //   )
    //   .then(res => {
    //     let data = res.data;
    //     if (data.isSuccess) {
    //       expand;
    //       dispatch({type: 'DATA_ADD_MONEV_SINGLE', payload: data.data});
    //       dispatch({type: 'LOADING', payload: false});
    //     } else {
    //       console.log('gagal');
    //       //Swal.fire('Gagal', `${data.ReturnMessage}`, 'error');
    //       dispatch({type: 'LOADING', payload: false});
    //     }
    //   })
    //   .catch(err => {
    //     console.log('errr:', err);
    //   });
  };
};

export const getJenisLokpris = () => {
  return dispatch => {
    dispatch({type: 'LOADING', payload: true});
    axios
      .get(`https://admin.bnpp.swg.co.id/api/mobile/monev/GetJenisLokpris`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch({type: 'DATA_JENIS_LOKPRIS', payload: data.Data});
          dispatch({type: 'LOADING', payload: false});
        } else {
          console.log('gagal');
          //Swal.fire('Gagal', `${data.ReturnMessage}`, 'error');
          dispatch({type: 'LOADING', payload: false});
        }
      })
      .catch(err => {
        console.log('errr:', err);
      });
  };
};

export const saveMonevStep = (iData, isDone, navigation = null) => {
  return dispatch => {
    var fd = JSON.stringify(iData);
    // console.log('========================================');
    // console.log(fd);
    // console.log('========================================');
    axios
      .post(`https://admin.bnpp.swg.co.id/api/mobile/monev/saveMonevStep`, fd, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        let data = res.data;
        if (isDone) {
          dispatch({type: 'RESET_FORM_MONEV'});
          FUNCToast('SUCCESS', {msg: 'Berhasil Input Data'});
          setTimeout(() => {
            navigation.dispatch(
              CommonActions.reset({
                index: 2,
                routes: [
                  {name: 'MainApp'},
                  {name: 'MonevList'},
                  {
                    name: 'MonevDetail',
                    params: {PARAMId: iData.idMonev, PARAMData: null},
                  },
                ],
              }),
            );
          }, 2000);
        }
      })
      .catch(err => {
        FUNCToast('FAIL', {msg: err.message});
      });
  };
};

export const getDetailMonev = id => {
  return dispatch => {
    dispatch({type: 'DATA_MONEV_DETAIL', payload: null});
    axios
      .get(
        `https://admin.bnpp.swg.co.id/api/mobile/monev/detailMonevById/${id}`,
      )
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch({type: 'DATA_MONEV_DETAIL', payload: data.Data});
        } else {
          console.log('gagal');
          //Swal.fire('Gagal', `${data.ReturnMessage}`, 'error');
        }
      })
      .catch(err => {
        console.log('errr:', err);
      });
  };
};
