import {FUNCIndoDate} from '../../utils';
import {http} from '../../utils/http';

export const setFormBerita = (formType, formValue) => {
  return {type: 'FORM_BERITA', formType, formValue};
};

export const getBeritas = type => {
  return async dispatch => {
    dispatch({type: 'DATA_BERITAS', payload: []});
    await http
      .get(`https://admin.bnpp.swg.co.id/api/mobile/beritas`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          setTimeout(() => {
            dispatch({type: 'DATA_BERITAS', payload: data.Data});
          }, 2000);
        } else {
          console.log('gagal');
          dispatch({type: 'DATA_BERITAS', payload: []});
          //Swal.fire('Gagal', `${data.ReturnMessage}`, 'error');
        }
      });
  };
};

export const getBeritaDetail = id => {
  return dispatch => {
    dispatch({type: 'RESET_FORM_BERITA'});
    http
      .get(`https://admin.bnpp.swg.co.id/api/mobile/berita?idContent=${id}`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch(setFormBerita('idBerita', data.Data.id));
          dispatch(setFormBerita('email', data.Data.email));
          dispatch(setFormBerita('judul', data.Data.judul));
          dispatch(setFormBerita('content', data.Data.content));
          dispatch(setFormBerita('poster', data.Data.poster));
          dispatch(setFormBerita('created', FUNCIndoDate(data.Data.created)));
          dispatch(setFormBerita('fullImageUrl', data.Data.fullImageUrl));
        }
      });
  };
};
