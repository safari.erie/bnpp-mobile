import {http} from '../../utils/http';
import axios from 'axios';
import {FUNCToast} from '../../utils';

const API_URL = 'https://admin.bnpp.swg.co.id/api/mobile/';

export const setFormDesaAction = (formType, formValue) => {
  return {type: 'ADD_DESA', formType, formValue};
};

export const setFormDesaAsetAction = (formType, formValue) => {
  return {type: 'ADD_DESA_ASET', formType, formValue};
};

export const getInfraDess = type => {
  return async dispatch => {
    dispatch({type: 'LOADING', payload: true});
    await http.get(`${API_URL}infra-desas`).then(res => {
      let data = res.data;
      if (data.Status) {
        dispatch({type, payload: data.Data});
        dispatch({type: 'LOADING', payload: false});
      } else {
        console.log('gagal');
        //Swal.fire('Gagal', `${data.ReturnMessage}`, 'error');
        dispatch({type: 'LOADING', payload: false});
      }
    });
  };
};

export const getInfraDesaDetail = id => {
  return async dispatch => {
    dispatch({type: 'LOADING', payload: true});
    await http.get(`${API_URL}infra-desa-detil?id=${id}`).then(res => {
      let data = res.data;
      if (data.Status) {
        //dispatch(setFormDesaAction('Id', data.Data.kec.id));
        dispatch(setFormDesaAction('Id', data.Data.kec.id));
        dispatch(
          setFormDesaAction(
            'NamaKades',
            data.Data.camat.nama_camat == null
              ? '-'
              : data.Data.camat.nama_camat,
          ),
        );
        dispatch(
          setFormDesaAction(
            'AlamatKantor',
            data.Data.camat.alamat_kantor == null
              ? '--'
              : data.Data.camat.alamat_kantor,
          ),
        );
        dispatch(
          setFormDesaAction(
            'PendidikanKades',
            data.Data.camat.pendidikan_camat == null
              ? '--'
              : data.Data.camat.pendidikan_camat,
          ),
        );
        dispatch(
          setFormDesaAction(
            'Regulasi',
            data.Data.camat.regulasi == null ? '--' : data.Data.camat.regulasi,
          ),
        );
      } else {
        alert('ERROR');
      }
    });
  };
};

export const prosesDesaEditAction = data => {
  return async dispatch => {
    await http.post(`${API_URL}update-info-desa`, data).then(res => {
      let data = res.data;

      if (data.Status) {
        //dispatch(getInfraDesaDetail(data.Id));
        alert(` ${data.Message} `);
      } else {
        alert(`Error ${data} `);
      }
    });
  };
};

export const getDesaAsetList = (type, id) => {
  console.log(id);
  return async dispatch => {
    dispatch({type: 'LOADING', payload: true});
    await http.get(`${API_URL}desa-asset?IdDesa=${id}`).then(res => {
      let data = res.data;
      if (data.Status) {
        dispatch({type, payload: data.Data});
        dispatch({type: 'LOADING', payload: false});
      } else {
        //console.log('gagal');
        dispatch({type, payload: data.Data});
        //Swal.fire('Gagal', `${data.ReturnMessage}`, 'error');
        dispatch({type: 'LOADING', payload: false});
      }
    });
  };
};

export const prosesAddDesaAset = (data, id) => {
  return async dispatch => {
    await http.post(`${API_URL}AddDesaAsset`, data).then(res => {
      let data = res.data;

      if (data.Status) {
        dispatch({type: 'MODAL_KEC_ASET', payload: false});
        //dispatch(getInfraDesaDetail(data.Id));
        alert(` ${data.Message} `);
        dispatch(getDesaAsetList('INFRA_DESA_ASETS', id));
      } else {
        alert(`Error ${data.Message} `);
      }
    });
  };
};

////////// =======================================================
////////// =======================================================
////////// =======================================================
////////// fatih
////////// =======================================================
////////// =======================================================
////////// =======================================================

export const setFormInfraDesa = (formType, formValue) => {
  return {type: 'FORM_INFRA_DESA', formType, formValue};
};

export const getInfraDesas = () => {
  return dispatch => {
    dispatch({type: 'DATA_INFRA_DESAS', payload: []});
    axios
      .get(`https://admin.bnpp.swg.co.id/api/mobile/infra-desa`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          // setTimeout(() => {
          dispatch({type: 'DATA_INFRA_DESAS', payload: data.Data});
          // }, 3000);
        } else {
          dispatch({type: 'DATA_INFRA_DESAS', payload: []});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const getInfraDesa = id => {
  return dispatch => {
    dispatch({type: 'DATA_INFRA_DESA', payload: []});
    let Url = `https://admin.bnpp.swg.co.id/api/mobile/infra-desa-detil?id=${id}`;
    axios
      .get(`${Url}`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          // setTimeout(() => {
          dispatch({type: 'DATA_INFRA_DESA', payload: data.Data});

          dispatch(setFormInfraDesa('NamaKades', data.Data.camat?.nama_camat));
          dispatch(
            setFormInfraDesa('GenderKades', data.Data.camat?.gender_camat),
          );
          dispatch(
            setFormInfraDesa(
              'PendidikanKades',
              data.Data.camat?.pendidikan_camat,
            ),
          );
          dispatch(
            setFormInfraDesa('StatusKantor', data.Data.camat?.status_kantor),
          );
          dispatch(
            setFormInfraDesa('KondisiKantor', data.Data.camat?.kondisi_kantor),
          );
          dispatch(
            setFormInfraDesa('AlamatKantor', data.Data.camat?.alamat_kantor),
          );
          dispatch(
            setFormInfraDesa('KodePosKantor', data.Data.camat?.kodepos_kantor),
          );
          dispatch(setFormInfraDesa('Regulasi', data.Data.camat?.regulasi));
          // }, 3000);
        } else {
          dispatch({type: 'DATA_INFRA_DESA', payload: []});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const getInfraDesaKades = id => {
  return dispatch => {
    dispatch(setFormInfraDesa('NamaKades', ''));
    dispatch(setFormInfraDesa('GenderKades', ''));
    dispatch(setFormInfraDesa('PendidikanKades', ''));

    let Url = `https://admin.bnpp.swg.co.id/api/mobile/infra-desa-kades?id=${id}`;
    axios
      .get(`${Url}`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch(setFormInfraDesa('NamaKades', data.Data.nama_kades));
          dispatch(setFormInfraDesa('GenderKades', data.Data.gender_kades));
          dispatch(
            setFormInfraDesa('PendidikanKades', data.Data.pendidikan_kades),
          );
        } else {
          dispatch({type: 'DATA_INFRA_DESA_KADES', payload: []});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const getInfraDesaKantorDesa = id => {
  return dispatch => {
    dispatch(setFormInfraDesa('Regulasi', ''));
    dispatch(setFormInfraDesa('AlamatKantor', ''));
    dispatch(setFormInfraDesa('StatusKantor', ''));
    dispatch(setFormInfraDesa('KondisiKantor', ''));

    let Url = `https://admin.bnpp.swg.co.id/api/mobile/infra-kantor-desa?id=${id}`;
    axios
      .get(`${Url}`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch(setFormInfraDesa('Regulasi', data.Data.regulasi));
          dispatch(setFormInfraDesa('AlamatKantor', data.Data.alamat));
          dispatch(setFormInfraDesa('StatusKantor', data.Data.status));
          dispatch(setFormInfraDesa('KondisiKantor', data.Data.kondisi));
        } else {
          dispatch({type: 'DATA_INFRA_DESA_KANTOR_DESA', payload: []});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const getInfraDesaBalaiDesa = id => {
  return dispatch => {
    dispatch(setFormInfraDesa('StatusBalai', ''));
    dispatch(setFormInfraDesa('KondisiBalai', ''));

    let Url = `https://admin.bnpp.swg.co.id/api/mobile/infra-desa-balai?id=${id}`;
    axios
      .get(`${Url}`)
      .then(res => {
        let data = res.data;
        if (data.Status) {
          dispatch(setFormInfraDesa('StatusBalai', data.Data.status));
          dispatch(setFormInfraDesa('KondisiBalai', data.Data.kondisi));
        } else {
          dispatch({type: 'DATA_INFRA_DESA_BALAI_DESA', payload: []});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const apiInfraDesaUpdateKades = (iData, navigation) => {
  return dispatch => {
    let Url = `https://admin.bnpp.swg.co.id/api/mobile/infra-desa-update-kades`;
    const json = JSON.stringify({
      id: iData.IdDesa,
      nama_kades: iData.NamaKades,
      gender_kades: iData.GenderKades,
      pendidikan_kades: iData.PendidikanKades,
    });

    axios
      .post(`${Url}`, json, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        let data = res.data;
        if (data.Status) {
          FUNCToast('SUCCESS', {msg: 'Berhasil Perbarui Kades'});
          dispatch(getInfraDesa(iData.IdDesa));
          navigation.goBack();
        } else {
          FUNCToast('FAIL', {msg: data.Message});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const apiInfraDesaUpdateKantorDesa = (iData, navigation) => {
  return dispatch => {
    let Url = `https://admin.bnpp.swg.co.id/api/mobile/infra-desa-update-kantor`;
    const json = JSON.stringify({
      id: iData.IdDesa,
      status: iData.StatusKantor,
      kondisi: iData.KondisiKantor,
      regulasi: iData.Regulasi,
      alamat: iData.AlamatKantor,
    });

    axios
      .post(`${Url}`, json, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        let data = res.data;
        if (data.Status) {
          FUNCToast('SUCCESS', {msg: 'Berhasil Perbarui Kantor Desa'});
          dispatch(getInfraDesa(iData.IdDesa));
          navigation.goBack();
        } else {
          FUNCToast('FAIL', {msg: data.Message});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};

export const apiInfraDesaUpdateBalaiDesa = (iData, navigation) => {
  return dispatch => {
    let Url = `https://admin.bnpp.swg.co.id/api/mobile/infra-desa-update-balai`;
    const json = JSON.stringify({
      id: iData.IdDesa,
      status: iData.StatusBalai,
      kondisi: iData.KondisiBalai,
    });
    console.log('json update balai desa', json);
    axios
      .post(`${Url}`, json, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        let data = res.data;
        if (data.Status) {
          FUNCToast('SUCCESS', {msg: 'Berhasil Perbarui Balai Desa'});
          dispatch(getInfraDesa(iData.IdDesa));
          navigation.goBack();
        } else {
          FUNCToast('FAIL', {msg: data.Message});
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };
};
