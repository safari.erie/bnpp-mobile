import {FUNCToast} from '../../utils';
import {http} from '../../utils/http';

export const setFormDownload = (formType, formValue) => {
  return {type: 'FORM_DOWNLOAD', formType, formValue};
};

export const getDownloads = type => {
  return async dispatch => {
    dispatch({type: 'DATA_DOWNLOADS', payload: []});
    await http
      .get(`https://admin.bnpp.swg.co.id/api/download-dokumen`)
      .then(res => {
        let data = res.data;
        if (data.status) {
          setTimeout(() => {
            dispatch({type: 'DATA_DOWNLOADS', payload: data.Data});
          }, 3000);
        } else {
          dispatch({type: 'DATA_DOWNLOADS', payload: []});
        }
      });
  };
};
