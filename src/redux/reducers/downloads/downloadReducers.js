import {GET_DOWNLOAD} from '../../actions';

const initialState = {
  formDownload: {Tahun: '', Keterangan: '', NamaDokumen: '', Url: ''},
  dataDownloads: [],
  setLoading: true,
  modalDownload: false,
};

const downloadReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'FORM_DOWNLOAD':
      return {
        ...state,
        formDownload: {
          ...state.formDownload,
          [action.formType]: action.formValue,
        },
      };
    case 'DATA_DOWNLOADS':
      return {...state, dataDownloads: action.payload};
    case 'LOADING':
      return {...state, setLoading: action.payload};
    case 'MODAL_DOWNLOAD':
      return {...state, modalDownload: action.payload};
    default:
      return state;
  }
};

export default downloadReducers;
