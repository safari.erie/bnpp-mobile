const initialState = {
  dataBeritas: [],
  formBerita: {
    idBerita: 0,
    email: '',
    judul: '',
    poster: '',
  },
};

const beritaReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'DATA_BERITAS':
      return {...state, dataBeritas: action.payload};
    case 'FORM_BERITA':
      return {
        ...state,
        formBerita: {
          ...state.formBerita,
          [action.formType]: action.formValue,
        },
      };
    case 'RESET_FORM_BERITA':
      return {...state, formBerita: initialState.formBerita};
    default:
      return state;
  }
};

export default beritaReducers;
