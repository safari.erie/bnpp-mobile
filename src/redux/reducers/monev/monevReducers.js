const initialState = {
  dataMonevs: [],

  dataAddMonevs: [],
  dataAddMonevSingle: [],
  dataBobots: [],
  dataSteps: [],
  currentStep: 1,
  expandTabInputMonev: {status: true},
  modalKetersediaan: false,
  modalKecukupan: false,
  modalKelayakan: false,
  modalComboMonev: false,
  jawabanInputMonev: {idIndikator: 0, type: 0, pertanyaans: []},
  selectKetersediaan: {jawaban: null},
  selectKecukupan: {jawaban: null},
  selectKelayakan: {jawaban: null},
  formMonev: {
    Kebutuhan: [],
    Keterisian: [],
    mabar: '',

    ObjName: null,
    Lokpri: null,

    FilterTahun: new Date().getFullYear(),

    IsLoading: false,
    PageCurrent: 1,
    CekData: false,
    Refreshing: false,

    Nama: '',
    Jabatan: '',
    Nohp: '',
    Email: '',
    AlamatKantor: '',
  },
  dataJenisLokpris: [],
  filterPencarianMonev: null,
  dataMonevDetail: null,
  dataMonevDetailIndikator: null,
};

const monevReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'DATA_MONEVS':
      return {...state, dataMonevs: action.payload};
    case 'DATA_ADD_MONEVS':
      return {...state, dataAddMonevs: action.payload};
    case 'DATA_ADD_MONEV_SINGLE':
      return {...state, dataAddMonevSingle: action.payload};
    case 'DATA_BOBOTS':
      return {...state, dataBobots: action.payload};
    case 'DATA_STEPS':
      return {...state, dataSteps: action.payload};
    case 'CURRENT_STEP':
      return {...state, currentStep: action.payload};
    case 'EXPAND_TAB_INPUTMONEV':
      return {...state, expandTabInputMonev: action.payload};
    case 'MODAL_KETERSEDIAAN':
      return {...state, modalKetersediaan: action.payload};
    case 'MODAL_KECUKUPAN':
      return {...state, modalKecukupan: action.payload};
    case 'MODAL_KELAYAKAN':
      return {...state, modalKelayakan: action.payload};
    case 'MODAL_COMBO_MONEV':
      return {...state, modalComboMonev: action.payload};
    case 'JAWABAN_INPUTMONEV':
      return {...state, jawabanInputMonev: action.payload};
    case 'SELECT_KETERSEDIAAN':
      return {...state, selectKetersediaan: action.payload};
    case 'SELECT_KECUKUPAN':
      return {...state, selectKecukupan: action.payload};
    case 'SELECT_KELAYAKAN':
      return {...state, selectKelayakan: action.payload};
    case 'FORM_MONEV':
      return {
        ...state,
        formMonev: {
          ...state.formMonev,
          [action.formType]: action.formValue,
        },
      };
    case 'RESET_FORM_MONEV':
      return {...state, formMonev: initialState.formMonev};
    case 'DATA_JENIS_LOKPRIS':
      return {...state, dataJenisLokpris: action.payload};
    case 'FILTER_PENCARIAN_MONEV':
      return {...state, filterPencarianMonev: action.payload};
    case 'DATA_MONEV_DETAIL':
      return {...state, dataMonevDetail: action.payload};
    case 'DATA_MONEV_DETAIL_INDIKATOR':
      return {...state, dataMonevDetailIndikator: action.payload};
    default:
      return state;
  }
};

export default monevReducers;
