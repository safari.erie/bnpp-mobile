const initialState = {
  formLogin: {
    email: 'admin@mail.com', //admin@mail.com
    password: 'password', //password

    gender: null,
    idUser: 0,
    PasswordLama: '',
    PasswordBaru: '',
    PasswordBaru2: '',
  },
  sessionUser: null,
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FORM_LOGIN':
      return {
        ...state,
        formLogin: {
          ...state.formLogin,
          [action.formType]: action.formValue,
        },
      };
    case 'RESET_FORM_LOGIN':
      return {...state, formLogin: initialState.formLogin};
    case 'MODAL_FORGOT_PASSWORD':
      return {...state, modalForget: action.payload};
    case 'forgot':
      return {
        ...state,
        formForgot: {
          ...state.formForgot,
          [action.formType]: action.formValue,
        },
      };
    case 'RESET_FORM_FORGOT':
      return {...state, formForgot: initialState.formForgot};
    case 'SESSION_USER':
      return {...state, sessionUser: action.payload};

    default:
      return state;
  }
};

export default loginReducer;
