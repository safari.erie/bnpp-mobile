const initialState = {
  dataHomes: [],
};

const homeReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'DATA_HOMES':
      return {...state, dataHomes: action.payload};
    default:
      return state;
  }
};

export default homeReducers;
