const initialState = {
  GetInfraDesas: [],
  GetInfraDesaAsets: [],
  setLoading: true,
  formDesa: {
    Id: 0,
    NamaKades: '',
    GenderKades: '',
    PendidikanKades: '',
    Regulasi: '',
    StatusKantor: 0,
    KondisiKantor: 0,
    AlamatKantor: '',
    StatusBalai: 0,
    KondisiBalai: 0,
  },
  formDesaAset: {
    idKec: 0,
    nama: '',
    baik: '',
    rusak: '',
  },
  modalDesaAset: false,

  //salim
  dataInfraDesas: [],
  dataInfraDesa: false,
  dataInfraDesaAssetKec: [],
  dataInfraDesaAssetKades: [],
  formInfraDesa: {
    IdDesa: '',
    Nama: '',
    Baik: '',
    Rusak: '',

    NamaKades: '',
    GenderKades: '',
    PendidikanKades: '',
  },
};

const infraDesaReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'INFRA_DESAS':
      return {...state, GetInfraDesas: action.payload};
    case 'INFRA_DESA_ASETS':
      return {...state, GetInfraDesaAsets: action.payload};
    case 'ADD_DESA':
      return {
        ...state,
        formDesa: {
          ...state.formDesa,
          [action.formType]: action.formValue,
        },
      };
    case 'ADD_DESA_ASET':
      return {
        ...state,
        formDesaAset: {
          ...state.formDesaAset,
          [action.formType]: action.formValue,
        },
      };
    case 'MODAL_KEC_ASET':
      return {...state, modalDesaAset: action.payload};

    //salim
    case 'DATA_INFRA_DESAS':
      return {...state, dataInfraDesas: action.payload};
    case 'DATA_INFRA_DESA':
      return {...state, dataInfraDesa: action.payload};
    case 'DATA_INFRA_DESA_ASSET_KEC':
      return {...state, dataInfraDesaAssetKec: action.payload};
    case 'DATA_INFRA_DESA_ASSET_CAMAT':
      return {...state, dataInfraDesaAssetKades: action.payload};
    case 'FORM_INFRA_DESA':
      return {
        ...state,
        formInfraDesa: {
          ...state.formInfraDesa,
          [action.formType]: action.formValue,
        },
      };
    case 'FORM_RESET_INFRA_DESA':
      return {...state, formInfraDesa: initialState.formInfraDesa};
    default:
      return state;
  }
};

export default infraDesaReducers;
