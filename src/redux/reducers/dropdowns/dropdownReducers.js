const initialState = {
  GetProvinsis: [],
  GetKabupatens: [],
  GetKacamatans: [],
  GetJenisKelamins: [],
  GetYaTidak: [],
  GetKondisis: [],
};

const dropdownReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_PROVINSIS':
      return {...state, GetProvinsis: action.payload};
    case 'GET_KABUPATENS':
      return {...state, GetKabupatens: action.payload};
    case 'GET_KECAMATANS':
      return {...state, GetKacamatans: action.payload};
    case 'GET_JENIS_KELAMIN':
      return {...state, GetJenisKelamins: action.payload};
    case 'GET_YA_TIDAK':
      return {...state, GetYaTidak: action.payload};
    case 'GET_KONDISI':
      return {...state, GetKondisis: action.payload};
    default:
      return state;
  }
};

export default dropdownReducers;
