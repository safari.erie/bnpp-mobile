const initialState = {
  GetInfraKecamatans: [],
  GetInfraKecamatanAsets: [],
  setLoading: true,
  formKecamatan: {
    Id: 0,
    NamaCamat: '',
    GenderCamat: '',
    PendidikanCamat: '',
    Regulasi: '',
    StatusKantor: 0,
    KondisiKantor: 0,
    AlamatKantor: '',
    StatusBalai: 0,
    KondisiBalai: 0,
  },
  formKecamatanAset: {
    idKec: 0,
    nama: '',
    baik: '',
    rusak: '',
  },
  modalKecamatanAset: false,

  //salim
  dataInfraKecamatans: [],
  dataInfraKecamatan: false,
  dataInfraKecamatanAssetKec: null,
  formInfraKecamatan: {
    IdKecamatan: '',
    Nama: '',
    Baik: '',
    Rusak: '',

    NamaCamat: '',
    GenderCamat: '',
    PendidikanCamat: '',
  },
};

const infraKecamatanReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'INFRA_KECAMATANS':
      return {...state, GetInfraKecamatans: action.payload};
    case 'INFRA_KECAMATAN_ASETS':
      return {...state, GetInfraKecamatanAsets: action.payload};
    case 'ADD_KECAMATAN':
      return {
        ...state,
        formKecamatan: {
          ...state.formKecamatan,
          [action.formType]: action.formValue,
        },
      };
    case 'ADD_KECAMATAN_ASET':
      return {
        ...state,
        formKecamatanAset: {
          ...state.formKecamatanAset,
          [action.formType]: action.formValue,
        },
      };
    case 'MODAL_KEC_ASET':
      return {...state, modalKecamatanAset: action.payload};

    //salim
    case 'DATA_INFRA_KECAMATANS':
      return {...state, dataInfraKecamatans: action.payload};
    case 'DATA_INFRA_KECAMATAN':
      return {...state, dataInfraKecamatan: action.payload};
    case 'DATA_INFRA_KECAMATAN_ASSET_KEC':
      return {...state, dataInfraKecamatanAssetKec: action.payload};
    case 'FORM_INFRA_KECAMATAN':
      return {
        ...state,
        formInfraKecamatan: {
          ...state.formInfraKecamatan,
          [action.formType]: action.formValue,
        },
      };
    case 'FORM_RESET_INFRA_KECAMATAN':
      return {...state, formInfraKecamatan: initialState.formInfraKecamatan};
    default:
      return state;
  }
};

export default infraKecamatanReducers;
