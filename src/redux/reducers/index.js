import {combineReducers} from 'redux';
import loginReducer from './login/loginReducer';
import homeReducers from './home/homeReducer';
import downloadReducers from './downloads/downloadReducers';
import dropdownReducers from './dropdowns/dropdownReducers';
import infraKecamatanReducers from './infraKecamatan/infraKecamatanReducers';
import infraDesaReducers from './infraDesa/infraDesaReducers';
import monevReducers from './monev/monevReducers';
import beritaReducers from './berita/beritaReducers';
import comboReducers from './combo/comboReducers';

export default combineReducers({
  loginReducer,
  homeReducers,
  downloadReducers,
  dropdownReducers,
  infraKecamatanReducers,
  infraDesaReducers,
  monevReducers,
  beritaReducers,
  comboReducers,
});
