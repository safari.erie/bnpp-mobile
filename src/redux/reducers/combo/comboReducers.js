const initialState = {
  dataComboProvince: [],
  dataComboCity: [],
  dataComboDistrict: [],
};

const comboReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'DATA_COMBO_PROVINCE':
      return {...state, dataComboProvince: action.payload};
    case 'DATA_COMBO_CITY':
      return {...state, dataComboCity: action.payload};
    case 'DATA_COMBO_DISTRICT':
      return {...state, dataComboDistrict: action.payload};
    default:
      return state;
  }
};

export default comboReducers;
