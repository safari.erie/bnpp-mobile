import './utils/ignoreLog';
import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import reducers from './redux/reducers';
import thunk from 'redux-thunk';
import Router from './router';
import OneSignalInit from './utils/onesignal';
import SplashScreen from 'react-native-splash-screen';

const store = createStore(reducers, compose(applyMiddleware(thunk)));

OneSignalInit();
const App = () => {
  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 3000);
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
