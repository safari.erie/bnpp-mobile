import Logo from './Logo.png';
import SplashBackground from './SplashBackground.png';
import Background from './Background.png';
import ProfileSample from './profile_sample.jpg';
import KantorKecamatanDefault from './kantor_kecamatan.png';
import LoadingSplash from './loading-splash.gif';
import IMGInfraKecDetail from './img--infrakec-detail.png';
export {
  Logo,
  SplashBackground,
  Background,
  ProfileSample,
  KantorKecamatanDefault,
  LoadingSplash,
  IMGInfraKecDetail,
};
