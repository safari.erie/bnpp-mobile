import IconDownload from './download.svg';
import IconDownloadActive from './downloadActive.svg';
import IconHomeActive from './homeActive.svg';
import IconHome from './home.svg';
import IconInfo from './info.svg';
import IconInfoActive from './infoActive.svg';
import IconLogin from './login.svg';
import IconloginActive from './loginActive.svg';
import IconArrowBackBlack from './ic-arrow-back-black.svg';
import IconFileDownload from './ic-file-download.svg';
import IconKecMonev from './ic-kec-monev.svg';

import ICUserEditProfile from './ic--user-edit-profile.svg';
import ICUserChangePassword from './ic--user-change-password.svg';
import ICUserVersiApp from './ic--user-versi-app.svg';
import ICUserLogout from './ic--user-logout.svg';

export {
  IconDownload,
  IconDownloadActive,
  IconHomeActive,
  IconHome,
  IconInfo,
  IconInfoActive,
  IconLogin,
  IconloginActive,
  IconArrowBackBlack,
  IconFileDownload,
  IconKecMonev,
  ICUserEditProfile,
  ICUserChangePassword,
  ICUserVersiApp,
  ICUserLogout,
};
