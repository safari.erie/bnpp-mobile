import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {fonts, colors, fontSize} from '../../utils';

const LabelComboBox = ({onPress, label, title}) => {
  return (
    <View style={{paddingHorizontal: 10, paddingTop: 5}}>
      <Text
        style={{
          fontSize: fontSize.small,
          fontFamily: fonts.primary[400],
          color: colors.dark,
        }}>
        {title} :
      </Text>
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: 10,
          borderRadius: 10,
          marginBottom: 10,
          borderWidth: 2,
          borderColor: '#F5F5F5',
          marginTop: 10,
        }}
        onPress={onPress}>
        <Text
          style={{
            fontSize: fontSize.small - 1,
            fontFamily: fonts.primary[300],
            color: colors.dark,
          }}>
          {label || 'Pilih Salah Satu'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default LabelComboBox;

const styles = StyleSheet.create({});
