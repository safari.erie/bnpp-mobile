import {Text, TextInput, View} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../utils';

const LabelTextInput = ({...item}) => {
  return (
    <View style={{paddingHorizontal: 10, paddingTop: 5}}>
      <Text
        style={{
          fontSize: fontSize.small,
          fontFamily: fonts.primary[400],
          color: colors.dark,
        }}>
        {item.title} :
      </Text>
      <TextInput
        style={{
          borderWidth: 2,
          borderColor: '#F5F5F5',
          padding: 10,
          marginVertical: 10,
          fontSize: fontSize.small - 1,
          fontFamily: fonts.primary[300],
          color: colors.dark,
          borderRadius: 10,
        }}
        {...item}
      />
    </View>
  );
};

export default LabelTextInput;
