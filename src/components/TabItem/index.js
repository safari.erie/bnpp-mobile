import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';
import {colors, fontSize} from '../../utils';

const TabItem = ({onPress, onLongPress, isFocused, label}) => {
  const IconComponent = () => {
    if (label == 'Home')
      return isFocused ? (
        <Icon name="appstore-o" color={colors.bluelight} size={25} />
      ) : (
        <Icon name="appstore-o" color={colors.grey} size={25} />
      );
    if (label == 'Notifikasi')
      return isFocused ? (
        <Icon name="inbox" color={colors.bluelight} size={25} />
      ) : (
        <Icon name="inbox" color={colors.grey} size={25} />
      );
    if (label == 'Download')
      return isFocused ? (
        <Icon name="clouddownloado" color={colors.bluelight} size={25} />
      ) : (
        <Icon name="clouddownloado" color={colors.grey} size={25} />
      );
    if (label == 'Login')
      return isFocused ? (
        <Icon name="login" color={colors.bluelight} size={25} />
      ) : (
        <Icon name="login" color={colors.grey} size={25} />
      );
    if (label == 'Logout')
      return isFocused ? (
        <Icon name="logout" color={colors.bluelight} size={25} />
      ) : (
        <Icon name="logout" color={colors.grey} size={25} />
      );
    if (label == 'User')
      return isFocused ? (
        <Icon name="setting" color={colors.bluelight} size={25} />
      ) : (
        <Icon name="setting" color={colors.grey} size={25} />
      );

    return <Icon name="setting" color={colors.grey} size={25} />;
  };

  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}>
      <IconComponent />
      <Text style={styles.text(isFocused)}>{label}</Text>
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  text: isFocused => ({
    fontSize: fontSize.small,
    color: isFocused ? colors.white : colors.grey,
    marginTop: 8,
  }),
});
