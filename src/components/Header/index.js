import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {fonts, colors, fontSize} from '../../utils';

const Header = ({
  onPressLeft,
  onPressRight,
  title,
  iconLeft,
  iconRight,
  type = 'primary',
}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
        width: '100%',
        paddingHorizontal: 20,
        paddingBottom: 20,
        borderBottomWidth: type === 'primary' ? 0.6 : 0,
        borderBottomColor: colors.greySecondary,
        backgroundColor: type === 'primary' ? colors.white : colors.black,
      }}>
      <View style={{width: '20%'}}>
        {iconLeft && (
          <TouchableOpacity onPress={onPressLeft}>{iconLeft}</TouchableOpacity>
        )}
      </View>
      <View style={{width: '60%'}}>
        <Text
          numberOfLines={1}
          style={{
            fontSize: fontSize.medium,
            fontFamily: fonts.primary[600],
            color: type == 'primary' ? colors.dark : colors.white,
            textAlign: 'center',
          }}>
          {title}
        </Text>
      </View>
      <View style={{width: '20%', alignItems: 'flex-end'}}>
        {iconRight && (
          <TouchableOpacity onPress={onPressRight}>
            {iconRight}
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({});
