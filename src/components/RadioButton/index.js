import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../utils';
import {RadioButton as RDBPaper} from 'react-native-paper';

const RadioButton = ({...item}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 10,
      }}>
      <View
        style={{
          borderRadius: 100,
          backgroundColor: colors.snow,
        }}>
        <RDBPaper color={colors.dark} {...item} />
      </View>
      <Text
        style={{
          fontSize: fontSize.small,
          fontFamily: fonts.primary[400],
          color: colors.dark,
          paddingHorizontal: 10,
        }}>
        {item.label}
      </Text>
    </View>
  );
};

export default RadioButton;

const styles = StyleSheet.create({});
