import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, fonts, fontSize} from '../../utils';
import {TouchableRipple} from 'react-native-paper';

const Button = ({type, title, onPress, width = '100%', icon = false}) => {
  return (
    <TouchableRipple
      onPress={onPress}
      borderless={true}
      rippleColor={'rgb(152 152 152 / 24%)'}
      style={{width, borderRadius: 10}}>
      <View style={styles.container(type)}>
        {icon}
        <Text style={styles.text(type, icon)}>{title}</Text>
      </View>
    </TouchableRipple>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: type => ({
    backgroundColor: type === 'secondary' ? colors.black : colors.bluelight,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    paddingVertical: 11,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: type === 'secondary' ? colors.white : colors.bluelight,
  }),
  text: (type, icon) => ({
    fontSize: fontSize.medium - 1,
    fontFamily: fonts.primary[600],
    color: type === 'secondary' ? colors.white : colors.white,
    textAlign: 'center',
    paddingLeft: !icon ? 0 : 10,
  }),
});
